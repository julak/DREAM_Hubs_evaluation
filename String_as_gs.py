#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 08:53:22 2019

@author: bioinf
"""
import pandas as pd
Network = pd.read_table('/home/bioinf/Documents/Exjobb/LASSIM_data/9606.protein.links.v10.5.txt',sep=' ')
key=pd.read_table('/home/bioinf/Documents/Hub_project/HPA/human.name_2_string.tsv',header=None)
key.columns=['number','Gene Name','STRING_Locus_ID']
GS = Network.merge(key, left_on='protein1', right_on='STRING_Locus_ID')
GS = GS.merge(key, left_on='protein2', right_on='STRING_Locus_ID')
GS = GS.loc[:,['Gene Name_x', 'Gene Name_y', 'combined_score']]
GS.columns = ['TF','target','combined_score']
GS = GS[GS.combined_score>900].sort_values(by='combined_score',ascending=False)


