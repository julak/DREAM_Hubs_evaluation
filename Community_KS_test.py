#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 16:33:54 2018

@author: bioinf
"""
import os
os.chdir('/home/bioinf/Documents/Hub_project/DREAM_Hubs_evaluation/')

import pandas as pd
import numpy as np
import scipy.stats as sts
import sklearn.preprocessing as skp
import matplotlib.pyplot as plt
import seaborn as sns
from Method_evaluation import Count_fun, count_outdegree, inverse_participation_ratio, count_IPR
from Community import Get_TF_outdegree, comHubs_TFrank
import datetime

def random_commmunity_full_mat(Network, Methods, TF_outdegree):
    random_mat = pd.DataFrame()
    random_name = pd.DataFrame()
    for i in range(1000):
        
        random_method = np.random.choice(Methods, size=len(Methods))#Random with replacement   
        #random_method = random_groups_replacement()
        
        PCC_com = pd.DataFrame(comHubs_TFrank(TF_outdegree, random_method, mean_method=np.mean).PCC) #mean_method=sts.mstats.gmean, sts.mstats.hmean, np.mean
        PCC_com.columns = [i]
        
        random_mat = random_mat.join(PCC_com, how='outer')
        random_name = random_name.join(pd.DataFrame(random_method, columns=[i]), how='outer')
    
    return random_mat

def plot_boxplot(random_mat):
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('font', size=30) 
    plt.rc('ytick', labelsize=30)
    plt.rc('xtick', labelsize=30)
    bp1 = ax.boxplot(random_mat, sym='', patch_artist=True, zorder=1)
    
    for box in bp1['boxes']:
        box.set( facecolor = 'gainsboro' )    
    for median in bp1['medians']:
        median.set(linestyle='-.', linewidth=2.5, color='royalblue')
        
    ax.plot(random_mat.T.mean(), 'r', markersize=8, zorder=2, label='Hub commnunity', linewidth=1.5)
    
    ax.set(ylabel='PCC', xlabel='#Methods', title='Hub community')
    ax.set_xticks([10, 20, 30]), 
    ax.set_xticklabels([10, 20, 30])
    
    return fig


def main():
    
    folder = '/home/bioinf/Documents/Hub_project/'
   
    #PCC_35_all = pd.DataFrame()
    for Network in ['1','3']:
        print(Network)
        #Network='3'
        now = datetime.datetime.now()        
        
        for rank_outdegree in [False, True]: #[True, False, 'Norm']:
            print(rank_outdegree)
            #rank_outdegree = FalseTrue
            Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
                'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
            ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
            Goldstandard = 'Network' + Network    
            gold_Network = pd.read_table(folder + 'Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
            gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()
            #rank_outdegree = False #Can be True, False or 'Norm'
            
            #len_Network=7000
            for len_Network in [5000, 10000, 100000]:
                #count_outdegree or count_IPR                
                TF_outdegree = Get_TF_outdegree(count_outdegree, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree)
    
                random_mat = random_commmunity_full_mat(Network, Methods, TF_outdegree)
                
                ##Kolmogorov-Smirnov test
                KS_vec = []
                for i,j in zip(np.arange(len(random_mat)-1), np.arange(1,len(random_mat))):
                    KS = sts.ks_2samp(random_mat.iloc[i], random_mat.iloc[j])
                    KS_vec.append([i+1,j+1,KS.statistic, KS.pvalue])
                KS_vec = pd.DataFrame(KS_vec, columns=['method_i','method_j','statistic','pvalue'])
                KS_vec.to_csv('/home/bioinf/Documents/Hub_project/Results/KS_Hub_community_'+str(len_Network)+'_'+str(rank_outdegree)+'_'+Network+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.csv', index=False)
                
                fig = plot_boxplot(random_mat)
                fig.savefig(folder + 'Results/boxplots/Boxplot'+str(len_Network)+'_'+str(rank_outdegree)+'_'+Network+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")
                        
if __name__ == "__main__":
    main()

