#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 10 08:43:38 2018

@author: bioinf
"""
import os
os.chdir('/home/bioinf/Documents/Hub_project/DREAM_Hubs_evaluation/')

import pandas as pd
import numpy as np
import scipy.stats as sts
import sklearn.preprocessing as skp
import matplotlib.pyplot as plt
import seaborn as sns
from Method_evaluation import Count_fun, count_outdegree, inverse_participation_ratio, count_IPR
import datetime
import random

def Get_TF_outdegree(outdegree_function, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree):
    TF_outdegree = outdegree_function(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree) 
    return TF_outdegree

def random_commmunity(Network, Methods, TF_outdegree):
    random_mat = pd.DataFrame()
    random_name = pd.DataFrame()
    for i in range(1000):
        
        random_method = np.random.choice(Methods, size=len(Methods))#Random with replacement   
        #random_method = random_groups_replacement()
        
        PCC_com = pd.DataFrame(comHubs_TFrank(TF_outdegree, random_method, mean_method=np.mean).PCC) #mean_method=sts.mstats.gmean, sts.mstats.hmean, np.mean
        PCC_com.columns = [i]
        
        random_mat = random_mat.join(PCC_com, how='outer')
        random_name = random_name.join(pd.DataFrame(random_method, columns=[i]), how='outer')
    
    return random_mat

def append_nones(length, list_):
    if len(list_) < length:
        nones = length - len(list_)
        return list_ + [None] * nones
    elif len(list_) > length:
        raise AttributeError('Length error list is too long.')
    else:
        return list_

def random_list2():
    Gr1 = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6']
    Gr2 = ['Correlation1','MI4','MI5','Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3']
    Gr3 = ['Meta1','Meta2','Meta3','Meta4','Meta5']
    Gr4 = ['Other1','Other2','Other3','Other4','Other5','Other6', 'Other7','Other8']
    Gr5 = ['Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7','Regression8']
    
    Gr1 = np.random.choice(Gr1, size=7)#len(Gr1))#random.shuffle(Gr1)
    #Gr1 = append_nones(12, Gr1)
    Gr2 = np.random.choice(Gr2, size=7)#=len(Gr2))#random.shuffle(Gr2)
    #Gr2 = append_nones(12, Gr2)
    Gr3 = np.random.choice(Gr3, size=7)#, size=len(Gr3))#random.shuffle(Gr3)
    #Gr3 = append_nones(12, Gr3)
    Gr4 = np.random.choice(Gr4, size=7)#, size=len(Gr4))#random.shuffle(Gr4)
    #Gr4 = append_nones(12, Gr4)
    Gr5 = np.random.choice(Gr5, size=7)#, size=len(Gr5))#random.shuffle(Gr5)
    #Gr5 = append_nones(12, Gr5)
    
    Gr=[Gr1,Gr2,Gr3,Gr4,Gr5]
    random.shuffle(Gr)
    Gr = list(map(list, zip(*Gr)))
    flat_list = [item for sublist in Gr for item in sublist]
    random_method = [x for x in flat_list if x is not None]
    return random_method

def random_list():
    Gr1 = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6']
    Gr2 = ['Correlation1','Correlation2', 'Correlation3']
    Gr3 = ['MI1', 'MI2', 'MI3','MI4','MI5']
    Gr4 = ['Meta1','Meta2','Meta3','Meta4','Meta5']
    Gr5 = ['Other1','Other2','Other3','Other4','Other5','Other6', 'Other7','Other8']
    Gr6 = ['Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7','Regression8']
    
    Gr1 = np.random.choice(Gr1, size=7)#len(Gr1))#random.shuffle(Gr1)
    Gr2 = np.random.choice(Gr2, size=7)#=len(Gr2))#random.shuffle(Gr2)
    Gr3 = np.random.choice(Gr3, size=7)#, size=len(Gr3))#random.shuffle(Gr3)
    Gr4 = np.random.choice(Gr4, size=7)#, size=len(Gr4))#random.shuffle(Gr4)
    Gr5 = np.random.choice(Gr5, size=7)#, size=len(Gr5))#random.shuffle(Gr5)
    Gr6 = np.random.choice(Gr6, size=7)#, size=len(Gr5))#random.shuffle(Gr5)
    
    Gr=[Gr1,Gr2,Gr3,Gr4,Gr5,Gr6]
    random.shuffle(Gr)
    Gr = list(map(list, zip(*Gr)))
    flat_list = [item for sublist in Gr for item in sublist]
    random_method = [x for x in flat_list if x is not None]
    return random_method


def random_commmunity_norepeat(Network, Methods, TF_outdegree):
    random_mat = pd.DataFrame()
    random_name = pd.DataFrame()
    mean_method=np.mean
    Used_orders = np.array([np.full(len(Methods),'t')])
    np.random.seed(seed=1)
    for i in range(1000):
        print(i)
        #random_method = np.random.choice(Methods, size=len(Methods))#Random with replacement   
        random_method = random_list()[:35]

        #####################################################################################
        PCC_com=[]
        for j in range(0,len(random_method)):
            f=0
            for n in range(1,len(Used_orders)):
                if np.isin(random_method[:j+1], Used_orders[n,:j+1]).sum()==len(random_method[:j+1]):
                    f=f+1
                    break
            
            if f==0:    
                com = pd.Series(mean_method(TF_outdegree.loc[:,random_method[:j+1]].T), index=TF_outdegree.index)
                        
                ##Correlation
                PCC = sts.pearsonr(x=TF_outdegree.loc[:,'GS'], y=com)
                PCC_com.append(PCC)
            
            else:
                PCC_com.append((np.nan,np.nan))
        #######################################################################################
        
        PCC_com = pd.DataFrame(PCC_com, index=np.arange(1,len(random_method)+1), columns=['PCC','pval'])
            
        PCC_com = pd.DataFrame(PCC_com.PCC) #mean_method=sts.mstats.gmean, sts.mstats.hmean, np.mean
        PCC_com.columns = [i]
        
        random_mat = random_mat.join(PCC_com, how='outer')
        random_name = random_name.join(pd.DataFrame(random_method, columns=[i]), how='outer')        
        
        Used_orders = np.append(Used_orders,np.array([random_method]), axis=0)

    return random_mat

def main():
    
    folder = '/home/bioinf/Documents/Hub_project/'

    Network = '1'
    rank_outdegree = False
    len_Network = 2000
    
    #New folder#
    now = datetime.datetime.now()
    dirName = folder + 'Results/Hub_community/'+Network+'/'+now.strftime("%Y-%m-%d_%H:%M")+'_6groups_correct'
    try:
        # Create target Directory
        os.mkdir(dirName)
        print("Directory " , dirName ,  " Created ") 
    except FileExistsError:
        print("Directory " , dirName ,  " already exists")
    
    #Read data#
    Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
        'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
    ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
    Goldstandard = 'Network' + Network    
    gold_Network = pd.read_table(folder + 'Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()
    
    #count_outdegree or count_IPR                
    TF_outdegree = Get_TF_outdegree(count_outdegree, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree)

    #Random community#
    Methods = Methods[:-1]
    random_mat = random_commmunity_norepeat(Network, Methods, TF_outdegree)
    #data=random_mat.T.melt()
        
    #ax = sns.lineplot(x='variable', y='value', data=data)    

    random_mat.to_csv(dirName+'/'+Network+'_Hub_community_'+str(len_Network)+'_'+str(rank_outdegree)+'.csv')
    #plot_community(PCC_mean_all, Network, rank_outdegree, dirName)


        
if __name__ == "__main__":
    main()