#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 09:10:30 2018

@author: bioinf
"""

import pandas as pd
import numpy as np
import scipy.stats as sts
import sklearn.preprocessing as skp
import matplotlib.pyplot as plt
import seaborn as sns

Network='1'
Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
        'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8'] 
Goldstandard = 'Network' + Network    
gold_Network = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
gold_TF = gold_Network.loc[:,0].unique()
gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()

TF_outdegree_full_all = pd.DataFrame()
TF_outdegree_all = pd.DataFrame()
nb_genes=np.array([])
nb_gold_genes=np.array([])
nb_TFs=np.array([])
nb_gold_TFs_full=np.array([])
nb_gold_TFs=np.array([])
for method in Methods:
    if method in ots_methods:
        name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Off-the-shelf methods/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'
    elif method == 'community':
        name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Community integration/DREAM5_NetworkInference_Community_Network' + Network + '.txt'
    else:
        name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Challenge participants/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'

    Method_Network_full = pd.read_table(name, header=None)
    Method_Network = Method_Network_full.iloc[np.isin(Method_Network_full.iloc[:,0], gold_genes)] 
    Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,1], gold_genes)] 
    
    TF_outdegree = pd.DataFrame(Method_Network.groupby(Method_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = [method])
    TF_outdegree_full = pd.DataFrame(Method_Network_full.groupby(Method_Network_full.loc[:,0].tolist()).size().sort_values(ascending=False), columns = [method])
    TF_outdegree_all = TF_outdegree_all.join(TF_outdegree, how='outer')
    TF_outdegree_full_all = TF_outdegree_full_all.join(TF_outdegree_full, how='outer')

    nb_genes = np.append(nb_genes, len(np.unique(np.array(Method_Network_full.iloc[:,:2]).flatten())))
    nb_gold_genes = np.append(nb_gold_genes,len(np.unique(np.array(Method_Network.iloc[:,:2]).flatten())))

    nb_TFs = np.append(nb_TFs,len(np.unique(np.array(Method_Network_full.iloc[:,0]).flatten())))
    nb_gold_TFs_full = np.append(nb_gold_TFs_full,np.sum(np.isin(np.unique(np.array(Method_Network_full.iloc[:,:2]).flatten()), gold_TF)))
    nb_gold_TFs = np.append(nb_gold_TFs,np.sum(np.isin(np.unique(np.array(Method_Network.iloc[:,:2]).flatten()), gold_TF)))


Gold_interactions = pd.DataFrame(TF_outdegree_all.sum(), columns=['Gold'])
Full_interactions = pd.DataFrame(TF_outdegree_full_all.sum(), columns=['Full'])

Mat = Full_interactions.join(Gold_interactions)
Mat = Mat.join(pd.DataFrame(nb_genes, columns=['nb_genes'], index=Methods))
Mat = Mat.join(pd.DataFrame(nb_gold_genes, columns=['nb_gold_genes'], index=Methods))
Mat = Mat.join(pd.DataFrame(nb_TFs, columns=['nb_TFs'], index=Methods))
Mat = Mat.join(pd.DataFrame(nb_gold_TFs_full, columns=['nb_gold_TFs_full'], index=Methods))
Mat = Mat.join(pd.DataFrame(nb_gold_TFs, columns=['nb_gold_TFs'], index=Methods))

Mat.to_csv('/home/bioinf/Documents/Hub_project/Method_information_'+Network+'.csv', sep='\t')