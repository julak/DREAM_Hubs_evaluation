# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import datetime

def plot_fig(com, title):
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('font', size=16)
    plt.rc('ytick', labelsize=10)
    plt.rc('xtick', labelsize=20)
    
    heatmap = sns.heatmap(com, cmap='seismic', center=0)
    ax.set(ylabel='PCC', xlabel='#Methods', title = title)     
    fig = heatmap.get_figure()
    return fig       

def plot_means(Hub_com, com_net, ind_met, len_Network, title):
    
    ind_met = ind_met.loc[:,'PCC_'+str(len_Network)+'_OD']
    
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('font', size=24)
    plt.rc('ytick', labelsize=20)
    plt.rc('xtick', labelsize=20)
    
    plt.plot(Hub_com.loc[:,str(len_Network)], color='b', linewidth=2.5, label='Hub community')
    plt.plot(com_net.loc[:,str(len_Network)], color='r', linewidth=2.5, label='Community network')
    plt.plot(np.full(len(ind_met),1), np.array(ind_met), '.k', markersize=8)
    ax.set(ylabel='PCC', xlabel='#Methods', title = title)     
    plt.legend(loc = 'upper right', frameon=True, bbox_to_anchor=(1, 0.2), fontsize=20)
    return fig       

def plot_coms(Hub_com, com_net, len_Network, title):
        
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('font', size=24)
    plt.rc('ytick', labelsize=20)
    plt.rc('xtick', labelsize=20)
    
    plt.plot(Hub_com.loc[:,str(len_Network)], color='b', linewidth=2.5, label='Hub community')
    plt.plot(com_net.loc[:,str(len_Network)], color='r', linewidth=2.5, label='Community network')
    ax.set(ylabel='PCC', xlabel='#Methods', title = title)     
    plt.legend(loc = 'upper right', frameon=True, bbox_to_anchor=(1, 0.2), fontsize=20)
    return fig       


com_net_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/1/2018-11-13_13:20/1_Community_network_False.csv', index_col=0)
com_net_False_1.index = np.arange(1,21)
com_net_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/3/2018-11-13_21:49/3_Community_network_False.csv', index_col=0)
com_net_False_3.index = np.arange(1,21)
Hub_com_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2018-11-12_15:26/1_Hub_community_False.csv', index_col=0)
Hub_com_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2018-11-12_16:13/3_Hub_community_False.csv', index_col=0)


#2000 and 7000
com_net_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/1/1_Community_network_False_2018-12-07_08:47.csv', index_col=0)
com_net_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/3/3_Community_network_False_2018-12-07_09:02.csv', index_col=0)
Hub_com_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2018-12-10_08:32/1_Hub_community__False.csv', index_col=0)
Hub_com_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2018-12-10_08:36/3_Hub_community__False.csv', index_col=0)

#com_false_1 = Hub_com_False_1.loc[com_net_False_1.index, com_net_False_1.columns] - com_net_False_1
#com_true_1 = Hub_com_True_1.loc[com_net_True_1.index, com_net_True_1.columns] - com_net_True_1
#com_false_3 = Hub_com_False_3.loc[com_net_False_3.index, com_net_False_3.columns] - com_net_False_3
#com_true_3 = Hub_com_True_3.loc[com_net_True_3.index, com_net_True_3.columns] - com_net_True_3

#Plot heatmaps
fig = plot_fig(com_false_1, 'in silico (Hub community - community network)')
fig.savefig('/home/bioinf/Documents/Hub_project/Results/1_Difference_communities_False_181119.png', dpi=200, bbox_inches="tight")
fig = plot_fig(com_true_1, 'in silico Ranked (Hub community - community network)')
fig.savefig('/home/bioinf/Documents/Hub_project/Results/1_Difference_communities_True_181119.png', dpi=200, bbox_inches="tight")
fig = plot_fig(com_false_3, 'E. coli (Hub community - community network)')
fig.savefig('/home/bioinf/Documents/Hub_project/Results/3_Difference_communities_False_181119.png', dpi=200, bbox_inches="tight")
fig = plot_fig(com_true_3, 'E. coli Ranked (Hub community - community network)')
fig.savefig('/home/bioinf/Documents/Hub_project/Results/3_Difference_communities_True_181119.png', dpi=200, bbox_inches="tight")
   
#Plot means at specific len_Network
#Hub_com_True_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2018-11-14_16:26/1_Hub_community__True_gmean.csv', index_col=0)
#Hub_com_True_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2018-11-14_16:50/3_Hub_community__True_gmean.csv', index_col=0)

#ind_met_false_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_1_2018-11-14_14:24.csv')
#ind_met_true_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_True_1_2018-11-14_14:24.csv')
#ind_met_false_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_3_2018-11-14_14:24.csv')
#ind_met_true_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_True_3_2018-11-14_14:24.csv')

#com_net_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/1/2018-12-06_11:26/1_Community_network_False.csv', index_col=0)
#Hub_com_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2018-12-07_08:21/1_Hub_community__False.csv', index_col=0)
now = datetime.datetime.now()
len_Network=2000
fig = plot_coms(Hub_com_False_1, com_net_False_1, len_Network=2000, title = 'in silico')
fig.savefig('/home/bioinf/Documents/Hub_project/Results/ind_com/ind_com_false_1_'+str(len_Network)+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")
fig = plot_coms(Hub_com_True_1.iloc[:20,:], com_net_True_1, len_Network=4000, title = 'in silico')
fig.savefig('/home/bioinf/Documents/Hub_project/Results/ind_com/ind_com_True_1_'+str(len_Network)+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")

len_Network=7000
fig = plot_coms(Hub_com_True_3.iloc[:20,:], com_net_True_3, len_Network=7000, title = 'E. coli')
fig.savefig('/home/bioinf/Documents/Hub_project/Results/ind_com/ind_com_True_3_'+str(len_Network)+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")
fig = plot_coms(Hub_com_False_3, com_net_False_3, len_Network=7000, title = 'E. coli')
fig.savefig('/home/bioinf/Documents/Hub_project/Results/ind_com/ind_com_false_3_'+str(len_Network)+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")

now = datetime.datetime.now()
len_Network=1000
for len_Network in [1000, 4000, 5000, 10000, 100000]:
    fig = plot_means(Hub_com_False_1.iloc[:20,:], com_net_False_1, ind_met_false_1, len_Network=len_Network, title = 'in silico')
    fig.savefig('/home/bioinf/Documents/Hub_project/Results/ind_com/ind_com_false_1_'+str(len_Network)+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")
    fig = plot_means(Hub_com_True_1.iloc[:20,:], com_net_True_1,  ind_met_true_1, len_Network=len_Network, title = 'in silico Ranked')
    fig.savefig('/home/bioinf/Documents/Hub_project/Results/ind_com/ind_com_true_1_'+str(len_Network)+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")
    fig = plot_means(Hub_com_False_3.iloc[:20,:], com_net_False_3,  ind_met_false_3, len_Network=len_Network, title = 'E. coli')
    fig.savefig('/home/bioinf/Documents/Hub_project/Results/ind_com/ind_com_false_3_'+str(len_Network)+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")
    fig = plot_means(Hub_com_True_3.iloc[:20,:], com_net_True_3,  ind_met_true_3, len_Network=len_Network, title = 'E. coli Ranked')
    fig.savefig('/home/bioinf/Documents/Hub_project/Results/ind_com/ind_com_true_3_'+str(len_Network)+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")