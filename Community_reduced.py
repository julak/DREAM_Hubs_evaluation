#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 16:00:55 2018

@author: bioinf
"""
import pandas as pd
import numpy as np
import scipy.stats as sts
import sklearn.preprocessing as skp
import matplotlib.pyplot as plt
import seaborn as sns
from Method_evaluation import Count_fun, count_outdegree, inverse_participation_ratio, count_IPR

def random_groups_replacement():
    Gr1 = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6']
    Gr2 = ['Correlation1','Meta2','MI4','MI5','Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3']
    Gr3 = ['Meta1','Meta3','Meta4','Meta5','Other1','Other2','Other3','Other4','Other5','Other6', 'Other7','Other8']
    Gr4 = ['Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7','Regression8']
    
    Gr1 = np.random.choice(Gr1, size=len(Gr1))#6)
    Gr1 = append_nones(12, list(Gr1))
    Gr2 = np.random.choice(Gr2, size=len(Gr2))#6)
    Gr2 = append_nones(12, list(Gr2))
    Gr3 = np.random.choice(Gr3, size=len(Gr3))#6)
    Gr3 = append_nones(12, list(Gr3))
    Gr4 = np.random.choice(Gr4, size=len(Gr4))#6)
    Gr4 = append_nones(12, list(Gr4))
    
    Gr=[Gr1,Gr2,Gr3,Gr4]
    random.shuffle(Gr)
    Gr = list(map(list, zip(*Gr)))
    flat_list = [item for sublist in Gr for item in sublist]
    random_method = [x for x in flat_list if x is not None]
    return random_method

def comHubs_TFrank(TF_outdegree, random_method, mean_method):
    
    PCC_com=[]
    for i in range(0,len(random_method)):
        com = pd.Series(mean_method(TF_outdegree.loc[:,random_method[:i+1]].T), index=TF_outdegree.index)
                
        ##Correlation
        PCC = sts.pearsonr(x=TF_outdegree.loc[:,'GS'], y=com)
        PCC_com.append(PCC)
    
    PCC_com = pd.DataFrame(PCC_com, index=np.arange(1,len(random_method)+1), columns=['PCC','pval'])
    
    return PCC_com

def plot_community(random_mat):
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('font', size=20) 
    plt.rc('ytick', labelsize=14)
    plt.rc('xtick', labelsize=14)
    
    plt.plot(random_mat.iloc[:,1:-1], 'k.')
    #plt.plot(random_mat.iloc[:,0], 'r.', markersize=8)
    #plt.plot(random_mat.iloc[:,-1], 'r.', markersize=8)
    plt.plot(random_mat.iloc[:,1:-1].T.mean(), 'c.', markersize=8)
    
    if Network=='1':    
        plt.ylim((-0.1, 0.62))
        ax.set(ylabel='PCC', xlabel='#Methods', title='in silico')
               
    elif Network=='3':
        plt.ylim((-0.04, 0.46))
        ax.set(ylabel='PCC', xlabel='#Methods', title='E-coli')
               
    ax.minorticks_on()
    ax.grid(which="major", color='lightgray', linestyle='--', linewidth=1)
    ax.grid(which="minor", color='lightgray', linestyle='--', linewidth=0.5)
    return fig

def random_commmunity(Network, Methods, TF_outdegree):
    random_mat = pd.DataFrame()
    random_name = pd.DataFrame()
    for i in range(1000):
        
        random_method = np.random.choice(Methods, size=len(Methods))#Random with replacement   
        #random_method = random_groups_replacement()
        
        PCC_com = pd.DataFrame(comHubs_TFrank(TF_outdegree, random_method, mean_method=np.mean).PCC) #mean_method=sts.mstats.gmean, sts.mstats.hmean, np.mean
        PCC_com.columns = [i]
        
        random_mat = random_mat.join(PCC_com, how='outer')
        random_name = random_name.join(pd.DataFrame(random_method, columns=[i]), how='outer')
    
    return random_mat.T.mean()

def Get_TF_outdegree(outdegree_function, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree):
    TF_outdegree = outdegree_function(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree) #This function do not work with network 2 as it is written now.
    return TF_outdegree

def Pearson_corr(x,Ymat):
    P = []
    Pval = []
    for i in Ymat:
        y = Ymat.loc[:,i]
        Pcof = sts.pearsonr(x, y)
        P.append(Pcof[0])
        Pval.append(Pcof[1])
    P = pd.DataFrame([P,Pval], columns = Ymat.columns, index = ['PCC', 'pval']).T
    #P = P.sort_values(by='Pearson')
    return P

def main():
    
    PCC_35_all = pd.DataFrame()
    for rank_outdegree in [True, False, 'Norm']:
        rank_outdegree=False
        for Network in ['1','3']:
            Network='1'
            Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
                'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
            ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
            Goldstandard = 'Network' + Network    
            gold_Network = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
            gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()
            #rank_outdegree = False #Can be True, False or 'Norm'
            
            PCC_mean_all = pd.DataFrame()
            len_Network = 2000
            for len_Network in [1000, 3000, 5000, 10000, 20000, 50000, 100000]:
                print(len_Network)
                
                TF_outdegree = Get_TF_outdegree(count_outdegree, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree)
                
                Methods_PCC = pd.DataFrame(Pearson_corr(TF_outdegree.GS, TF_outdegree).PCC)
                Methods_PCC.columns=[len_Network]
                
                PCC_mean = pd.DataFrame(random_commmunity(Network, Methods, TF_outdegree), columns=[len_Network])
                PCC_mean = PCC_mean.append(Methods_PCC)
                PCC_mean_all = PCC_mean_all.join(PCC_mean, how='outer')
                
            PCC_mean_all = PCC_mean_all.drop('GS')
            
            color_pallet = "bright"
            plt.style.use('seaborn-ticks')
            sns.set_color_codes(color_pallet)
        
            fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
            plt.rc('font', size=20)
            plt.rc('ytick', labelsize=6)
            plt.rc('xtick', labelsize=20)
            
            heatmap = sns.heatmap(PCC_mean_all.sort_values(by=5000), cmap='RdBu_r', center=0)
            fig = heatmap.get_figure()
            fig.savefig('/home/bioinf/Documents/Hub_project/Figures/'+Network+'PCC_heatmap.png', dpi=200, bbox_inches="tight")
            
            plt.plot(PCC_mean_all, linewidth=2)
            ax.set(ylabel='PCC', xlabel='#Methods')     
            ax.legend(labels = list(PCC_mean_all.columns), bbox_to_anchor=(1, 1))
            
            if rank_outdegree == False:
                fig.savefig('/home/bioinf/Documents/Hub_project/Figures/'+Network+'/Communities/Hub_community.png', dpi=200, bbox_inches="tight")
            elif rank_outdegree == True:
                fig.savefig('/home/bioinf/Documents/Hub_project/Figures/'+Network+'/Communities/Hub_community_rank.png', dpi=200, bbox_inches="tight")
            elif rank_outdegree == 'Norm':
                fig.savefig('/home/bioinf/Documents/Hub_project/Figures/'+Network+'/Communities/Hub_community_Norm.png', dpi=200, bbox_inches="tight")
            
            PCC_35 = pd.DataFrame(PCC_mean_all.iloc[-1,:])
            PCC_35.columns = [Network + '_' + str(rank_outdegree)]
            PCC_35_all = PCC_35_all.join(PCC_35, how='outer')
            
    PCC_35_all.to_csv('/home/bioinf/Documents/Hub_project/PCC_35.csv', sep='\t')
                
if __name__ == "__main__":
    main()
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 09:14:44 2018

@author: bioinf
"""

