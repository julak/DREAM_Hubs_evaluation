#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 13:47:56 2018

@author: bioinf
"""

import mygene
import scipy.stats as sts
import pandas as pd
import os
import numpy as np

os.chdir('/home/bioinf/Documents/Hub_project/HPA/')

tissue = pd.read_table('rna_tissue.tsv')
celline = pd.read_table('rna_celline.tsv')
TF = pd.read_table('TFs_DBD_TFpredDB.tsv') 

#GS TRRUST
Goldstandard = pd.read_table('trrust_rawdata.human.tsv', header=None) 
#GS STRING
Network = pd.read_table('9606.protein.links.v10.5.txt',sep=' ')
key=pd.read_table('human.name_2_string.tsv',header=None)
key.columns=['number','Gene Name','STRING_Locus_ID']
GS = Network.merge(key, left_on='protein1', right_on='STRING_Locus_ID')
GS = GS.merge(key, left_on='protein2', right_on='STRING_Locus_ID')
GS = GS.loc[:,['Gene Name_x', 'Gene Name_y', 'combined_score']]
GS.columns = ['TF','target','combined_score']
Goldstandard = GS[GS.combined_score>900].sort_values(by='combined_score',ascending=False)

#Tissue expression
Exp_tissue = pd.DataFrame(index=tissue.Gene.unique(), columns=tissue.Sample.unique())
for i in range(len(tissue)):
    Exp_tissue.loc[tissue.Gene[i],tissue.Sample[i]] = tissue.Value[i]
Exp_tissue = Exp_tissue.reset_index()

mg = mygene.MyGeneInfo()
HPA_entrez = mg.querymany(list(Exp_tissue.loc[:,'index']), scopes='ensemblgene', fields='symbol', as_dataframe=True)
HPA_entrez = HPA_entrez.reset_index()
Exp_tissue = Exp_tissue.merge(HPA_entrez, left_on='index', right_on='query')
Exp_tissue.index = Exp_tissue.symbol
Exp_tissue = Exp_tissue.iloc[:,1:38]

Exp_tissue = Exp_tissue.apply(pd.to_numeric)
Exp_tissue = Exp_tissue.groupby('symbol').mean()

#Exp_tissue_znorm = pd.DataFrame(sts.zscore(Exp_tissue, axis=1), columns=Exp_tissue.columns, index=Exp_tissue.index)
#genes = Exp_tissue_znorm.index

#Celline expression
Exp_celline = pd.DataFrame(index=celline.Gene.unique(), columns=celline.Sample.unique())
for i in range(len(celline)):
    Exp_celline.loc[celline.Gene[i], celline.Sample[i]] = celline.Value[i]
Exp_celline = Exp_celline.reset_index()

mg = mygene.MyGeneInfo()
HPA_entrez = mg.querymany(list(Exp_celline.loc[:,'index']), scopes='ensemblgene', fields='symbol', as_dataframe=True)
HPA_entrez = HPA_entrez.reset_index()
Exp_celline = Exp_celline.merge(HPA_entrez, left_on='index', right_on='query')
Exp_celline.index = Exp_celline.symbol
Exp_celline = Exp_celline.iloc[:,1:38]

Exp_celline = Exp_celline.apply(pd.to_numeric)
Exp_celline = Exp_celline.groupby('symbol').mean()

#Exp_celline_znorm = pd.DataFrame(sts.zscore(Exp_celline, axis=1), columns=Exp_celline.columns, index=Exp_celline.index)
#genes = Exp_celline_znorm.index

#Combine Exp
#Exp_znorm = Exp_tissue_znorm.join(Exp_celline_znorm, how='inner')
Exp_znorm = Exp_tissue.join(Exp_celline, how='inner')
Exp_znorm = Exp_znorm[((Exp_znorm==0).sum(axis=1)<20)] #removed genes with more than 24 zeros
Exp_znorm = pd.DataFrame(sts.zscore(Exp_znorm, axis=1), columns=Exp_znorm.columns, index=Exp_znorm.index)
#Exp_znorm = Exp_znorm[Exp_znorm.T.std()!=0]

#Exp_znorm = pd.read_table('HPAznorm2_expression_data.tsv').T
#Exp_znorm = Exp_znorm.dropna()
genes = Exp_znorm.index

##TFs
ens = np.unique(np.array(TF.iloc[:,1]))
symbol_TF = mg.querymany(ens, scopes='ensembl.protein', fields='symbol', as_dataframe=True)
symbol_TF = list(symbol_TF.loc[:,'symbol'].dropna())
symbol_TF = pd.DataFrame(symbol_TF).drop_duplicates()

symbol_TF = [TF for TF in np.array(symbol_TF).flatten() if TF in genes]


##GS
GS = Goldstandard.iloc[:,:2]
#GS.to_csv('/home/bioinf/Documents/Hub_project/HPA/HPAname_gold_standard.tsv', header=None, index=False, sep='\t')
GS.columns = ['TF','target']
GS = GS[GS.isin(genes)].dropna()
GS = GS[GS.TF.isin(np.array(symbol_TF).flatten())]

#TFs
TFs = np.unique(np.append(np.array(symbol_TF),np.array(GS.TF)))

##Exp
gene_order = list(TFs) + [x for x in Exp_znorm.index if x not in list(symbol_TF)]
Exp_znorm = Exp_znorm.loc[gene_order,:].T

#Exp_2 = Exp_tissue.join(Exp_celline, how='inner')
#gene_order = list(TFs) + [x for x in Exp_2.index if x not in list(symbol_TF.iloc[:,0])]
#Exp_2 = Exp_2.loc[gene_order,:].T


#Save data
#Exp_znorm=Exp_znorm.sample(frac=1, axis=1)
#TFs = Exp_znorm.columns[:100]
pd.DataFrame(TFs).to_csv('/home/bioinf/Documents/Hub_project/HPA/HPAznorm2_transcription_factors.tsv', index=False, header=None)
GS.to_csv('/home/bioinf/Documents/Hub_project/HPA/HPAznorm2_gold_standard_string.tsv', sep='\t', index=False, header=None)
Exp_znorm.to_csv('/home/bioinf/Documents/Hub_project/HPA/HPAznorm2_expression_data.tsv', index=False, sep='\t')

##Anonymize data
exp = pd.read_table('/home/bioinf/Documents/Hub_project/HPA/HPAznorm_expression_data.tsv')
#TF = pd.read_table('/home/bioinf/Documents/Hub_project/HPA/HPAznorm_transcription_factors.tsv', header=None)
#GS = pd.read_table('/home/bioinf/Documents/Hub_project/HPA/HPAznorm_gold_standard.tsv', header=None)
#
#key = pd.DataFrame([np.array(exp.columns), ['G'+str(x) for x in np.arange(1, len(exp.columns)+1)]]).T
#exp.columns = key.iloc[:,1]
#
#key.columns = ['symbol','name']
#TF.columns = ['symbol']
#
#TF = TF.merge(key, on='symbol', how='left').loc[:,'name']
#
#GS.columns=['TF','target']
#
#key.columns=['TF','TF_name']
#GS = GS.merge(key, on='TF', how='left')
#key.columns=['target','target_name']
#GS = GS.merge(key, on='target', how='left')
#GS = GS.loc[:,['TF_name','target_name']]
#
#exp.to_csv('/home/bioinf/Documents/Hub_project/HPA/HPAznorm_expression_data.tsv', index = False, sep='\t')
#TF.to_csv('/home/bioinf/Documents/Hub_project/HPA/HPAznorm_transcription_factors.tsv', header=None, index = False, sep='\t')
#GS.to_csv('/home/bioinf/Documents/Hub_project/HPA/HPAznorm_gold_standard.tsv', header=None, index=False, sep='\t')