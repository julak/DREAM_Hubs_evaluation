#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 13:25:51 2018

@author: bioinf
"""

import os
os.chdir('/home/bioinf/Documents/Hub_project/DREAM_Hubs_evaluation/')

import pandas as pd
import numpy as np
import scipy.stats as sts
import sklearn.preprocessing as skp
import matplotlib.pyplot as plt
import seaborn as sns
#from Method_evaluation import Count_fun, count_outdegree, inverse_participation_ratio, count_IPR
import datetime

def count_outdegree_PC(Network, Methods, ots_methods, len_Network, rank_outdegree):
    #Counts outdegree of all TFs in predicted networks from all methods.
    
    TF_outdegree_all = pd.DataFrame()
    for method in Methods:
        if method in ots_methods:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Off-the-shelf methods/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'
        elif method == 'community':
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Community integration/DREAM5_NetworkInference_Community_Network' + Network + '.txt'
        else:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Challenge participants/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'

        Method_Network = pd.read_table(name, header=None)
        if len(Method_Network)>len_Network:
            Method_Network = Method_Network.iloc[:len_Network,:]
    
        TF_outdegree = pd.DataFrame(Method_Network.groupby(Method_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = [method])
        
        TF_outdegree_all = TF_outdegree_all.join(TF_outdegree, how='outer')
    
    #Add hub communities
        #TFcount_rank = add_community(TF_outdegree_all)    
        
    #Gold standard outdegree        
     
    TF_outdegree_all = TF_outdegree_all.fillna(0).astype(int) 
    
    if rank_outdegree == True:
        TF_outdegree_all = TF_outdegree_all.rank(axis=0, method='average', ascending=True)
    elif rank_outdegree == 'Norm':
        TF_outdegree_all = TF_outdegree_all.div(np.max(TF_outdegree_all, axis=0)) 
        
    return TF_outdegree_all

def comHubs_TFrank(TF_outdegree, random_method, mean_method):  
    PCC_com=[]
    for i in range(0,len(random_method)):
        com = pd.Series(mean_method(TF_outdegree.loc[:,random_method[:i+1]].T), index=TF_outdegree.index)
                
        ##Correlation
        PCC = sts.pearsonr(x=TF_outdegree.loc[:,'GS'], y=com)
        PCC_com.append(PCC)
    
    PCC_com = pd.DataFrame(PCC_com, index=np.arange(1,len(random_method)+1), columns=['PCC','pval'])    
    return PCC_com

def plot_heatmap(PCC_mean_all, Network, rank_outdegree, dirName):
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)

    #fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,12), dpi=75)
    plt.rc('font', size=20)
    plt.rc('ytick', labelsize=10)
    plt.rc('xtick', labelsize=10)
    
    fig = sns.clustermap(PCC_mean_all, cmap='seismic', center=0, col_cluster=False)
    #ax.set(ylabel='PCC', xlabel='#Methods')     
    
    fig.savefig(dirName +'/'+Network+'_Hub_community_clustermap_both_'+str(rank_outdegree)+'.png', dpi=200, bbox_inches="tight")
    return

def Get_TF_outdegree(outdegree_function, Network, Methods, ots_methods, len_Network, rank_outdegree):
    TF_outdegree = outdegree_function(Network, Methods, ots_methods, len_Network, rank_outdegree) 
    return TF_outdegree

def Pair_corr(TF_outdegree, len_Network):
    PCC_all = pd.DataFrame(columns=[str(len_Network)])
    PCC_pval_all = pd.DataFrame(columns=[str(len_Network)])

    #PCC_all = pd.DataFrame(index=TF_outdegree.columns, columns=[str(len_Network)+column for column in TF_outdegree.columns])
    #PCC_pval_all = pd.DataFrame(index=np.arange(0,len(TF_outdegree.columns)), columns=np.arange(0,len(TF_outdegree.columns)))
    for i in range(len(TF_outdegree.columns)):
        for j in range(len(TF_outdegree.columns)-i-1):
            PCC = sts.pearsonr(TF_outdegree.iloc[:,i], TF_outdegree.iloc[:,i+j+1])
            PCC_all = PCC_all.append(pd.DataFrame(PCC[0], columns=[TF_outdegree.columns[i]+'_'+TF_outdegree.columns[i+j+1]], index=[str(len_Network)]).T)
            PCC_pval_all = PCC_pval_all.append(pd.DataFrame(PCC[1], columns=[TF_outdegree.columns[i]+'_'+TF_outdegree.columns[i+j+1]], index=[str(len_Network)]).T)
            #PCC_all.iloc[i,i+j+1] = PCC[0] 
            #PCC_pval_all[i,i+j+1] = PCC[1]           
    return PCC_all, PCC_pval_all


def main():
    
    Network='1'
    rank_outdegree = False
    
    folder = '/home/bioinf/Documents/Hub_project/'
    now = datetime.datetime.now()
    dirName=folder+'Results/Pairwise_correlation/'
    
    
    Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
        'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
    ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']

    
    PCC_mean_all = pd.DataFrame()
    for len_Network in [1000, 2000, 3000, 4000, 5000, 7000, 10000, 15000, 20000, 50000, 80000, 100000]:
        #len_Network=100000
        print(len_Network)
        
        #count_outdegree or count_IPR                
        TF_outdegree = Get_TF_outdegree(count_outdegree_PC, Network, Methods, ots_methods, len_Network, rank_outdegree)
        
        PCC_all, PCC_pval_all = Pair_corr(TF_outdegree.iloc[:,:-2], len_Network)
        
        PCC_mean_all = PCC_mean_all.join(PCC_all, how='outer')
    
    IPR=''
    PCC_mean_all.to_csv(dirName+'/'+Network+'_Hub_community_'+IPR+'_'+str(rank_outdegree)+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.csv')
    
    #plot_heatmap(PCC_mean_all, Network, rank_outdegree, dirName)

    #edge_cutoff = PCC_mean_all.mean().argmax()



if __name__ == "__main__":
    main()

