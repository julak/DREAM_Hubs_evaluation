#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 10 14:31:13 2018

@author: bioinf
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as sts


def plot_all_with_sem(com_net1, hub_com1, hub_com1gr, com_net3, hub_com3, hub_com3gr, ci):
    data_com1 = com_net1.T.melt()
    data_com1.columns = ['Methods', 'PCC']
    data_hub1 = hub_com1.T.melt()
    data_hub1.columns = ['Methods', 'PCC']
    data_hub1gr = hub_com1gr.T.melt()
    data_hub1gr.columns = ['Methods', 'PCC']
    data_com3 = com_net3.T.melt()
    data_com3.columns = ['Methods', 'PCC']
    data_hub3 = hub_com3.T.melt()
    data_hub3.columns = ['Methods', 'PCC']
    data_hub3gr = hub_com3gr.T.melt()
    data_hub3gr.columns = ['Methods', 'PCC']
           
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(ncols=2, facecolor='w', edgecolor='k', figsize=(20,8), dpi=75)
    plt.rc('ytick', labelsize=24)
    plt.rc('xtick', labelsize=24)
    
    sns.lineplot(x='Methods', y='PCC', data=data_com1, linewidth=2, ci=ci, c='k', label='Community network', ax=ax[0])
    sns.lineplot(x='Methods', y='PCC', data=data_hub1, linewidth=2, ci=ci, c='g', label='Random hub community', ax=ax[0])    
    sns.lineplot(x='Methods', y='PCC', data=data_hub1gr, linewidth=2, ci=ci, c='r', label='Hub community', ax=ax[0])    
    sns.lineplot(x='Methods', y='PCC', data=data_com3, linewidth=2, ci=ci, c='k', label='Community network', ax=ax[1])
    sns.lineplot(x='Methods', y='PCC', data=data_hub3, linewidth=2, ci=ci, c='g', label='Random hub community', ax=ax[1])    
    sns.lineplot(x='Methods', y='PCC', data=data_hub3gr, linewidth=2, ci=ci, c='r', label='Hub community', ax=ax[1])    
    
    ax[0].set_ylim([0.4,0.8])
    ax[0].set_ylabel('PCC',fontsize=32)
    ax[0].set_xlabel('Methods',fontsize=32)
    ax[0].set_yticks([0.5, 0.6, 0.7, 0.8])
    ax[0].set_title('in silico', fontsize=36)
    ax[1].set_ylim([0.1,0.42])
    ax[1].set_ylabel('PCC',fontsize=32)
    ax[1].set_xlabel('Methods',fontsize=32)
    ax[1].set_yticks([0.2, 0.3, 0.4])
    ax[1].set_title('E. coli', fontsize=36)

    #ax[0].legend(bbox_to_anchor=(1, 0.22), fontsize=24)
    #ax[1].legend(bbox_to_anchor=(1, 0.22), fontsize=24)
    ax[0].legend(bbox_to_anchor=(1, 0.3), fontsize=24)
    ax[1].legend(bbox_to_anchor=(1, 0.3), fontsize=24)

    sns.despine()

    return fig

def plot_yeast_with_sem(com_net4, hub_com4, hub_com4gr, ci):
    data_com4 = com_net4.T.melt()
    data_com4.columns = ['Methods', 'PCC']
    data_hub4 = hub_com4.T.melt()
    data_hub4.columns = ['Methods', 'PCC']
    data_hub4gr = hub_com4gr.T.melt()
    data_hub4gr.columns = ['Methods', 'PCC']
           
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('ytick', labelsize=24)
    plt.rc('xtick', labelsize=24)
    
    sns.lineplot(x='Methods', y='PCC', data=data_com4, linewidth=2, ci=ci, c='k', label='Community network')
    sns.lineplot(x='Methods', y='PCC', data=data_hub4, linewidth=2, ci=ci, c='g', label='Random hub community')    
    sns.lineplot(x='Methods', y='PCC', data=data_hub4gr, linewidth=2, ci=ci, c='r', label='Hub community')    
      
    ax.set_ylim([-0.15,0.1])
    ax.set_ylabel('PCC',fontsize=32)
    ax.set_xlabel('Methods',fontsize=32)
    ax.set_yticks([-0.1, 0, 0.1])
    ax.set_title('in silico', fontsize=36)
    
    ax.legend(bbox_to_anchor=(1, 1), fontsize=24)

    sns.despine()

    return fig

def KolmogorovSmirnovTest(com):
#    T_vec = []
#    for i,j in zip(np.arange(len(com)-1), np.arange(1,len(com))):
#
#        f_exp = com.iloc[i]
#        f_obs = com.iloc[j]
#        T = sts.ttest_ind(f_obs, f_exp)
#        T_vec.append([i+1,j+1,T.statistic, T.pvalue])
#        
#    T_vec = pd.DataFrame(T_vec, columns=['method_i','method_j','statistic','pvalue'])
#    return T_vec
    KS_vec = []
    for i,j in zip(np.arange(len(com)-1), np.arange(1,len(com))):
        KS = sts.ks_2samp(com.iloc[i], com.iloc[j])
        KS_vec.append([i+1,j+1,KS.statistic, KS.pvalue])
    KS_vec = pd.DataFrame(KS_vec, columns=['method_i','method_j','statistic','pvalue'])
    #KS_vec.to_csv('/home/bioinf/Documents/Exjobb/Resultat/'+Network+'/Community/KS_test_on_DREAM_vs_Hub_distribution_replacement_'+Network+'.csv', sep='\t',index=False)
    return KS_vec

#in silico
com_net_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/1/1_Community_network_False_2019-02-20_13:14.csv', index_col=0)
#hub_com1gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2019-02-20_11:33/1_Hub_community_2000_False_group.csv', index_col=0)
#hub_com1gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2019-03-06_15:31/1_Hub_commuity_2000_False.csv', index_col=0)
hub_com1gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2019-03-13_19:01_6groups_correct/1_Hub_community_2000_False.csv', index_col=0)
Hub_com_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2019-02-20_14:22/1_Hub_community_2000_False.csv', index_col=0)
#E. coli
com_net_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/3/3_Community_network_False_2019-02-20_13:26.csv', index_col=0)
#hub_com3gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2019-02-20_11:47/3_Hub_community_80000_False_group.csv', index_col=0)
#hub_com3gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2019-03-06_16:45/3_Hub_community_80000_False.csv', index_col=0)
hub_com3gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2019-03-13_19:00_6groups_correct/3_Hub_community_80000_False.csv', index_col=0)
Hub_com_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2019-02-20_14:22/3_Hub_community_80000_False.csv', index_col=0)
#S. cerevisiae
com_net_False_4 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/4/4_Community_network_False_2019-02-20_13:26.csv', index_col=0)
hub_com4gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/4/2019-02-20_11:47/4_Hub_community_3000_False_group.csv', index_col=0)
Hub_com_False_4 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/4/2019-02-20_14:23/4_Hub_community_3000_False.csv', index_col=0)

#in silico & E. coli
fig = plot_all_with_sem(com_net1=com_net_False_1, hub_com1=Hub_com_False_1, hub_com1gr=hub_com1gr, com_net3=com_net_False_3, hub_com3=Hub_com_False_3, hub_com3gr=hub_com3gr, ci=95)
fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Communities_with_95CI_article_6groups.png', dpi=200, bbox_inches="tight")
fig = plot_all_with_sem(com_net1=com_net_False_1, hub_com1=Hub_com_False_1, com_net3=com_net_False_3, hub_com3=Hub_com_False_3, ci='sd')
fig.savefig('/home/bioinf/Documents/Hub_project/Results/Communities_with_sd_norepeat.png', dpi=200, bbox_inches="tight")

#S. cerevisiae
fig = plot_yeast_with_sem(com_net4=com_net_False_4, hub_com4=Hub_com_False_4, hub_com4gr=hub_com4gr, ci=95)
fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Communities_yeast_95CI_article.png', dpi=200, bbox_inches="tight")


#SStatistical test
KS1c = KolmogorovSmirnovTest(com_net_False_1.fillna(com_net_False_1.mean()))
KS1c.columns=['i','j','1c_KS','1c_pval']
KS1h = KolmogorovSmirnovTest(Hub_com_False_1.fillna(Hub_com_False_1.mean())).iloc[:,2:]
KS1h.columns=['1h_KS','1h_pval']
KS3c = KolmogorovSmirnovTest(com_net_False_3.fillna(com_net_False_3.mean())).iloc[:,2:]
KS3c.columns=['3c_KS','3c_pval']
KS3h = KolmogorovSmirnovTest(Hub_com_False_3.fillna(Hub_com_False_3.mean())).iloc[:,2:]
KS3h.columns=['3h_KS','3h_pval']

KS = pd.concat([KS1c,KS1h,KS3c,KS3h],axis=1)
(KS<0.05)
KS.to_csv('/home/bioinf/Documents/Hub_project/Results/KS_test.csv', sep='\t',index=False)

