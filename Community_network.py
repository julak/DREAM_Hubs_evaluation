#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 09:09:57 2018

@author: bioinf
"""
import pandas as pd
import numpy as np
import scipy.stats as sts
import seaborn as sns
from sklearn import utils
import matplotlib.pyplot as plt
import random
import time
import datetime
import os

def read_and_rank_networks(Network, Methods, ots_methods, use_len_Network, len_Network):
    Rank_mat = pd.DataFrame()
    Name_mat = pd.DataFrame(columns=['TF', 'target'])
    
    for method in Methods:  
        
        if method in ots_methods:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Off-the-shelf methods/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'
        elif method == 'community':
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Community integration/DREAM5_NetworkInference_Community_Network' + Network + '.txt'
        else:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Challenge participants/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'
        
        
        Met_Net = pd.read_table(name, header=None)
        if use_len_Network==True:
            Met_Net = Met_Net.iloc[:len_Network,:]
        Met_Net.loc[:,'rank'] = Met_Net.iloc[:,2].rank(ascending=False)        
        Met_Net.index = Met_Net.loc[:,0] + Met_Net.loc[:,1]        
        Met_Net.columns = ['TF', 'target', 'score', method]        
        
        Rank_mat = Rank_mat.join(Met_Net.loc[:,method], how='outer')        
        Name_mat = pd.merge(Name_mat.loc[:,['TF','target']], Met_Net.loc[:,['TF','target']], left_on=['TF','target'], right_on=['TF','target'], how='outer')
    
    Rank_mat = Rank_mat.fillna(100001)
    Name_mat.index = Name_mat.TF + Name_mat.target
    Rank_mat = Name_mat.join(Rank_mat, how='outer')
    return Rank_mat

def Pearson_corr(x,Ymat):
    P = []
    Pval = []
    for i in Ymat:
        y = Ymat.loc[:,i]
        Pcof = sts.pearsonr(x, y)
        P.append(Pcof[0])
        Pval.append(Pcof[1])
    P = pd.DataFrame([P,Pval], columns = Ymat.columns, index = ['PCC', 'pval']).T
    #P = P.sort_values(by='Pearson')
    return P

def PCC_community_network(Network, Methods, rank_network_mat, gold_Network, gold_genes, len_Network, rank_outdegree):
    PCC_com_all=[]
    com_outdegree_all=pd.DataFrame()

    for i in range(0,35):
        rank_network_mat.loc[:,'community'] = np.mean(rank_network_mat.loc[:,Methods[:i+1]], axis=1)             
        
        community = rank_network_mat.loc[:,['TF','target','community']]
        community = community.iloc[np.isin(community.iloc[:,0], gold_genes)] 
        community = community.iloc[np.isin(community.iloc[:,1], gold_genes)]
        community = community.sort_values(by='community')
        if len(community)>len_Network:
            community = community.iloc[:len_Network,:]
        
        com_outdegree = pd.DataFrame(community.groupby(community.loc[:,'TF'].tolist()).size().sort_values(ascending=False), columns=[i])
        com_outdegree_all = com_outdegree_all.join(com_outdegree, how='outer')

    GS_outdegree = pd.DataFrame(gold_Network.groupby(gold_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns=['GS'])
    com_outdegree_all = com_outdegree_all.join(GS_outdegree, how='outer').fillna(0)
    
    if rank_outdegree == True:
        com_outdegree_all = com_outdegree_all.rank(axis=0, method='average', ascending=True)
    elif rank_outdegree == 'Norm':
        com_outdegree_all = com_outdegree_all.div(np.max(com_outdegree_all, axis=0)) 
        
    PCC_com_all = Pearson_corr(com_outdegree_all.GS, com_outdegree_all.iloc[:,:-1])
        
    return PCC_com_all
    
def random_community_network(rank_network_mat, Network, Methods, ots_methods, gold_Network, gold_genes, len_Network, use_len_Network, rank_outdegree):
        
    #rank_network_mat = read_and_rank_networks(Network, Methods, ots_methods, use_len_Network, len_Network)
        
    random_mat = pd.DataFrame()
    for i in range(10):
        print(i)
        random_method = np.random.choice(Methods, size=len(Methods))#Random with replacement
        #random_method = random_groups_replacement()
        
        PCC_com = PCC_community_network(Network, random_method, rank_network_mat, gold_Network, gold_genes, len_Network, rank_outdegree)
        PCC_com = pd.DataFrame(PCC_com.PCC)
        PCC_com.columns = [i]    
        random_mat = random_mat.join(PCC_com, how='outer')
    
    return random_mat.T.mean()

def random_community_norepeat(Network, Methods, TF_outdegree):

    random_mat = pd.DataFrame()
    Used_orders = np.array([np.full(len(Methods),'t')])
    for i in range(10):
        print(i)
        random_method = np.random.choice(Methods, size=len(Methods))#Random with replacement   
        ######################################
        
        PCC_com_all=[]
        com_outdegree_all=pd.DataFrame()
        for j in range(0,len(random_method)):
            f=0
            for n in range(1,len(Used_orders)):
                if np.isin(random_method[:j+1], Used_orders[n,:j+1]).sum()==len(random_method[:j+1]):
                    f=f+1
                    break
            
            if f==0:    
                rank_network_mat.loc[:,'community'] = np.mean(rank_network_mat.loc[:,random_method[:j+1]], axis=1)             
        
                community = rank_network_mat.loc[:,['TF','target','community']]
                community = community[community.community!=100001]
                community = community.iloc[np.isin(community.iloc[:,0], gold_genes)] 
                community = community.iloc[np.isin(community.iloc[:,1], gold_genes)]
                community = community.sort_values(by='community')
                
                if len(community)>len_Network:
                    community = community.iloc[:len_Network,:]
                
                com_outdegree = pd.DataFrame(community.groupby(community.loc[:,'TF'].tolist()).size().sort_values(ascending=False), columns=[j])
                com_outdegree_all = com_outdegree_all.join(com_outdegree, how='outer')
            else:
                com_outdegree = pd.DataFrame(np.full(len(random_method),np.nan), columns=[j])
                com_outdegree_all = com_outdegree_all.join(com_outdegree, how='outer')
                
        GS_outdegree = pd.DataFrame(gold_Network.groupby(gold_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns=['GS'])
        com_outdegree_all = com_outdegree_all.join(GS_outdegree, how='outer').fillna(0)
        
        PCC_com_all = Pearson_corr(com_outdegree_all.GS, com_outdegree_all.iloc[:,:-1])

        ################################################   
        PCC_com = pd.DataFrame(PCC_com_all, columns=['PCC','pval'])
        PCC_com.index=np.arange(1,len(random_method)+1)
            
        PCC_com = pd.DataFrame(PCC_com.PCC) #mean_method=sts.mstats.gmean, sts.mstats.hmean, np.mean
        PCC_com.columns = [i]
        
        random_mat = random_mat.join(PCC_com, how='outer')
        
        Used_orders=np.append(Used_orders,np.array([random_method]), axis=0)

    return random_mat.T.mean()

def plot_community(PCC_mean_all, Network, rank_outdegree, dirName):
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)

    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('font', size=20)
    plt.rc('ytick', labelsize=20)
    plt.rc('xtick', labelsize=20)
    
    plt.plot(PCC_mean_all, linewidth=2)
    ax.set(ylabel='PCC', xlabel='#Methods', title=Network)
    ax.legend(labels = list(PCC_mean_all.columns), bbox_to_anchor=(1, 1))
    
    
    fig.savefig(dirName+'/'+Network+'_Community_network'+str(rank_outdegree)+'.png', dpi=200, bbox_inches="tight")
    return

def main():

    folder= '/home/bioinf/Documents/Hub_project/'
    for Network in ['1', '3']:
        #Network='3'
        now = datetime.datetime.now()
        dirName=folder+'Results/Community_network/'+Network+'/'+now.strftime("%Y-%m-%d_%H:%M")
        try:
            # Create target Directory
            os.mkdir(dirName)
            print("Directory " , dirName ,  " Created ") 
        except FileExistsError:
            print("Directory " , dirName ,  " already exists")
       
        for rank_outdegree in [False, True]: #[False, True, 'Norm']
            #rank_outdegree = False
            Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
                'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
            ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
            Goldstandard = 'Network' + Network    
            gold_Network = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
            gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()
            
            use_len_Network = False #Use len Network when constructing community network
            #rank_outdegree = True #Can be True, False or 'Norm'
            
            rank_network_mat = read_and_rank_networks(Network, Methods, ots_methods, use_len_Network, len_Network=100000)
            
            PCC_mean_all = pd.DataFrame()
            for len_Network in [1000, 4000, 5000, 7000, 10000, 20000, 50000, 100000]:
                #len_Network=7000
                print(len_Network)
                start = time.time()

                PCC_mean = pd.DataFrame(random_community_norepeat(rank_network_mat=rank_network_mat.iloc[:,:-1], Network, Methods=Methods[:-1], ots_methods, gold_Network, gold_genes, len_Network, use_len_Network, rank_outdegree), columns=[len_Network])
                PCC_mean_all = PCC_mean_all.join(PCC_mean, how='outer')
                end = time.time()
                print(end - start)
            
            PCC_mean_all.index = np.arange(1, len(PCC_mean_all)+1)
            PCC_mean_all.to_csv(dirName+'/'+Network+'_Community_network_'+str(rank_outdegree)+'.csv')
        
            #PCC_mean_all = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Cluster/3_Community_network_False_2018-11-16_09:18.csv', index_col=0)
            #rank_outdegree=False
            #Network='3'
            #dirName='/home/bioinf/Documents/Hub_project/'
            
            plot_community(PCC_mean_all, Network, rank_outdegree, dirName)
            
if __name__ == "__main__":
    main()
