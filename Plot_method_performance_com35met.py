#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 09:00:21 2018

@author: bioinf
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import random
import scipy.stats as sts
import seaborn as sns

def Count_fun(len_Network, method, name, gold_genes, gold_TF):
    #Counts outdegree of all TFs in predicted network from method i.
    
    Method_Network = pd.read_table(name, header=None)
    if len(Method_Network)>len_Network:
        Method_Network = Method_Network.iloc[:len_Network,:]
    Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,0], gold_genes)] 
    Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,1], gold_genes)] 
    

    TF_outdegree = pd.DataFrame(Method_Network.groupby(Method_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = [method])
    return TF_outdegree

def count_outdegree(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree, gold_TF):
    #Counts outdegree of all TFs in predicted networks from all methods.
    
    TF_outdegree_all = pd.DataFrame()
    for method in Methods:
        if method in ots_methods:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Off-the-shelf methods/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'
        elif method == 'community':
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Community integration/DREAM5_NetworkInference_Community_Network' + Network + '.txt'
        else:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Challenge participants/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'

        TF_outdegree = Count_fun(len_Network, method=method, name=name, gold_genes=gold_genes, gold_TF=gold_TF)    
        TF_outdegree_all = TF_outdegree_all.join(TF_outdegree, how='outer')
    
    #Add hub communities
        #TFcount_rank = add_community(TF_outdegree_all)    
        
    #Gold standard outdegree        
    GS = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    GS_outdegree = pd.DataFrame(GS.groupby(GS.loc[:,0].tolist()).size().sort_values(ascending=False), columns = ['GS'])
    TF_outdegree_all = TF_outdegree_all.join(GS_outdegree, how='outer')
    
    TF_outdegree_all = TF_outdegree_all.fillna(0).astype(int) 
    
    if rank_outdegree == True:
        TF_outdegree_all = TF_outdegree_all.rank(axis=0, method='average', ascending=True)
    elif rank_outdegree == 'Norm':
        TF_outdegree_all = TF_outdegree_all.div(np.max(TF_outdegree_all, axis=0)) 
        
    return TF_outdegree_all

def Get_TF_outdegree(outdegree_function, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree, gold_TF):
    TF_outdegree = outdegree_function(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree, gold_TF) 
    return TF_outdegree

def random_PCC(Network, rank_outdegree, len_Network):
    
    Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
        'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
    ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
    Goldstandard = 'Network' + Network    
    gold_Network = pd.read_table('/home/bioinf/Documents/Hub_project/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()
    gold_TF = gold_Network.loc[:,0].unique()

    #count_outdegree or count_IPR                
    TF_outdegree = Get_TF_outdegree(count_outdegree, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree, gold_TF)

    GS=TF_outdegree.loc[:,'GS']
    
    random.sample(list(np.array(TF_outdegree.iloc[:,:-1]).flatten()), len(GS))
    np.random.seed(seed=1)  
    Pmat=[]
    for i in range(10000):
        #X = np.arange(0, len(GS))
        #random.shuffle(X)
        X = random.sample(list(np.array(TF_outdegree.iloc[:,:-1]).flatten()), len(GS))
        Pcof = sts.pearsonr(X, GS)
        Pmat.append(Pcof[0])
    
    return np.mean(Pmat)

def read_and_rank_networks(Network, Methods, ots_methods, use_len_Network, len_Network):
    Rank_mat = pd.DataFrame()
    Name_mat = pd.DataFrame(columns=['TF', 'target'])
    
    for method in Methods:  
        
        if method in ots_methods:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Off-the-shelf methods/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'
        elif method == 'community':
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Community integration/DREAM5_NetworkInference_Community_Network' + Network + '.txt'
        else:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Challenge participants/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'
        
        
        Met_Net = pd.read_table(name, header=None)
        if use_len_Network==True:
            Met_Net = Met_Net.iloc[:len_Network,:]
        Met_Net.loc[:,'rank'] = Met_Net.iloc[:,2].rank(ascending=False)        
        Met_Net.index = Met_Net.loc[:,0] + Met_Net.loc[:,1]        
        Met_Net.columns = ['TF', 'target', 'score', method]        
        
        Rank_mat = Rank_mat.join(Met_Net.loc[:,method], how='outer')        
        Name_mat = pd.merge(Name_mat.loc[:,['TF','target']], Met_Net.loc[:,['TF','target']], left_on=['TF','target'], right_on=['TF','target'], how='outer')
    
    Rank_mat = Rank_mat.fillna(100001)
    Name_mat.index = Name_mat.TF + Name_mat.target
    Rank_mat = Name_mat.join(Rank_mat, how='outer')
    return Rank_mat

def com_net(Network, len_Network, rank_outdegree):
    
    folder = '/home/bioinf/Documents/Hub_project/'
    Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
                    'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
    ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
    Goldstandard = 'Network' + Network    
    gold_Network = pd.read_table(folder + 'Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()
    gold_TF = gold_Network.loc[:,0].unique()
    
    use_len_Network = False #Use len Network when constructing community network
    rank_network_mat = read_and_rank_networks(Network, Methods, ots_methods, use_len_Network, len_Network=100000)
    
    print('ranked networks')
    
    rank_network_mat.loc[:,'community'] = np.mean(rank_network_mat.iloc[:,:-1], axis=1)             
    
    print('done community')
    community = rank_network_mat.loc[:,['TF','target','community']]
    community = community[community.community!=100001]
    community = community.iloc[np.isin(community.iloc[:,0], gold_genes)] 
    community = community.iloc[np.isin(community.iloc[:,1], gold_genes)]
    community = community.sort_values(by='community')
    
    if len(community)>len_Network:
        community = community.iloc[:len_Network,:]
    
    com_outdegree = pd.DataFrame(community.groupby(community.loc[:,'TF'].tolist()).size().sort_values(ascending=False))
    GS_outdegree = pd.DataFrame(gold_Network.groupby(gold_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns=['GS'])
    com_outdegree_all = com_outdegree.join(GS_outdegree, how='outer').fillna(0)
    if rank_outdegree == True:
        com_outdegree_all = com_outdegree_all.rank(axis=0, method='average', ascending=True)
    PCC_net = sts.pearsonr(x=com_outdegree_all.iloc[:,-1], y=com_outdegree_all.iloc[:,0])

    return PCC_net

def Hub_com(Network, len_Network, rank_outdegree):
    
    folder = '/home/bioinf/Documents/Hub_project/'
    Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
                    'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
    ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
    Goldstandard = 'Network' + Network    
    gold_Network = pd.read_table(folder + 'Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()
    gold_TF = gold_Network.loc[:,0].unique()

    TF_outdegree = Get_TF_outdegree(count_outdegree, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree, gold_TF)
    
    com = pd.Series(np.mean(TF_outdegree.iloc[:,:-2].T), index=TF_outdegree.index)
                    
    PCC_hub = sts.pearsonr(x=TF_outdegree.loc[:,'GS'], y=com)

    return PCC_hub

def plot_boxes(False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3):
    
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
        
    fig, ax = plt.subplots(nrows=2, ncols=1, facecolor='w', edgecolor='k', figsize=(20,10), dpi=75)
    
    plt.subplots_adjust(hspace=0.4)
    plt.rc('ytick', labelsize=22)
    plt.rc('xtick', labelsize=22)

    #Plot bars#
    ax[0].bar(np.arange(0,6), height=np.array(False1)[:6], width=0.9, color=['C9','C9','C9','C9','C9','C9'], label='Bayesian')
    ax[0].bar(np.arange(7,10), height=np.array(False1)[6:9], width=0.9, color=['C8','C8','C8'], label='Correlation')
    ax[0].bar(np.arange(11,16), height=np.array(False1)[9:14], width=0.9, color=['C7','C7','C7','C7','C7'], label='MI')
    ax[0].bar(np.arange(17,22), height=np.array(False1)[14:19], width=0.9, color=['lightpink','lightpink','lightpink','lightpink','lightpink'], label='Meta')
    ax[0].bar(np.arange(23,31), height=np.array(False1)[19:27], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Other')
    ax[0].bar(np.arange(32,40), height=np.array(False1)[27:35], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Regression')
    #ax[0].bar(np.arange(41,42), height=np.array(False1)[35], width=0.9, color=['grey'], label='DREAM Community (w/o replacement)')
    ax[0].bar(np.arange(42,43), height=Hub_com1[0], width=0.9, color=['r'], label='Hub community')
    ax[0].bar(np.arange(41,42), height=com_net1[0], width=0.9, color=['k'], label='Community network (w replacement)')
    ax[0].bar(np.arange(43,44), height=rand1, width=0.9, color=['k'], label='random')

    ax[1].bar(np.arange(0,6), height=np.array(False3)[:6], width=0.9, color=['C9','C9','C9','C9','C9','C9'], label='Bayesian')
    ax[1].bar(np.arange(7,10), height=np.array(False3)[6:9], width=0.9, color=['C8','C8','C8'], label='Correlation')
    ax[1].bar(np.arange(11,16), height=np.array(False3)[9:14], width=0.9, color=['C7','C7','C7','C7','C7'], label='MI')
    ax[1].bar(np.arange(17,22), height=np.array(False3)[14:19], width=0.9, color=['lightpink','lightpink','lightpink','lightpink','lightpink'], label='Meta')
    ax[1].bar(np.arange(23,31), height=np.array(False3)[19:27], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Other')
    ax[1].bar(np.arange(32,40), height=np.array(False3)[27:35], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Regression')
    #ax[1].bar(np.arange(41,42), height=np.array(False3)[35], width=0.9, color=['grey'], label='DREAM Community (w/o replacement)')
    ax[1].bar(np.arange(42,43), height=Hub_com3[0], width=0.9, color=['r'], label='Hub community')
    ax[1].bar(np.arange(41,42), height=com_net3[0], width=0.9, color=['k'], label='Community network (w replacement)')
    ax[1].bar(np.arange(43,44), height=rand3, width=0.9, color=['k'], label='random')
    
    #settings#
    ax[0].axhline(0, color='k', linewidth=1) #Plot line at 0
    ax[0].axhline(Hub_com1[0], color='k', linewidth=1, linestyle='--') #Plot line at 0
    ax[0].set_ylabel('PCC',fontsize=32)
    ax[0].set_yticks([0.4,0.8])
    ax[0].set_title('in silico', fontsize=36)
    ax[1].axhline(0, color='k', linewidth=1) #Plot line at 0
    ax[1].axhline(Hub_com3[0], color='k', linewidth=1, linestyle='--') #Plot line at 0
    ax[1].set_ylabel('PCC',fontsize=32)
    ax[1].set_yticks([0.2,0.4])
    ax[1].set_title('E. coli', fontsize=36)

    #plt.legend(frameon=False, bbox_to_anchor=(1.21, 1.9))
    #plt.setp(ax, xticks=np.arange(42), xticklabels=['1','2','3','4','5','6','','1','2','3','','1','2','3','4','5','','1','2','3','4','5','','1','2','3','4','5','6','7','8','','1','2','3','4','5','6','7','8','','C'])
    plt.setp(ax, xticks=[2.5,8,13,19,26.5,35.5,42,41,43], xticklabels=['Bayesian','Corrrelation','MI','Meta','Other','Regression', 'H', 'C', 'R'])
    
    sns.despine(offset=0, bottom=True, trim=False)
    
    ax[0].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off
    ax[1].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off

    return fig

def plot_boxes_yeast(False4, Hub_com4, com_net4, rand4):
    
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(20,5), dpi=75)
    
    plt.subplots_adjust(hspace=0.4)
    plt.rc('ytick', labelsize=22)
    plt.rc('xtick', labelsize=22)

    #Plot bars#
    ax.bar(np.arange(0,6), height=np.array(False4)[:6], width=0.9, color=['C9','C9','C9','C9','C9','C9'], label='Bayesian')
    ax.bar(np.arange(7,10), height=np.array(False4)[6:9], width=0.9, color=['C8','C8','C8'], label='Correlation')
    ax.bar(np.arange(11,16), height=np.array(False4)[9:14], width=0.9, color=['C7','C7','C7','C7','C7'], label='MI')
    ax.bar(np.arange(17,22), height=np.array(False4)[14:19], width=0.9, color=['lightpink','lightpink','lightpink','lightpink','lightpink'], label='Meta')
    ax.bar(np.arange(23,31), height=np.array(False4)[19:27], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Other')
    ax.bar(np.arange(32,40), height=np.array(False4)[27:35], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Regression')
    ax.bar(np.arange(42,43), height=Hub_com4[0], width=0.9, color=['r'], label='Hub community')
    ax.bar(np.arange(41,42), height=com_net4[0], width=0.9, color=['k'], label='Community network (w replacement)')
    ax.bar(np.arange(43,44), height=rand4, width=0.9, color=['k'], label='random')
  
    #settings#
    ax.axhline(0, color='k', linewidth=1) #Plot line at 0
    ax.axhline(Hub_com4[0], color='k', linewidth=1, linestyle='--') #Plot line at 0
    ax.set_ylabel('PCC',fontsize=32)
    ax.set_yticks([-0.1, 0.1, 0.2])
    ax.set_title('in silico', fontsize=36)

    plt.setp(ax, xticks=[2.5,8,13,19,26.5,35.5,42,41,43], xticklabels=['Bayesian','Corrrelation','MI','Meta','Other','Regression', 'H', 'C', 'R'])

    sns.despine(offset=0, bottom=True, trim=False)
    
    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=True) 

    return fig

def plot_FC(False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3, True1, True3, Hub_com1_rank, Hub_com3_rank, com_net1_rank, com_net3_rank, rand1_rank, rand3_rank):
    insilico = np.append(np.array(False1[:-1]), [com_net1[0], Hub_com1[0], rand1])
    ecoli = np.append(np.array(False3[:-1]), [com_net3[0], Hub_com3[0], rand3])
    insilico_rank = np.append(np.array(True1[:-1]), [com_net1_rank[0], Hub_com1_rank[0], rand1_rank])
    ecoli_rank = np.append(np.array(True3[:-1]), [com_net3_rank[0], Hub_com3_rank[0], rand3_rank])
    
    diff1 = np.log2(np.abs(insilico/insilico_rank))
    diff3 = np.log2(np.abs(ecoli/ecoli_rank))
    
    fig, ax = plt.subplots(nrows=2, ncols=1, facecolor='w', edgecolor='k', figsize=(20,10), dpi=75)
    plt.subplots_adjust(hspace=0.4)
    plt.rc('ytick', labelsize=20)
    plt.rc('xtick', labelsize=20)
    
    #Plot bars#
    ax[0].bar(np.arange(0,6), height=diff1[:6], width=0.9, color=['C9','C9','C9','C9','C9','C9'], label='Bayesian')
    ax[0].bar(np.arange(7,10), height=diff1[6:9], width=0.9, color=['C8','C8','C8'], label='Correlation')
    ax[0].bar(np.arange(11,16), height=diff1[9:14], width=0.9, color=['C7','C7','C7','C7','C7'], label='MI')
    ax[0].bar(np.arange(17,22), height=diff1[14:19], width=0.9, color=['lightpink','lightpink','lightpink','lightpink','lightpink'], label='Meta')
    ax[0].bar(np.arange(23,31), height=diff1[19:27], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Other')
    ax[0].bar(np.arange(32,40), height=diff1[27:35], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Regression')
    ax[0].bar(np.arange(41,42), height=diff1[35], width=0.9, color=['k'], label='Community network (w replacement)')
    ax[0].bar(np.arange(42,43), height=diff1[36], width=0.9, color=['r'], label='Hub community')
    ax[0].bar(np.arange(43,44), height=0, width=0.9, color=['k'], label='random')

    ax[1].bar(np.arange(0,6), height=diff3[:6], width=0.9, color=['C9','C9','C9','C9','C9','C9'], label='Bayesian')
    ax[1].bar(np.arange(7,10), height=diff3[6:9], width=0.9, color=['C8','C8','C8'], label='Correlation')
    ax[1].bar(np.arange(11,16), height=diff3[9:14], width=0.9, color=['C7','C7','C7','C7','C7'], label='MI')
    ax[1].bar(np.arange(17,22), height=diff3[14:19], width=0.9, color=['lightpink','lightpink','lightpink','lightpink','lightpink'], label='Meta')
    ax[1].bar(np.arange(23,31), height=diff3[19:27], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Other')
    ax[1].bar(np.arange(32,40), height=diff3[27:35], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Regression')
    ax[1].bar(np.arange(41,42), height=diff3[35], width=0.9, color=['k'], label='Community network (w replacement)')
    ax[1].bar(np.arange(42,43), height=diff3[36], width=0.9, color=['r'], label='Hub community')
    ax[1].bar(np.arange(43,44), height=0, width=0.9, color=['k'], label='random')
  
    #settings#
    ax[0].axhline(0, color='k', linewidth=1) #Plot line at 0
    ax[0].set_ylabel('log2|FC|',fontsize=28)
    #ax[0].set_yticks([0,0.5,1])
    ax[0].set_title('in silico', fontsize=36)
    ax[1].axhline(0, color='k', linewidth=1) #Plot line at 0
    ax[1].set_ylabel('log2|FC|',fontsize=28)
    #ax[1].set_yticks([0.0,0.4,0.8])
    ax[1].set_title('E. coli', fontsize=36)
    
    plt.setp(ax, xticks=[2,8,13,19,26.5,35.5,41,42,43], xticklabels=['Bayesian','Corrrelation','MI','Meta','Other','Regression', 'C', 'H' ,'R'])
    
    sns.despine(offset=0, bottom=True, trim=False)
    
    ax[0].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off
    ax[1].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off
    
    return fig

def read_outdegree_data():
    #Read data#
    False1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_1_2019-02-20_11:19.csv', index_col=0)
    False1 = False1.loc[:,'PCC_2000_OD']
    Hub_com1 = Hub_com('1',2000,False)
    com_net1 = com_net('1',2000,False)
    rand1 = random_PCC('1', False, 2000)

    False3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_3_2019-02-20_11:20.csv', index_col=0)
    False3 = False3.loc[:,'PCC_80000_OD']
    Hub_com3 = Hub_com('3',80000,False)
    com_net3 = com_net('3',80000,False)
    rand3 = random_PCC('3', False, 80000)

    return False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3

def read_outdegree_data_yeast():
    #Read data#
    False4 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_4_2019-02-20_11:21.csv', index_col=0)
    False4 = False4.loc[:,'PCC_3000_OD']
    Hub_com4 = Hub_com('4',3000,False)
    com_net4 = com_net('4',3000,False)
    rand4 = random_PCC('1', False, 2000)

    return False4, Hub_com4, com_net4, rand4
    
def read_rank_data():
    #Read data#
    False1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_True_1_2019-02-21_13:32.csv', index_col=0)
    False1 = False1.loc[:,'PCC_2000_OD']
    Hub_com1 = Hub_com('1',2000,True)
    com_net1 = com_net('1',2000,True)
    rand1 = random_PCC('1', True, 2000)

    False3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_True_3_2019-02-21_13:33.csv', index_col=0)
    False3 = False3.loc[:,'PCC_80000_OD']
    Hub_com3 = Hub_com('3',80000,True)
    com_net3 = com_net('3',80000,True)
    rand3 = random_PCC('3', True, 80000)

    return False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3
    
def main():

    #Outdegree
    False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3 = read_outdegree_data()
    fig=plot_boxes(False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3)
    fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Method_performance_article.png', dpi=200, bbox_inches="tight")
    
    #rank
    False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3 = read_rank_data()
    fig=plot_boxes(False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3)
    fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Method_performance_rank_article.png', dpi=200, bbox_inches="tight")
    
    #Plot fold change
    False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3 = read_outdegree_data()
    True1, True3, Hub_com1_rank, Hub_com3_rank, com_net1_rank, com_net3_rank, rand1_rank, rand3_rank = read_rank_data()
    fig=plot_FC(False1, False3, Hub_com1, Hub_com3, com_net1, com_net3, rand1, rand3, True1, True3, Hub_com1_rank, Hub_com3_rank, com_net1_rank, com_net3_rank, rand1_rank, rand3_rank)
    fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Method_performance_FC_article.png', dpi=200, bbox_inches="tight")

    #S.cerevisiae
    False4, Hub_com4, com_net4, rand4 = read_outdegree_data_yeast()
    fig = plot_boxes_yeast(False4, Hub_com4, com_net4, rand4)
    fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Method_performance_yeast_article.png', dpi=200, bbox_inches="tight")
    
    
    return

if __name__ == "__main__":
    main()