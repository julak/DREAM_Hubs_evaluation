#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 10:37:31 2019

@author: bioinf
"""
for method in Methods:
    
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('font', size=32)
    plt.rc('ytick', labelsize=24)
    plt.rc('xtick', labelsize=24)
    plt.title(method)  
  
    plt.hist(TF_outdegree.loc[:,method].sort_values())
    
    sns.despine()
    
    fig.savefig('/home/bioinf/Documents/Hub_project/Results/histogram/insilico_'+method+'.png', dpi=200, bbox_inches="tight")
    
#diff3 = np.log2(np.abs((False3-True3)/True3))
#diff1 = np.log2(np.abs((False1-True1)/True1))
diff3 = np.log2(np.abs(False3/True3))
diff1 = np.log2(np.abs(False1/True1))


fig, ax = plt.subplots(nrows=2, ncols=1, facecolor='w', edgecolor='k', figsize=(20,10), dpi=75)
plt.subplots_adjust(hspace=0.4)
plt.rc('ytick', labelsize=20)
plt.rc('xtick', labelsize=20)

#Plot bars#
ax[0].bar(np.arange(0,6), height=np.array(diff1)[:6], width=0.9, color=['C0','C0','C0','C0','C0','C0'], label='Bayesian')
ax[0].bar(np.arange(7,10), height=np.array(diff1)[6:9], width=0.9, color=['C1','C1','C1'], label='Correlation')
ax[0].bar(np.arange(11,16), height=np.array(diff1)[9:14], width=0.9, color=['C2','C2','C2','C2','C2'], label='MI')
ax[0].bar(np.arange(17,22), height=np.array(diff1)[14:19], width=0.9, color=['C3','C3','C3','C3','C3'], label='Meta')
ax[0].bar(np.arange(23,31), height=np.array(diff1)[19:27], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Other')
ax[0].bar(np.arange(32,40), height=np.array(diff1)[27:35], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Regression')
ax[0].bar(np.arange(41,42), height=np.array(diff1)[35], width=0.9, color=['grey'], label='DREAM Community (w/o replacement)')

ax[1].bar(np.arange(0,6), height=np.array(diff3)[:6], width=0.9, color=['C0','C0','C0','C0','C0','C0'], label='Bayesian')
ax[1].bar(np.arange(7,10), height=np.array(diff3)[6:9], width=0.9, color=['C1','C1','C1'], label='Correlation')
ax[1].bar(np.arange(11,16), height=np.array(diff3)[9:14], width=0.9, color=['C2','C2','C2','C2','C2'], label='MI')
ax[1].bar(np.arange(17,22), height=np.array(diff3)[14:19], width=0.9, color=['C3','C3','C3','C3','C3'], label='Meta')
ax[1].bar(np.arange(23,31), height=np.array(diff3)[19:27], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Other')
ax[1].bar(np.arange(32,40), height=np.array(diff3)[27:35], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Regression')
ax[1].bar(np.arange(41,42), height=np.array(diff3)[35], width=0.9, color=['grey'], label='DREAM Community (w/o replacement)')

#settings#
ax[0].axhline(0, color='k', linewidth=1) #Plot line at 0
ax[0].set_ylabel('log2|FC|',fontsize=28)
#ax[0].set_yticks([0,0.5,1])
ax[0].set_title('in silico', fontsize=36)
ax[1].axhline(0, color='k', linewidth=1) #Plot line at 0
ax[1].set_ylabel('log2|FC|',fontsize=28)
#ax[1].set_yticks([0.0,0.4,0.8])
ax[1].set_title('E. coli', fontsize=36)

plt.setp(ax, xticks=[2,8,13,19,26.5,35.5,41], xticklabels=['Bayesian','Corrrelation','MI','Meta','Other','Regression', 'D'])

sns.despine(offset=0, bottom=True, trim=False)

ax[0].tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=True) # labels along the bottom edge are off
ax[1].tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=True) # labels along the bottom edge are off

fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Fold_change_outdergee_rank.png', dpi=200, bbox_inches="tight")
