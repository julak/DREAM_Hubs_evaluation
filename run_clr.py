#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 08:50:48 2018

@author: bioinf
"""

import os
import pandas as pd
from sklearn.metrics import mutual_info_score
import numpy as np
from oct2py import octave

def calc_MI(x, y, bins):
    c_xy = np.histogram2d(x, y, bins)[0]
    mi = mutual_info_score(None, None, contingency=c_xy)
    return mi

home_folder = '/home/bioinf/Documents/Hub_project/'

os.chdir(home_folder)        

Exp = pd.read_table('Dream5/DREAM5_network_inference_challenge/Network1/input data/net1_expression_data.tsv')
TF = pd.read_csv('Dream5/DREAM5_network_inference_challenge/Network1/input data/net1_transcription_factors.tsv', sep='\t', header=None)

MIfull = pd.DataFrame(columns=Exp.columns, index=Exp.columns)
for i in Exp.columns:
    for j in Exp.columns:
        MI = calc_MI(x=Exp.loc[:,i], y=Exp.loc[:,j], bins=10)
        MIfull.loc[i,j] = MI

MImatrix = np.array(MIfull)
genes = MIfull.columns

##Matlab part##
#MImatrix_file = '/home/bioinf/Documents/Hub_project/Run_methods/test_clr.tsv';
#gene_file = '/home/bioinf/Documents/Hub_project/Run_methods/test_clr_genes.tsv';

#MImatrix = importdata(expression_file);
#Genes = importdata(gene_file);

os.chdir(home_folder+'Run_methods/CLRv1.2.2/Code/')

[A, MIfull] = octave.clr(MImatrix,'normal',10,3);

###

CLR=pd.DataFrame(A, columns = genes, index = genes)

TFs = np.array(TF).flatten()
TFs = [str(x) for x in TFs]

CLR_dir = CLR.loc[TFs,:] #Only keep rows that are TFs to get directed network, see above.

TF = list(np.repeat(CLR_dir.index, len(CLR_dir.columns)))
targets = list(np.tile(CLR_dir.columns, len(CLR_dir.index)))
value = list(np.array(CLR_dir).flatten())
Net = pd.DataFrame(np.transpose([TF,targets,value]))
Net.columns = ['TF','target','confidence']
Net_sort = Net.sort_values(by='confidence', ascending=False).iloc[:100000,:]
    
# almost get the same result as in DREAM5 (only checked top interactions) only difference is that TF-TF interactions have direction. 
# Seem almost as they only have taken lowest TF to highest TF. 

os.chdir(home_folder+'Run_methods/')        

Net_sort.to_csv('CLR_Net.csv', index=False, header=None)
