#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 13:16:07 2018

@author: bioinf
"""
import pandas as pd
import numpy as np
import scipy.stats as sts
import sklearn.preprocessing as skp
import matplotlib.pyplot as plt
import seaborn as sns
import datetime

def Pearson_corr(x,Ymat):
    P = []
    Pval = []
    for i in Ymat:
        y = Ymat.loc[:,i]
        Pcof = sts.pearsonr(x, y)
        P.append(Pcof[0])
        Pval.append(Pcof[1])
    P = pd.DataFrame([P,Pval], columns = Ymat.columns, index = ['Pearson', 'pval']).T
    P = P.sort_values(by='Pearson')
    return P

def Count_fun(len_Network, method, name, gold_genes):
    #Counts outdegree of all TFs in predicted network from method i.
    
    Method_Network = pd.read_table(name, header=None)
    if len(Method_Network)>len_Network:
        Method_Network = Method_Network.iloc[:len_Network,:]
    Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,0], gold_genes)] 
    Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,1], gold_genes)] 

    TF_outdegree = pd.DataFrame(Method_Network.groupby(Method_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = [method])
    return TF_outdegree

def count_outdegree(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree):
    #Counts outdegree of all TFs in predicted networks from all methods.
    
    TF_outdegree_all = pd.DataFrame()
    for method in Methods:
        if method in ots_methods:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Off-the-shelf methods/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'
        elif method == 'community':
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Community integration/DREAM5_NetworkInference_Community_Network' + Network + '.txt'
        else:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Challenge participants/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'

        TF_outdegree = Count_fun(len_Network, method=method, name=name, gold_genes=gold_genes)    
        TF_outdegree_all = TF_outdegree_all.join(TF_outdegree, how='outer')
    
    #Add hub communities
        #TFcount_rank = add_community(TF_outdegree_all)    
        
    #Gold standard outdegree        
    GS = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    GS_outdegree = pd.DataFrame(GS.groupby(GS.loc[:,0].tolist()).size().sort_values(ascending=False), columns = ['GS'])
    TF_outdegree_all = TF_outdegree_all.join(GS_outdegree, how='outer')
    
    TF_outdegree_all = TF_outdegree_all.fillna(0).astype(int) 
    
    if rank_outdegree == True:
        TF_outdegree_all = TF_outdegree_all.rank(axis=0, method='average', ascending=True)
    elif rank_outdegree == 'Norm':
        TF_outdegree_all = TF_outdegree_all.div(np.max(TF_outdegree_all, axis=0)) 
        
    return TF_outdegree_all

def inverse_participation_ratio(TF_score, method):
    a = TF_score #adjacency matrix, aij = weight of edge (i,j)
    s = np.sum(a.T) #sum of weight of TF edges
    #Y = np.sum(np.square(a.divide(s, axis='index')), axis='columns') #inverse participation ratio for each TF
    #IPR = pd.DataFrame(np.divide(1,Y), index=TF_score.index, columns=[method]) #a high value should be a hub.
    IPR=pd.DataFrame(s,columns=[method])
    return IPR

def count_IPR(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree):
    
    IPR_all = pd.DataFrame()
    for method in Methods:
        TF_score = pd.read_csv('/home/bioinf/Documents/Exjobb/Networks/Networks' + Network + '_' + method + '.csv', index_col=0)
        TF_score = TF_score.iloc[np.isin(TF_score.index,gold_genes),np.isin(TF_score.columns,gold_genes)]

        #Edge cutoff
        Edges = pd.DataFrame(np.array(TF_score).flatten())
        Edge_cutoff = float(Edges[Edges>0].dropna().nlargest(len_Network, columns=0).min())
        TF_score[TF_score < Edge_cutoff] = 0
        
        TF_score = TF_score.loc[~(TF_score==0).all(axis=1)]
        
        #L2 normalisation
        TF_score = pd.DataFrame(skp.normalize(TF_score, norm='l2', axis=1, copy=True, return_norm=False), index=TF_score.index, columns=TF_score.columns)

        IPR = inverse_participation_ratio(TF_score, method)
        IPR_all = IPR_all.join(IPR, how='outer')  
    
    #Add hub communities
        #TFcount_rank = add_community(TF_outdegree_all)  
    
    #Gold standard outdegree        
    GS = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    GS_outdegree = pd.DataFrame(GS.groupby(GS.loc[:,0].tolist()).size().sort_values(ascending=False), columns = ['GS'])
    IPR_all = IPR_all.join(GS_outdegree, how='outer')
    
    IPR_all = IPR_all.fillna(0).astype(int) 
    
    #Rank outdegree
    if rank_outdegree == True:
        IPR_all = IPR_all.rank(axis=0, method='average', ascending=True)
    elif rank_outdegree == 'Norm':
        IPR_all = IPR_all.div(IPR_all.max()) 
        
    return IPR_all

def loop_count_outdegree(outdegree_function, Network, Methods, ots_methods, Goldstandard, gold_Network, gold_genes, rank_outdegree):
    
    PCC_interactions = pd.DataFrame()
    for len_Network in [1000, 2000, 3000, 4000, 5000, 7000, 10000, 15000, 20000, 50000, 80000, 100000]:
        print(len_Network)

        TF_outdegree_all = outdegree_function(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree)

        #Pearson correlation coefficient
        PCC = Pearson_corr(x=TF_outdegree_all.loc[:,'GS'], Ymat=TF_outdegree_all)
        PCC.columns = ['PCC_' + str(len_Network),'pval_' + str(len_Network)]
        
        PCC_interactions = PCC_interactions.join(PCC, how='outer')
    
    return PCC_interactions

def plot_heatmap(PCC, Mask):
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)

    plt.rc('ytick', labelsize=10)
    plt.rc('xtick', labelsize=12) 
    sns.heatmap(PCC, cmap='seismic', center=0, mask=Mask)
    return fig 
            
def main():
    
    Network='3'
    rank_outdegree=True
    
    folder = '/home/bioinf/Documents/Hub_project/Results/'
    now = datetime.datetime.now()

    Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
        'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
    ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
    Goldstandard = 'Network' + Network    
    gold_Network = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    
    gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()    
    
    #PCC between outdegree in predicted network and GS. 
    PCC_outdegree = loop_count_outdegree(count_outdegree, Network, Methods, ots_methods, Goldstandard, gold_Network, gold_genes, rank_outdegree)
    PCC_outdegree.columns = [col+'_OD' for col in PCC_outdegree]
    #PCC_IPR = loop_count_outdegree(count_IPR, Network, Methods, ots_methods, Goldstandard, gold_Network, gold_genes, rank_outdegree)
    #PCC_IPR.columns = [col+'_IPR' for col in PCC_IPR]
    
    PCC = PCC_outdegree#.join(PCC_IPR)
    
    #DREAM scores
    DREAM_scores = pd.read_table('/home/bioinf/Documents/Hub_project/Dream5/Scores.txt', index_col=0).loc[:,['AUROC_Net' + Network, 'AUPR_Net' + Network]]
    DREAM_scores = DREAM_scores.rename(columns={'AUROC_Net' + Network:'AUROC', 'AUPR_Net' + Network:'AUPR'})
    DREAM_scores = DREAM_scores.rename({'Community_RankSum' : 'community'})
    PCC = PCC.join(DREAM_scores)
    
    PCC = PCC.drop('GS')
    
    PCC.to_csv(folder+'PCC_ind_methods/PCC_methods_'+str(rank_outdegree)+'_'+Network+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.csv')

    #Plot
#    Mask = (PCC > 0.05)
#    Mask = Mask.loc[:,[col for col in PCC if col.startswith('pval')]+['AUPR']]
#    Mask.AUPR = False
#    Mask.columns = [col for col in PCC if col.startswith('PCC')]+['AUPR']
#    
#    PCC = PCC.loc[:,[col for col in PCC if col.startswith('PCC')]+['AUPR']]
#
#    fig = plot_heatmap(PCC, Mask)
#    
#    fig.savefig(folder+'PCC_heatmap_outdegree_'+str(rank_outdegree)+'_'+Network+'_'+now.strftime("%Y-%m-%d_%H:%M")+'.png', dpi=200, bbox_inches="tight")

if __name__ == "__main__":
    main()