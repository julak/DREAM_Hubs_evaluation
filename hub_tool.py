#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 15:58:21 2018

@author: bioinf
"""

def edge_cutoff(Network, Methods, range_len_Network=[1000, 3000, 5000, 10000, 20000, 50000, 100000]):
    
    PCC_all = pd.DataFrame()
    for len_Network in range_len_Network:
        
        TF_outdegree = Get_TF_outdegree(count_outdegree, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree)
        
        Methods_PCC = pd.DataFrame(Pearson_corr(TF_outdegree.GS, TF_outdegree).Pearson)
        Methods_PCC.columns=[len_Network]

        PCC = pd.DataFrame(random_commmunity(Network, Methods, TF_outdegree), columns=[len_Network])
        PCC = PCC.append(Methods_PCC)
        PCC_all = PCC_all.join(PCC, how='outer')
    
    edge_cutoff = PCC_all.mean().argmax()
    return


def count_TF_outdegree(predicted_networks, edge_cutoff):
    
    return TF_outdegree


def community_network(edge_cutoff):
        
    return


def community_hubs(edge_cutoff):
    
    return


def validate_hubs():
    
    return correlation_scores