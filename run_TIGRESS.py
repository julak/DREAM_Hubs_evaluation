#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 16:10:05 2018

@author: bioinf
"""

import os
import pandas as pd
import numpy as np
from oct2py import octave

home_folder = '/home/bioinf/Documents/Hub_project/'

os.chdir(home_folder)        

Exp = pd.read_table('Dream5/DREAM5_network_inference_challenge/Network1/input data/net1_expression_data.tsv')
TF = pd.read_csv('Dream5/DREAM5_network_inference_challenge/Network1/input data/net1_transcription_factors.tsv', sep='\t', header=None)

data = np.array(Exp)
genenames = np.array(Exp.columns) 
tf_index = [i for i, j in enumerate(genenames) if j in np.array(TF).flatten()]

data = read_data(datapath,networkname);
TF_file = strcat([datapath,'/',networkname,'_transcription_factors.tsv']);
data.tflist = transpose(importdata(TF_file));
[tf,loc]=ismember(data.genenames,data.tflist);
idx=[1:length(data.genenames)];
idx=idx(tf);
idx=idx(loc(tf));
data.tf_index = idx;


##TIGRESS part in matlab##
os.chdir(home_folder+'Run_methods/TIGRESS-PKG-2.1/src/run_tigress/')

freq = octave.tigress(data,'R',1000,'alpha',0.2,'L',5,'verbose',true);
scores= octave.score_edges(freq,'method','area','L',5);
#save_file = strcat(['./TIGRESS_', networkname, '_DREAM5parameters.txt'])
edges= octave.predict_network(scores,data.tf_index,'cutoff',100000,'genenames',data.genenames,'name_net',save_file)