#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 15:44:28 2019

@author: bioinf
"""

Exp1 = pd.read_table('/home/bioinf/Documents/Hub_project/Dream5/DREAM5_network_inference_challenge/Network1/input data/net1_expression_data.tsv')
Exp3 = pd.read_table('/home/bioinf/Documents/Hub_project/Dream5/DREAM5_network_inference_challenge/Network3/input data/net3_expression_data.tsv')
Exp4 = pd.read_table('/home/bioinf/Documents/Hub_project/Dream5/DREAM5_network_inference_challenge/Network4/input data/net4_expression_data.tsv')
HPA = pd.read_table('/home/bioinf/Documents/Hub_project/HPA/HPAznorm2_expression_data.tsv')


np.linalg.cond(Exp1)
np.linalg.cond(Exp3)
np.linalg.cond(Exp4)
np.linalg.cond(HPA)