#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 09:00:21 2018

@author: bioinf
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import random
import scipy.stats as sts
import seaborn as sns

def Count_fun(len_Network, method, name, gold_genes):
    #Counts outdegree of all TFs in predicted network from method i.
    
    Method_Network = pd.read_table(name, header=None)
    Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,0], gold_genes)] 
    Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,1], gold_genes)] 
    if len(Method_Network)>len_Network:
        Method_Network = Method_Network.iloc[:len_Network,:]

    TF_outdegree = pd.DataFrame(Method_Network.groupby(Method_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = [method])
    return TF_outdegree

def count_outdegree(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree):
    #Counts outdegree of all TFs in predicted networks from all methods.
    
    TF_outdegree_all = pd.DataFrame()
    for method in Methods:
        if method in ots_methods:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Off-the-shelf methods/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'
        elif method == 'community':
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Community integration/DREAM5_NetworkInference_Community_Network' + Network + '.txt'
        else:
            name = '/home/bioinf/Documents/Exjobb/Dream5/Network_predictions/Challenge participants/'+ method + '/DREAM5_NetworkInference_' + method + '_Network' + Network + '.txt'

        TF_outdegree = Count_fun(len_Network, method=method, name=name, gold_genes=gold_genes)    
        TF_outdegree_all = TF_outdegree_all.join(TF_outdegree, how='outer')
    
    #Add hub communities
        #TFcount_rank = add_community(TF_outdegree_all)    
        
    #Gold standard outdegree        
    GS = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    GS_outdegree = pd.DataFrame(GS.groupby(GS.loc[:,0].tolist()).size().sort_values(ascending=False), columns = ['GS'])
    TF_outdegree_all = TF_outdegree_all.join(GS_outdegree, how='outer')
    
    TF_outdegree_all = TF_outdegree_all.fillna(0).astype(int) 
    
    if rank_outdegree == True:
        TF_outdegree_all = TF_outdegree_all.rank(axis=0, method='average', ascending=True)
    elif rank_outdegree == 'Norm':
        TF_outdegree_all = TF_outdegree_all.div(np.max(TF_outdegree_all, axis=0)) 
        
    return TF_outdegree_all

def Get_TF_outdegree(outdegree_function, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree):
    TF_outdegree = outdegree_function(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree) 
    return TF_outdegree

def random_PCC(Network, rank_outdegree, len_Network):
    
    Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3', 'Meta1','Meta2','Meta3','Meta4','Meta5','MI1', 'MI2', 'MI3','MI4','MI5','Other1','Other2','Other3','Other4',
        'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
    ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
    Goldstandard = 'Network' + Network    
    gold_Network = pd.read_table('/home/bioinf/Documents/Hub_project/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()
    
    #count_outdegree or count_IPR                
    TF_outdegree = Get_TF_outdegree(count_outdegree, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree)

    GS=TF_outdegree.loc[:,'GS']
    
    random.sample(list(np.array(TF_outdegree.iloc[:,:-1]).flatten()), len(GS))
    
    Pmat=[]
    for i in range(100):
        #X = np.arange(0, len(GS))
        #random.shuffle(X)
        X = random.sample(list(np.array(TF_outdegree.iloc[:,:-1]).flatten()), len(GS))
        Pcof = sts.pearsonr(X, GS)
        Pmat.append(Pcof[0])
    
    return np.mean(Pmat)

def plot_boxes(False1, False3, False4, Hub_com1, Hub_com3, Hub_com4, Hub_com10, Hub_com30, Hub_com40, Hub_com1gr, Hub_com10gr, Hub_com3gr, Hub_com30gr, com_net1, com_net3, com_net4, com_net10, com_net30, com_net40, rand1, rand3, rand4, include4):
    
#    meanFalse = np.divide(False1, False1.max())+np.divide(False3, False3.max())
#    Hub_com_mean = np.divide(Hub_com1, False1.max())+np.divide(Hub_com3, False3.max())
#    com_net_mean = np.divide(com_net1, False1.max())+np.divide(com_net3, False3.max())
#    rand_mean = np.divide(rand1, False1.max())+np.divide(rand3, False3.max())

    meanFalse = False1+False3
    Hub_com_mean = Hub_com1+Hub_com3
    com_net_mean = com_net1+com_net3
    Hub_com_mean0 = Hub_com10+Hub_com30
    com_net_mean0 = com_net10+com_net30
    rand_mean = rand1+rand3
    
    if include4:
        fig, ax = plt.subplots(nrows=4, ncols=1, facecolor='w', edgecolor='k', figsize=(20,20), dpi=75)
    else:
        fig, ax = plt.subplots(nrows=3, ncols=1, facecolor='w', edgecolor='k', figsize=(20,15), dpi=75)
    plt.subplots_adjust(hspace=0.4)
    plt.rc('ytick', labelsize=20)
    plt.rc('xtick', labelsize=20)

    #Plot bars#
    ax[0].bar(np.arange(0,6), height=np.array(meanFalse)[:6], width=0.9, color=['C0','C0','C0','C0','C0','C0'], label='Bayesian')
    ax[0].bar(np.arange(7,10), height=np.array(meanFalse)[6:9], width=0.9, color=['C1','C1','C1'], label='Correlation')
    ax[0].bar(np.arange(11,16), height=np.array(meanFalse)[9:14], width=0.9, color=['C2','C2','C2','C2','C2'], label='MI')
    ax[0].bar(np.arange(17,22), height=np.array(meanFalse)[14:19], width=0.9, color=['C3','C3','C3','C3','C3'], label='Meta')
    ax[0].bar(np.arange(23,31), height=np.array(meanFalse)[19:27], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Other')
    ax[0].bar(np.arange(32,40), height=np.array(meanFalse)[27:35], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Regression')
    ax[0].bar(np.arange(41,42), height=np.array(meanFalse)[35], width=0.9, color=['grey'], label='DREAM Community (w/o replacement)')
    ax[0].bar(np.arange(42,43), height=Hub_com_mean, width=0.9, color=['k'], label='Hub community')
    ax[0].bar(np.arange(43,44), height=com_net_mean, width=0.9, color=['k'], label='Community network (w replacement)')
    #ax[0].bar(np.arange(42,43), height=Hub_com_mean0, width=0.9, color=['C0'], label='Hub community')
    #ax[0].bar(np.arange(43,44), height=com_net_mean0, width=0.9, color=['C0'], label='Community network (w replacement)')
    ax[0].bar(np.arange(44,45), height=rand_mean, width=0.9, color=['r'], label='random')
    
    ax[1].bar(np.arange(0,6), height=np.array(False1)[:6], width=0.9, color=['C0','C0','C0','C0','C0','C0'], label='Bayesian')
    ax[1].bar(np.arange(7,10), height=np.array(False1)[6:9], width=0.9, color=['C1','C1','C1'], label='Correlation')
    ax[1].bar(np.arange(11,16), height=np.array(False1)[9:14], width=0.9, color=['C2','C2','C2','C2','C2'], label='MI')
    ax[1].bar(np.arange(17,22), height=np.array(False1)[14:19], width=0.9, color=['C3','C3','C3','C3','C3'], label='Meta')
    ax[1].bar(np.arange(23,31), height=np.array(False1)[19:27], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Other')
    ax[1].bar(np.arange(32,40), height=np.array(False1)[27:35], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Regression')
    ax[1].bar(np.arange(41,42), height=np.array(False1)[35], width=0.9, color=['grey'], label='DREAM Community (w/o replacement)')
    ax[1].bar(np.arange(42,43), height=Hub_com1, width=0.9, color=['k'], label='Hub community')
    ax[1].bar(np.arange(44,45), height=Hub_com1gr, width=0.9, color=['k'], label='Hub community')
    ax[1].bar(np.arange(43,44), height=com_net1, width=0.9, color=['k'], label='Community network (w replacement)')
    #ax[1].bar(np.arange(42,43), height=Hub_com10, width=0.9, color=['C0'], label='Hub community')
    #ax[1].bar(np.arange(44,45), height=Hub_com10gr, width=0.9, color=['C0'], label='Hub community')
    #ax[1].bar(np.arange(43,44), height=com_net10, width=0.9, color=['C0'], label='Community network (w replacement)')
    ax[1].bar(np.arange(44,45), height=rand1, width=0.9, color=['r'], label='random')

    ax[2].bar(np.arange(0,6), height=np.array(False3)[:6], width=0.9, color=['C0','C0','C0','C0','C0','C0'], label='Bayesian')
    ax[2].bar(np.arange(7,10), height=np.array(False3)[6:9], width=0.9, color=['C1','C1','C1'], label='Correlation')
    ax[2].bar(np.arange(11,16), height=np.array(False3)[9:14], width=0.9, color=['C2','C2','C2','C2','C2'], label='MI')
    ax[2].bar(np.arange(17,22), height=np.array(False3)[14:19], width=0.9, color=['C3','C3','C3','C3','C3'], label='Meta')
    ax[2].bar(np.arange(23,31), height=np.array(False3)[19:27], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Other')
    ax[2].bar(np.arange(32,40), height=np.array(False3)[27:35], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Regression')
    ax[2].bar(np.arange(41,42), height=np.array(False3)[35], width=0.9, color=['grey'], label='DREAM Community (w/o replacement)')
    ax[2].bar(np.arange(42,43), height=Hub_com3, width=0.9, color=['k'], label='Hub community')
    ax[2].bar(np.arange(44,45), height=Hub_com3gr, width=0.9, color=['k'], label='Hub community')
    ax[2].bar(np.arange(43,44), height=com_net3, width=0.9, color=['k'], label='Community network (w replacement)')
    #ax[2].bar(np.arange(42,43), height=Hub_com30, width=0.9, color=['C0'], label='Hub community')
    #ax[2].bar(np.arange(44,45), height=Hub_com30gr, width=0.9, color=['C0'], label='Hub community')
    #ax[2].bar(np.arange(43,44), height=com_net30, width=0.9, color=['C0'], label='Community network (w replacement)')
    ax[2].bar(np.arange(44,45), height=rand3, width=0.9, color=['r'], label='random')
    
    if include4:
        ax[3].bar(np.arange(0,6), height=np.array(False4)[:6], width=0.9, color=['C0','C0','C0','C0','C0','C0'], label='Bayesian')
        ax[3].bar(np.arange(7,10), height=np.array(False4)[6:9], width=0.9, color=['C1','C1','C1'], label='Correlation')
        ax[3].bar(np.arange(11,16), height=np.array(False4)[9:14], width=0.9, color=['C2','C2','C2','C2','C2'], label='MI')
        ax[3].bar(np.arange(17,22), height=np.array(False4)[14:19], width=0.9, color=['C3','C3','C3','C3','C3'], label='Meta')
        ax[3].bar(np.arange(23,31), height=np.array(False4)[19:27], width=0.9, color=['C4','C4','C4','C4','C4','C4','C4','C4'], label='Other')
        ax[3].bar(np.arange(32,40), height=np.array(False4)[27:35], width=0.9, color=['C5','C5','C5','C5','C5','C5','C5','C5'], label='Regression')
        ax[3].bar(np.arange(41,42), height=np.array(False4)[35], width=0.9, color=['grey'], label='DREAM Community (w/o replacement)')
        ax[3].bar(np.arange(42,43), height=Hub_com4, width=0.9, color=['k'], label='Hub community')
        ax[3].bar(np.arange(43,44), height=com_net4, width=0.9, color=['k'], label='Community network (w replacement)')
        ax[3].bar(np.arange(44,45), height=rand4, width=0.9, color=['r'], label='random')

    #settings#
    ax[0].axhline(0, color='k', linewidth=1) #Plot line at 0
    #ax[0].axhline(Hub_com_mean, color='k', linewidth=1, linestyle='--') #Plot line at 0
    #ax[0].axhline(Hub_com_mean0, color='C0', linewidth=1, linestyle='--') #Plot line at 0
    ax[0].set_ylabel('Overall score',fontsize=28)
    ax[0].set_yticks([0,0.5,1])
    ax[0].set_title('Overall', fontsize=36)
    ax[1].axhline(0, color='k', linewidth=1) #Plot line at 0
    ax[1].axhline(Hub_com1gr, color='k', linewidth=1, linestyle='--') #Plot line at 0
    #ax[1].axhline(Hub_com10gr, color='C0', linewidth=1, linestyle='--') #Plot line at 0
    ax[1].set_ylabel('PCC',fontsize=28)
    ax[1].set_yticks([0.0,0.4,0.8])
    ax[1].set_title('in silico', fontsize=36)
    ax[2].axhline(0, color='k', linewidth=1) #Plot line at 0
    ax[2].axhline(Hub_com3gr, color='k', linewidth=1, linestyle='--') #Plot line at 0
    #ax[2].axhline(Hub_com30gr, color='C0', linewidth=1, linestyle='--') #Plot line at 0
    ax[2].set_ylabel('PCC',fontsize=28)
    ax[2].set_yticks([0.0,0.25,0.5])
    ax[2].set_title('E. coli', fontsize=36)
    if include4:
        ax[3].axhline(0, color='k', linewidth=1) #Plot line at 0
        ax[3].axhline(Hub_com4, color='k', linewidth=1, linestyle='--') #Plot line at 0
        ax[3].set_ylabel('PCC',fontsize=28)
        ax[3].set_yticks([0.0,0.25,0.5])
        ax[3].set_title('S. serevisiae', fontsize=36)
        
    
    #plt.legend(frameon=False, bbox_to_anchor=(1.21, 1.9))
    #plt.setp(ax, xticks=np.arange(42), xticklabels=['1','2','3','4','5','6','','1','2','3','','1','2','3','4','5','','1','2','3','4','5','','1','2','3','4','5','6','7','8','','1','2','3','4','5','6','7','8','','C'])
    plt.setp(ax, xticks=[2,8,13,19,26.5,35.5,41,42,43,44], xticklabels=['Bayesian','Corrrelation','MI','Meta','Other','Regression', 'D', 'H', 'C', 'R'])
    
    sns.despine(offset=0, bottom=True, trim=False)
    
    ax[0].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off
    ax[1].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off
    ax[2].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off
    if include4:
        ax[3].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off

    return fig

def read_outdegree_data():
    #Read data#
    False1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_1_2019-02-20_11:19.csv', index_col=0)
    False1 = False1.loc[:,'PCC_2000_OD']
    Hub_com_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2019-02-20_14:22/1_Hub_community_2000_False.csv', index_col=0)
    Hub_com1 = Hub_com_False_1.mean(axis=1).iloc[-1]
    Hub_com10 = Hub_com_False_1.mean(axis=1).iloc[10]#.iloc[-1]
    Hub_com_False_1gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2019-02-20_11:33/1_Hub_community_2000_False_group.csv', index_col=0)
    Hub_com1gr = Hub_com_False_1gr.mean(axis=1).iloc[-1]
    Hub_com10gr = Hub_com_False_1gr.mean(axis=1).iloc[10]#.iloc[-1]
    com_net_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/1/1_Community_network_False_2018-12-10_10:05.csv', index_col=0)
    com_net1 = com_net_False_1.mean(axis=1).iloc[-1]
    com_net10 = com_net_False_1.mean(axis=1).iloc[10]#.iloc[-1]
    rand1 = random_PCC('1', False, 2000)

    False3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_3_2019-02-20_11:20.csv', index_col=0)
    False3 = False3.loc[:,'PCC_7000_OD']
    Hub_com_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2019-02-20_14:22/3_Hub_community_80000_False.csv', index_col=0)
    Hub_com3 = Hub_com_False_3.mean(axis=1).iloc[-1]
    Hub_com30 = Hub_com_False_3.mean(axis=1).iloc[10]#.iloc[-1]
    Hub_com_False_3gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2019-02-20_11:47/3_Hub_community_80000_False_group.csv', index_col=0)
    Hub_com3gr = Hub_com_False_3gr.mean(axis=1).iloc[-1]
    Hub_com30gr = Hub_com_False_3gr.mean(axis=1).iloc[10]#.iloc[-1]
    com_net_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/3/3_Community_network_False_2019-02-20_13:26.csv', index_col=0)
    com_net3 = com_net_False_3.mean(axis=1).iloc[-1]
    com_net30 = com_net_False_3.mean(axis=1).iloc[10]#[-1]
    rand3 = random_PCC('3', False, 7000)
    
    False4 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_4_2019-02-20_11:21.csv', index_col=0)
    False4 = False4.loc[:,'PCC_7000_OD']
    Hub_com_False_4 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/4/2019-02-20_14:23/4_Hub_community_3000_False.csv', index_col=0)
    Hub_com4 = Hub_com_False_4.mean(axis=1).iloc[-1]
    Hub_com40 = Hub_com_False_4.mean(axis=1).iloc[10]#.iloc[-1]
    com_net_False_4 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/4/4_Community_network_False_2019-02-20_13:26.csv', index_col=0)
    com_net4 = com_net_False_4.mean(axis=1).iloc[-1]
    com_net40 = com_net_False_4.mean(axis=1).iloc[10]#.iloc[-1]
    rand4 = random_PCC('4', False, 7000)
    return False1, False3, False4, Hub_com1, Hub_com3, Hub_com4, Hub_com10, Hub_com30, Hub_com40, Hub_com1gr, Hub_com10gr, Hub_com3gr, Hub_com30gr, com_net1, com_net3, com_net4, com_net10, com_net30, com_net40, rand1, rand3, rand4
    
def read_rank_data():
    #Read data#
    False1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_True_1_2018-11-14_14:24.csv', index_col=0)
    False1 = False1.loc[:,'PCC_2000_OD']
    Hub_com_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2019-01-31_14:24_rank/1_Hub_community_2000_True.csv', index_col=0)
    Hub_com1 = Hub_com_False_1.mean(axis=1).iloc[-1]
    Hub_com10 = Hub_com_False_1.mean(axis=1).iloc[10]#.iloc[-1]
    Hub_com_False_1gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2019-02-01_09:28/1_Hub_community_2000_True_groups_rep.csv', index_col=0)
    Hub_com1gr = Hub_com_False_1gr.mean(axis=1).iloc[-1]
    Hub_com10gr = Hub_com_False_1gr.mean(axis=1).iloc[10]#.iloc[-1]
    com_net_False_1 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/1/1_Community_network_True_2019-01-31_22:01.csv', index_col=0)
    com_net1 = com_net_False_1.mean(axis=1).iloc[-1]
    com_net10 = com_net_False_1.mean(axis=1).iloc[10]#.iloc[-1]
    rand1 = random_PCC('1', False, 2000)

    False3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_True_3_2018-11-14_14:24.csv', index_col=0)
    False3 = False3.loc[:,'PCC_7000_OD']
    Hub_com_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2019-01-31_14:46_rank/3_Hub_community_7000_True.csv', index_col=0)
    Hub_com3 = Hub_com_False_3.mean(axis=1).iloc[-1]
    Hub_com30 = Hub_com_False_3.mean(axis=1).iloc[10]#.iloc[-1]
    Hub_com_False_3gr = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2019-02-01_09:29/3_Hub_community_7000_True_groups_rep.csv', index_col=0)
    Hub_com3gr = Hub_com_False_3gr.mean(axis=1).iloc[-1]
    Hub_com30gr = Hub_com_False_3gr.mean(axis=1).iloc[10]#.iloc[-1]
    com_net_False_3 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/3/3_Community_network_True_2019-01-31_22:01.csv', index_col=0)
    com_net3 = com_net_False_3.mean(axis=1).iloc[-1]
    com_net30 = com_net_False_3.mean(axis=1).iloc[10]#[-1]
    rand3 = random_PCC('3', False, 7000)
    
    False4 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_True_4_2019-01-31_14:29.csv', index_col=0)
    False4 = False4.loc[:,'PCC_7000_OD']
    Hub_com_False_4 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Hub_community/4/2019-01-03_09:39_7000/4_Hub_community_7000_False.csv', index_col=0)
    Hub_com4 = Hub_com_False_4.mean(axis=1).iloc[-1]
    Hub_com40 = Hub_com_False_4.mean(axis=1).iloc[10]#.iloc[-1]
    com_net_False_4 = pd.read_csv('/home/bioinf/Documents/Hub_project/Results/Community_network/4/4_Community_network_True_2019-01-31_22:01.csv', index_col=0)
    com_net4 = com_net_False_4.mean(axis=1).iloc[-1]
    com_net40 = com_net_False_4.mean(axis=1).iloc[10]#.iloc[-1]
    rand4 = random_PCC('4', False, 7000)
    return False1, False3, False4, Hub_com1, Hub_com3, Hub_com4, Hub_com10, Hub_com30, Hub_com40, Hub_com1gr, Hub_com10gr, Hub_com3gr, Hub_com30gr, com_net1, com_net3, com_net4, com_net10, com_net30, com_net40, rand1, rand3, rand4
    
def main():

    False1, False3, False4, Hub_com1, Hub_com3, Hub_com4, Hub_com10, Hub_com30, Hub_com40, Hub_com1gr, Hub_com10gr, Hub_com3gr, Hub_com30gr, com_net1, com_net3, com_net4, com_net10, com_net30, com_net40, rand1, rand3, rand4 = read_outdegree_data()
    False1, False3, False4, Hub_com1, Hub_com3, Hub_com4, Hub_com10, Hub_com30, Hub_com40, Hub_com1gr, Hub_com10gr, Hub_com3gr, Hub_com30gr, com_net1, com_net3, com_net4, com_net10, com_net30, com_net40, rand1, rand3, rand4 = read_rank_data()
    #Plot#
    include4 = False
    fig=plot_boxes(False1, False3, False4, Hub_com1, Hub_com3, Hub_com4, Hub_com10, Hub_com30, Hub_com40, Hub_com1gr, Hub_com10gr, Hub_com3gr, Hub_com30gr, com_net1, com_net3, com_net4, com_net10, com_net30, com_net40, rand1, rand3, rand4, include4)
    
    if include4:
        fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Method_performance_1_2000_3_7000_4_7000.png', dpi=200, bbox_inches="tight")
    else:
        fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Method_performance_1_2000_3_7000_overall_groups_rank.png', dpi=200, bbox_inches="tight")
    return

if __name__ == "__main__":
    main()