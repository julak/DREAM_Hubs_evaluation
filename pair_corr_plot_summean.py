#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 09:11:24 2018

@author: bioinf
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

def format_matrix_for_lineplot(A):
    A.columns = [int(x) for x in A.columns]
    A = A.melt()
    A.columns = ['Interactions', 'PCC']
    return A

def plot_fun(pc3, pc1, gs3, gs1, ylabel, title):
    pc3_edge_cutoff = pc3.mean().argmax()#E.coli
    pc1_edge_cutoff = pc1.mean().argmax()#in silico
    
    pc3 = format_matrix_for_lineplot(pc3)
    pc3['Interactions'] = np.log10(pc3['Interactions'])
    pc1 = format_matrix_for_lineplot(pc1)
    pc1['Interactions'] = np.log10(pc1['Interactions'])
    gs3 = format_matrix_for_lineplot(gs3)
    gs3['Interactions'] = np.log10(gs3['Interactions'])
    gs1 = format_matrix_for_lineplot(gs1)
    gs1['Interactions'] = np.log10(gs1['Interactions'])
        
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    fig, (ax1,ax2) = plt.subplots(ncols=2, facecolor='w', edgecolor='k', figsize=(20,8), dpi=75)
    
    plt.rc('font', size=32)
    plt.rc('ytick', labelsize=28)
    plt.rc('xtick', labelsize=28)
    
    ax = sns.lineplot(x='Interactions', y='PCC', data=pc3, ci=95, marker='D', markersize=7, c='b', label='Pairwise corr.', ax=ax2)
    ax = sns.lineplot(x='Interactions', y='PCC', data=pc1, ci=95, marker='D', markersize=7, c='b', label='Pairwise corr.', ax=ax1)
    ax = sns.lineplot(x='Interactions', y='PCC', data=gs3, ci=95, marker='D', markersize=7, c='C1', label='GS corr.', ax=ax2)
    ax = sns.lineplot(x='Interactions', y='PCC', data=gs1, ci=95, marker='D', markersize=7, c='C1', label='GS corr.', ax=ax1)
    ax2.axvline(x=np.log10(pc3_edge_cutoff), c='k')
    ax1.axvline(x=np.log10(pc1_edge_cutoff), c='k')
    #plt.title('Pairwise correlation'+title) 
    
    ax1.set_yticks([0.2, 0.4, 0.6])
    ax2.set_yticks([0.2, 0.4, 0.6])
    ax1.set_ylim([0, 0.65])
    ax2.set_ylim([0, 0.65])
    ax1.set_xticks([3, 4, 5])
    ax2.set_xticks([3, 4, 5])
    ax1.set_title('in silico', fontsize=36)
    ax2.set_title('E. coli', fontsize=36)

    #plt.legend(bbox_to_anchor=(1.5, 1))
    sns.despine()
    return fig

def plot_yeast(pc4, gs4, ylabel, title):
    pc4_edge_cutoff = pc4.mean().argmax()
    
    pc4 = format_matrix_for_lineplot(pc4)
    pc4['Interactions'] = np.log10(pc4['Interactions'])
    gs4 = format_matrix_for_lineplot(gs4)
    gs4['Interactions'] = np.log10(gs4['Interactions'])
        
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    
    plt.rc('font', size=32)
    plt.rc('ytick', labelsize=28)
    plt.rc('xtick', labelsize=28)
    
    ax = sns.lineplot(x='Interactions', y='PCC', data=pc4, ci=95, marker='D', markersize=7, c='b', label='Pairwise corr.')   
    ax = sns.lineplot(x='Interactions', y='PCC', data=gs4, ci=95, marker='D', markersize=7, c='C1', label='GS corr.')   
    
    ax.axvline(x=np.log10(pc4_edge_cutoff), c='k')
    
    ax.set_yticks([0, 0.2, 0.4])
    ax.set_ylim([-0.2, 0.4])
    ax.set_xticks([3, 4, 5])
    ax.set_title('S. cerevisiae', fontsize=36)
    
    sns.despine()
    return fig

def main():
    #Pairwise correlation methodX-methodY.
    pc1 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/Pairwise_correlation/1_Hub_community__False_2019-02-20_11:16.csv", index_col=0)
    #True1 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/Hub_community/1/2018-11-26_14:52_paircorrheatmap/1_Hub_community__True.csv", index_col=0)
    pc3 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/Pairwise_correlation//3_Hub_community__False_2019-02-20_11:15.csv", index_col=0)
    #True3 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/Hub_community/3/2018-11-26_14:55_paircorrheatmap/3_Hub_community__True.csv", index_col=0)
    pc4 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/Pairwise_correlation//4_Hub_community__False_2019-02-20_11:14.csv", index_col=0)
    
    #Method correlation with GS.
    gs1 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_1_2019-02-20_11:19.csv", index_col=0)
    gs3 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_3_2019-02-20_11:20.csv", index_col=0)
    gs4 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_4_2019-02-20_11:21.csv", index_col=0)
    #GSTrue1 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_1_2018-11-14_14:24.csv", index_col=0)
    #GSTrue3 = pd.read_csv("/home/bioinf/Documents/Hub_project/Results/PCC_ind_methods/PCC_methods_False_3_2018-11-14_14:24.csv", index_col=0)
    gs1 = gs1.iloc[:-1,[0,2,4,6,8,10,12,14,16,18,20,22]]
    gs1.columns = ['1000','2000','3000','4000','5000','7000','10000','15000','20000','50000','80000','100000']
    gs3 = gs3.iloc[:-1,[0,2,4,6,8,10,12,14,16,18,20,22]]
    gs3.columns = ['1000','2000','3000','4000','5000','7000','10000','15000','20000','50000','80000','100000']
    gs4 = gs4.iloc[:-1,[0,2,4,6,8,10,12,14,16,18,20,22]]
    gs4.columns = ['1000','2000','3000','4000','5000','7000','10000','15000','20000','50000','80000','100000']
    #GSTrue1 = GSTrue1.iloc[:-1,[0,2,4,6,8,10,12,14,16,18,20,22]]
    #GSTrue1.columns = ['1000','2000','3000','4000','5000','7000','10000','15000','20000','50000','80000','100000']
    #GSTrue3 = GSTrue3.iloc[:-1,[0,2,4,6,8,10,12,14,16,18,20,22]]
    #GSTrue3.columns = ['1000','2000','3000','4000','5000','7000','10000','15000','20000','50000','80000','100000']
    
    #Plot
    fig = plot_fun(pc3, pc1, gs3, gs1, ' mean', '')
    fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Paircorr_GScorr_95CI_article.png', dpi=200, bbox_inches="tight")

    fig = plot_yeast(pc4, gs4, ' mean', '')
    fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/Paircorr_GScorr_yeast_95CI_article.png', dpi=200, bbox_inches="tight")
    #fig = plot_fun(True3.sum(), True1.sum(), 'sum' , ' (ranked)')
    #fig.savefig('/home/bioinf/Documents/Hub_project/Results/Hub_community/Paircorr_sum_ranked.png', dpi=200, bbox_inches="tight")
    #fig = plot_fun(False3.sum(), False1.sum(), 'sum', '')
    #fig.savefig('/home/bioinf/Documents/Hub_project/Results/Hub_community/Paircorr_sum.png', dpi=200, bbox_inches="tight")
    #fig = plot_fun(True3.mean(), True1.mean(), 'mean', ' (ranked)')
    #fig.savefig('/home/bioinf/Documents/Hub_project/Results/Hub_community/Paircorr_mean_ranked.png', dpi=200, bbox_inches="tight")
    
    edge_cutoff = pc3.mean().argmax()
    edge_cutoff = pc1.mean().argmax()
    edge_cutoff = pc4.mean().argmax()
    return

if __name__ == "__main__":
    main()
