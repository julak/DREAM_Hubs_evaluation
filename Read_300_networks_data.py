#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 14:06:34 2018

@author: bioinf
"""
import pandas as pd
import os
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as sts

def Expression_data():
    os.chdir('/home/bioinf/Documents/Hub_project/300_networks/Supplementary_data/Intermediary files/')
    
    transcript_exp = pd.read_csv('transcript_expr.rank.prec90.txt', sep='\t')
    #enhancer_exp = pd.read_csv('enhancer_expr.rank.txt', sep='\t')
    #promoter_exp = pd.read_csv('promoter_expr.rank.prec90.txt', sep='\t')
    
    ##Transcript to gene expression by takin mean of trasncripts
    transcript_exp.index = list(pd.DataFrame(list(pd.Series(transcript_exp.index).map(lambda x: x.split('-')))).iloc[:,0])
    Exp = pd.DataFrame()
    for gene in np.unique(transcript_exp.index):
        if list(transcript_exp.index).count(gene) == 1:
            Exp = Exp.append(pd.DataFrame(transcript_exp.loc[gene,:], columns = [gene]).T)   
        else:
            Exp = Exp.append(pd.DataFrame(transcript_exp.loc[gene,:].mean(), columns = [gene]).T)
    return Exp

##Read 32 high level networks##     
def Read_32_high_level_networks():
    os.chdir('/home/bioinf/Documents/Hub_project/300_networks/Network_compendium/Tissue-specific_regulatory_networks_FANTOM5-v1/32_high-level_networks/')
    network_files = os.listdir()
    network_files = [file for file in network_files if file not in ['_clusters.txt']]
    return network_files

def annotate_sample_network():
    sample_annotation = pd.read_excel('/home/bioinf/Documents/Hub_project/300_networks/nmeth.3799-S2.xlsx')
    sample_annotation = sample_annotation.fillna('NaN')
    
    networks = [network.replace(' (', '_') for network in sample_annotation.loc[:,'circuit name']]
    networks = [network.replace(') ', '_') for network in networks]
    networks = [network.replace(', ', '_') for network in networks]
    networks = [network.replace("'", '') for network in networks]
    networks = [network.replace(' ', '_') for network in networks]
    networks = [network.replace(')', '') for network in networks]
    sample_annotation.loc[:,'circuit name'] = [network.lower() for network in networks]
    
    sample_annotation = sample_annotation.loc[:,['circuit name', 'libId']]
    
    network_clusters = pd.read_table('/home/bioinf/Documents/Hub_project/300_networks/Network_compendium/Tissue-specific_regulatory_networks_FANTOM5-v1/32_high-level_networks/_clusters.txt', header=None)
    network_clusters.columns=['circuit name', 'HL_network']
    sample_network = sample_annotation.merge(network_clusters, on='circuit name', how='outer').loc[:,['libId', 'HL_network']]
    return sample_network

def main():

    Exp = Expression_data()
    network_files = Read_32_high_level_networks()
    sample_network = annotate_sample_network()
    
    P_all = pd.DataFrame()
    for file in network_files:
        network = pd.read_table(file, header=None)

        ##Samples corresponding to network and genes corresponding to TFs
        file = file.replace('.txt','')
        samples = list(sample_network.libId[sample_network.HL_network == file])
        Exp_net = Exp.loc[:,samples]
        Exp_net = Exp_net.loc[np.unique(network.iloc[:,0]),:].dropna()
        
        ##Outdegree
        TF_outdegree = pd.DataFrame(network.groupby(network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = ['GS']).loc[Exp_net.index,:]
        
        ##PCA
        pca = PCA()
        data = pca.fit_transform(Exp_net)
        PC1=0
        PC2=1
        
        ##Pearson correlation
        P = sts.pearsonr(data[:,PC1], TF_outdegree.GS)
        P_all = P_all.append(pd.DataFrame([P[0],P[1], len(samples)], columns=[file], index=['corr', 'pval', 'nb_samples']).T)
        
        ##Plot PCA##
        color_pallet = "bright" 
        plt.style.use('seaborn-ticks') #plt.style.available
        sns.set_color_codes(color_pallet)
        plt.rc('font', size=32) 
        plt.rc('ytick', labelsize=24)
        plt.rc('xtick', labelsize=24)
        
        fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
        
        ax.scatter(data[:,PC1], data[:,PC2], c='darkgrey',s=140)
        ax.scatter(data[:,PC1], data[:,PC2], s=140, c=TF_outdegree.iloc[:,0], cmap='Reds')
        
        plt.xlabel('PC' + str(PC1 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC1]*100))
        plt.ylabel('PC' + str(PC2 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC2]*100)) 
        
        fig.savefig('/home/bioinf/Documents/Hub_project/Results/PCA/PCA_'+file+'.png', dpi=200)
        
    P_all.to_csv('/home/bioinf/Documents/Hub_project/Results/PCA/PCA_Pearson_correlation_300_networks.tsv', sep='\t')

if __name__ == "__main__":
    main()
