#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 14:04:32 2018

@author: bioinf
"""
import os
os.chdir('/home/bioinf/Documents/Hub_project/DREAM_Hubs_evaluation/')

import pandas as pd
import numpy as np
import scipy.stats as sts
import seaborn as sns
from sklearn.decomposition import PCA
from sklearn import utils
import matplotlib.pyplot as plt
import sklearn.preprocessing as skp
from Method_evaluation import Count_fun, count_outdegree, inverse_participation_ratio, count_IPR

def PCA_Mat_rank(TF_outdegree, PC1, PC2):
    pca=PCA()
    data = pca.fit_transform(TF_outdegree.T) 
    
    color_pallet = "bright" 
    plt.style.use('seaborn-ticks') #plt.style.available
    sns.set_color_codes(color_pallet)
     
        
    fig,ax = plt.subplots(nrows=1, ncols=2, facecolor='w', edgecolor='k', figsize=(20,8), dpi=75)
    plt.rc('ytick', labelsize=28)
    plt.rc('xtick', labelsize=28)
    plt.rc('font', size=28)   
    
    s=140
    ax[0].scatter(data[0:6][:,PC1],data[0:6][:,PC2], s=s, color='C9', marker = 'o' , label='Bayesian')
    ax[0].scatter(data[[6,29,30]][:,PC1],data[[6,29,30]][:,PC2], s=s, color='C8', marker = 'o' , label='Correlation')
    ax[0].scatter(data[7:12][:,PC1],data[7:12][:,PC2], s=s, color='C7', marker = 'o' , label='MI')
    ax[0].scatter(data[[12,13,31,32,33]][:,PC1],data[[12,13,31,32,33]][:,PC2], s=s, color='lightpink', marker = 'o' , label='Meta')
    ax[0].scatter(data[14:22][:,PC1],data[14:22][:,PC2], s=s, color='C5', marker = 'o' , label='Other')
    ax[0].scatter(data[[22,23,24,25,26,27,28,34]][:,PC1],data[[22,23,24,25,26,27,28,34]][:,PC2], s=s, color='C4', marker = 'o' , label='Regression')
    ax[1].scatter(data[0:6][:,PC1],data[0:6][:,PC2], s=s, color='C9', marker = 'o' , label='Bayesian')
    ax[1].scatter(data[[6,29,30]][:,PC1],data[[6,29,30]][:,PC2], s=s, color='C8', marker = 'o' , label='Correlation')
    ax[1].scatter(data[7:12][:,PC1],data[7:12][:,PC2], s=s, color='C7', marker = 'o' , label='MI')
    ax[1].scatter(data[[12,13,31,32,33]][:,PC1],data[[12,13,31,32,33]][:,PC2], s=s, color='lightpink', marker = 'o' , label='Meta')
    ax[1].scatter(data[14:22][:,PC1],data[14:22][:,PC2], s=s, color='C5', marker = 'o' , label='Other')
    ax[1].scatter(data[[22,23,24,25,26,27,28,34]][:,PC1],data[[22,23,24,25,26,27,28,34]][:,PC2], s=s, color='C4', marker = 'o' , label='Regression')
    
    ##Add method number to points##
    #n=[1,2,3,4,5,6,1,2,3,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8]
    #x=data[[0,1,2,3,4,5,6,29,30,7,8,9,10,11,12,13,31,32,33,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,34]]
    #for i, txt in enumerate(n):
    #    ax[0].annotate(txt, (x[:,PC1][i], x[:,PC2][i]))
    #    ax[1].annotate(txt, (x[:,PC1][i], x[:,PC2][i]))
    
    plt.legend(loc = 'upper right', frameon=True, bbox_to_anchor=(1.4, 1), fontsize=24)    
    ax[0].set_xlabel('PC' + str(PC1 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC1]*100), fontsize=32)
    ax[0].set_ylabel('PC' + str(PC2 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC2]*100), fontsize=32) 
    ax[1].set_xlabel('PC' + str(PC1 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC1]*100), fontsize=32)
    #ax[1].set_ylabel('PC' + str(PC2 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC2]*100)) 
    sns.despine(offset=0, trim=False)
    
    if PC1==0 and PC2==1:
        ax[0].set_yticks([-1000,0, 1000])
        ax[0].set_xticks([0, 1000, 2000, 3000])
        ax[1].set_ylim([-320,300])
        ax[1].set_xlim([-500,300])
        ax[1].set_yticks([-200, 0, 200])
        ax[1].set_xticks([-400,-200, 0, 200])
    elif PC1==1 and PC2==2:
        ax[0].set_yticks([-500, 0, 500, 1000])
        ax[0].set_xticks([-1000, 0, 1000])
        ax[1].set_ylim([-300,300])
        ax[1].set_xlim([-250,300])
        ax[1].set_yticks([-200, 0, 200])
        ax[1].set_xticks([-200, 0, 200])

    return fig

def PCA_Mat_rank_log(TF_outdegree_1,TF_outdegree_3,TF_outdegree_4, PC1, PC2, PC3):
    
    TF_outdegree_1[TF_outdegree_1 == 0] = 1
    TF_outdegree_1 = np.log(TF_outdegree_1)
    TF_outdegree_3[TF_outdegree_3 == 0] = 1
    TF_outdegree_3 = np.log(TF_outdegree_3)
    TF_outdegree_4[TF_outdegree_4 == 0] = 1
    TF_outdegree_4 = np.log(TF_outdegree_4)
    
    TF_outdegree = TF_outdegree_1.append(TF_outdegree_3)
    #TF_outdegree = TF_outdegree.append(TF_outdegree_4)

    pca=PCA()
    data = pca.fit_transform(TF_outdegree.T) 
    
    color_pallet = "bright" 
    plt.style.use('seaborn-ticks') #plt.style.available
    sns.set_color_codes(color_pallet)
     
        
    fig,ax = plt.subplots(nrows=1, ncols=3, facecolor='w', edgecolor='k', figsize=(30,8), dpi=75)
    plt.rc('ytick', labelsize=28)
    plt.rc('xtick', labelsize=28)
    plt.rc('font', size=28)   
    
    s=140
    ax[0].scatter(data[0:6][:,PC1],data[0:6][:,PC2], s=s, color='C9', marker = 'o' , label='Bayesian')
    ax[0].scatter(data[[6,29,30]][:,PC1],data[[6,29,30]][:,PC2], s=s, color='C8', marker = 'o' , label='Correlation')
    ax[0].scatter(data[7:12][:,PC1],data[7:12][:,PC2], s=s, color='C7', marker = 'o' , label='MI')
    ax[0].scatter(data[[12,13,31,32,33]][:,PC1],data[[12,13,31,32,33]][:,PC2], s=s, color='lightpink', marker = 'o' , label='Meta')
    ax[0].scatter(data[14:22][:,PC1],data[14:22][:,PC2], s=s, color='C5', marker = 'o' , label='Other')
    ax[0].scatter(data[[22,23,24,25,26,27,28,34]][:,PC1],data[[22,23,24,25,26,27,28,34]][:,PC2], s=s, color='C4', marker = 'o' , label='Regression')
    ax[1].scatter(data[0:6][:,PC2],data[0:6][:,PC3], s=s, color='C9', marker = 'o' , label='Bayesian')
    ax[1].scatter(data[[6,29,30]][:,PC2],data[[6,29,30]][:,PC3], s=s, color='C8', marker = 'o' , label='Correlation')
    ax[1].scatter(data[7:12][:,PC2],data[7:12][:,PC3], s=s, color='C7', marker = 'o' , label='MI')
    ax[1].scatter(data[[12,13,31,32,33]][:,PC2],data[[12,13,31,32,33]][:,PC3], s=s, color='lightpink', marker = 'o' , label='Meta')
    ax[1].scatter(data[14:22][:,PC2],data[14:22][:,PC3], s=s, color='C5', marker = 'o' , label='Other')
    ax[1].scatter(data[[22,23,24,25,26,27,28,34]][:,PC2],data[[22,23,24,25,26,27,28,34]][:,PC3], s=s, color='C4', marker = 'o' , label='Regression')
    ax[2].scatter(data[0:6][:,PC1],data[0:6][:,PC3], s=s, color='C9', marker = 'o' , label='Bayesian')
    ax[2].scatter(data[[6,29,30]][:,PC1],data[[6,29,30]][:,PC3], s=s, color='C8', marker = 'o' , label='Correlation')
    ax[2].scatter(data[7:12][:,PC1],data[7:12][:,PC3], s=s, color='C7', marker = 'o' , label='MI')
    ax[2].scatter(data[[12,13,31,32,33]][:,PC1],data[[12,13,31,32,33]][:,PC3], s=s, color='lightpink', marker = 'o' , label='Meta')
    ax[2].scatter(data[14:22][:,PC1],data[14:22][:,PC3], s=s, color='C5', marker = 'o' , label='Other')
    ax[2].scatter(data[[22,23,24,25,26,27,28,34]][:,PC1],data[[22,23,24,25,26,27,28,34]][:,PC3], s=s, color='C4', marker = 'o' , label='Regression')
         
    ##Add method number to points##
    #n=[1,2,3,4,5,6,1,2,3,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8]
    #x=data[[0,1,2,3,4,5,6,29,30,7,8,9,10,11,12,13,31,32,33,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,34]]
    #for i, txt in enumerate(n):
    #    ax[0].annotate(txt, (x[:,PC1][i], x[:,PC2][i]))
    #    ax[1].annotate(txt, (x[:,PC1][i], x[:,PC2][i]))
    
    plt.legend(loc = 'upper right', frameon=True, bbox_to_anchor=(1.5, 1), fontsize=24)    
    ax[0].set_xlabel('PC' + str(PC1 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC1]*100), fontsize=32)
    ax[0].set_ylabel('PC' + str(PC2 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC2]*100), fontsize=32) 
    ax[1].set_xlabel('PC' + str(PC2 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC2]*100), fontsize=32)
    ax[1].set_ylabel('PC' + str(PC3 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC3]*100), fontsize=32) 
    ax[2].set_xlabel('PC' + str(PC1 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC1]*100), fontsize=32)
    ax[2].set_ylabel('PC' + str(PC3 + 1) + ' (%.2f%%)' % (pca.explained_variance_ratio_[PC3]*100), fontsize=32) 
    
    sns.despine(offset=0, trim=False)
    
    ax[0].set_yticks([-10, 0, 10, 20])
    ax[0].set_xticks([-20, 0, 20, 40])
    ax[1].set_yticks([-10, 0, 10])
    ax[1].set_xticks([-10, 0, 10, 20])
    ax[2].set_yticks([-10, 0, 10, 20])
    ax[2].set_xticks([-20, 0, 20, 40])    
        
    return fig

def PCA_log_sep(TF_outdegree_1,TF_outdegree_3,TF_outdegree_4, PC1, PC2):
    
    TF_outdegree_1[TF_outdegree_1 == 0] = 1
    TF_outdegree_1 = np.log(TF_outdegree_1)
    TF_outdegree_3[TF_outdegree_3 == 0] = 1
    TF_outdegree_3 = np.log(TF_outdegree_3)
    TF_outdegree_4[TF_outdegree_4 == 0] = 1
    TF_outdegree_4 = np.log(TF_outdegree_4)

    pca1=PCA()
    data1 = pca1.fit_transform(TF_outdegree_1.T) 
    pca2=PCA()
    data2 = pca2.fit_transform(TF_outdegree_3.T) 
    
    color_pallet = "bright" 
    plt.style.use('seaborn-ticks') #plt.style.available
    sns.set_color_codes(color_pallet)
     
        
    fig,ax = plt.subplots(nrows=1, ncols=2, facecolor='w', edgecolor='k', figsize=(20,8), dpi=75)
    plt.rc('ytick', labelsize=28)
    plt.rc('xtick', labelsize=28)
    plt.rc('font', size=28)   
    
    s=140
    ax[0].scatter(data1[0:6][:,PC1],data1[0:6][:,PC2], s=s, color='C9', marker = 'o' , label='Bayesian')
    ax[0].scatter(data1[[6,29,30]][:,PC1],data1[[6,29,30]][:,PC2], s=s, color='C8', marker = 'o' , label='Correlation')
    ax[0].scatter(data1[7:12][:,PC1],data1[7:12][:,PC2], s=s, color='C7', marker = 'o' , label='MI')
    ax[0].scatter(data1[[12,13,31,32,33]][:,PC1],data1[[12,13,31,32,33]][:,PC2], s=s, color='lightpink', marker = 'o' , label='Meta')
    ax[0].scatter(data1[14:22][:,PC1],data1[14:22][:,PC2], s=s, color='C5', marker = 'o' , label='Other')
    ax[0].scatter(data1[[22,23,24,25,26,27,28,34]][:,PC1],data1[[22,23,24,25,26,27,28,34]][:,PC2], s=s, color='C4', marker = 'o' , label='Regression')
    ax[1].scatter(data2[0:6][:,PC1],data2[0:6][:,PC2], s=s, color='C9', marker = 'o' , label='Bayesian')
    ax[1].scatter(data2[[6,29,30]][:,PC1],data2[[6,29,30]][:,PC2], s=s, color='C8', marker = 'o' , label='Correlation')
    ax[1].scatter(data2[7:12][:,PC1],data2[7:12][:,PC2], s=s, color='C7', marker = 'o' , label='MI')
    ax[1].scatter(data2[[12,13,31,32,33]][:,PC1],data2[[12,13,31,32,33]][:,PC2], s=s, color='lightpink', marker = 'o' , label='Meta')
    ax[1].scatter(data2[14:22][:,PC1],data2[14:22][:,PC2], s=s, color='C5', marker = 'o' , label='Other')
    ax[1].scatter(data2[[22,23,24,25,26,27,28,34]][:,PC1],data2[[22,23,24,25,26,27,28,34]][:,PC2], s=s, color='C4', marker = 'o' , label='Regression')
          
    ##Add method number to points##
    #n=[1,2,3,4,5,6,1,2,3,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8]
    #x=data[[0,1,2,3,4,5,6,29,30,7,8,9,10,11,12,13,31,32,33,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,34]]
    #for i, txt in enumerate(n):
    #    ax[0].annotate(txt, (x[:,PC1][i], x[:,PC2][i]))
    #    ax[1].annotate(txt, (x[:,PC1][i], x[:,PC2][i]))
    
    plt.legend(loc = 'upper right', frameon=True, bbox_to_anchor=(1.5, 1), fontsize=24)    
    ax[0].set_xlabel('PC' + str(PC1 + 1) + ' (%.2f%%)' % (pca1.explained_variance_ratio_[PC1]*100), fontsize=32)
    ax[0].set_ylabel('PC' + str(PC2 + 1) + ' (%.2f%%)' % (pca1.explained_variance_ratio_[PC2]*100), fontsize=32) 
    ax[1].set_xlabel('PC' + str(PC1 + 1) + ' (%.2f%%)' % (pca2.explained_variance_ratio_[PC1]*100), fontsize=32)
    ax[1].set_ylabel('PC' + str(PC2 + 1) + ' (%.2f%%)' % (pca2.explained_variance_ratio_[PC2]*100), fontsize=32) 
    ax[0].set_title('in silico', fontsize=36)
    ax[1].set_title('E. coli', fontsize=36)

    sns.despine(offset=0, trim=False)
    
    if PC1==0 and PC2==1:
        ax[0].set_yticks([-10, 0, 10])
        ax[0].set_xticks([-10, 0, 10, 20])
        ax[1].set_yticks([-10, 0, 10, 20])
        ax[1].set_xticks([-10, 0, 10, 20, 30])
    elif PC1==1 and PC2==2:      
        ax[0].set_yticks([-10,0,10, 20])
        ax[0].set_xticks([-10,0,10])
        ax[1].set_yticks([-10, 0, 10, 20])
        ax[1].set_xticks([-10, 0, 10, 20])
       
    return fig


def Get_TF_outdegree(outdegree_function, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree):
    TF_outdegree = outdegree_function(Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree) 
    return TF_outdegree

def get_outdegree_mat(Network, len_Network, rank_outdegree):
    Methods = ['Bayesian1', 'Bayesian2', 'Bayesian3','Bayesian4', 'Bayesian5', 'Bayesian6','Correlation1','Correlation2', 'Correlation3','MI1', 'MI2', 'MI3','MI4','MI5','Meta1','Meta2','Meta3','Meta4','Meta5','Other1','Other2','Other3','Other4',
        'Other5','Other6', 'Other7','Other8','Regression1', 'Regression2', 'Regression3', 'Regression4','Regression5', 'Regression6', 'Regression7', 'Regression8', 'community']
    ots_methods = ['Correlation2', 'Correlation3', 'MI1', 'MI2', 'MI3','Regression8']
    Goldstandard = 'Network' + Network    
    gold_Network = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    
    gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()    
    
    TF_outdegree = Get_TF_outdegree(count_outdegree, Network, Methods, ots_methods, Goldstandard, gold_genes, len_Network, rank_outdegree)

    TF_outdegree = TF_outdegree.drop('community', axis=1)
    TF_outdegree = TF_outdegree.drop('GS', axis=1)
    return TF_outdegree

TF_outdegree_1 = get_outdegree_mat(Network='1', len_Network=2000, rank_outdegree=False)
TF_outdegree_3 = get_outdegree_mat(Network='3', len_Network=80000, rank_outdegree=False)
TF_outdegree_4 = get_outdegree_mat(Network='4', len_Network=3000, rank_outdegree=False)

TF_outdegree = TF_outdegree_1.append(TF_outdegree_3)
#TF_outdegree = TF_outdegree.append(TF_outdegree_4)

##PCA##
fig = PCA_Mat_rank(TF_outdegree, PC1=0, PC2=1)
fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/PCA_outdegree_PC1_PC2_article.png', dpi=200, bbox_inches="tight")
fig = PCA_Mat_rank(TF_outdegree, PC1=1, PC2=2)
fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/PCA_outdegree_PC2_PC3_article.png', dpi=200, bbox_inches="tight")

fig = PCA_log_sep(TF_outdegree_1, TF_outdegree_3, TF_outdegree_4, PC1=1, PC2=2)
fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/PCA_outdegree_PC1_PC2_sep_log_article.png', dpi=200, bbox_inches="tight")

#log
fig = PCA_Mat_rank_log(TF_outdegree_1, TF_outdegree_3, TF_outdegree_4, PC1=0, PC2=1, PC3=2)
fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/PCA_outdegree_PC1_PC2_PC3_log_article.png', dpi=200, bbox_inches="tight")
