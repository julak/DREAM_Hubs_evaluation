#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 13:21:04 2019

@author: bioinf
"""

import os
os.chdir('/home/bioinf/Documents/Hub_project/DREAM_Hubs_evaluation/')
import pandas as pd
import numpy as np
import scipy.stats as sts
import sklearn.preprocessing as skp
import matplotlib.pyplot as plt
import seaborn as sns
from Method_evaluation import Count_fun
from Pairwise_correlation import Pair_corr
#from Hub_community_with_sem import random_commmunity_norepeat
import random
import datetime

def Pearson_corr(x,Ymat):
    P = []
    Pval = []
    for i in Ymat:
        y = Ymat.loc[:,i]
        Pcof = sts.pearsonr(x, y)
        P.append(Pcof[0])
        Pval.append(Pcof[1])
    P = pd.DataFrame([P,Pval], columns = Ymat.columns, index = ['PCC', 'pval']).T
    #P = P.sort_values(by='Pearson')
    return P

def count_outdegree(Network, Methods, GS, gold_genes, len_Network, network_names):
    #Counts outdegree of all TFs in predicted networks from all methods.
    
    TF_outdegree_all = pd.DataFrame()
    for name, method in zip(network_names, Methods):

        Method_Network = pd.read_table(name, header=None)
        if len(Method_Network)>len_Network:
            Method_Network = Method_Network.iloc[:len_Network,:]
        Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,0], gold_genes)] 
        Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,1], gold_genes)] 
       
        TF_outdegree = pd.DataFrame(Method_Network.groupby(Method_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = [method])
         
        TF_outdegree_all = TF_outdegree_all.join(TF_outdegree, how='outer')
    
    #Gold standard outdegree        
    GS_outdegree = pd.DataFrame(GS.groupby(GS.loc[:,0].tolist()).size().sort_values(ascending=False), columns = ['GS'])
    TF_outdegree_all = TF_outdegree_all.join(GS_outdegree, how='outer')
    
    TF_outdegree_all = TF_outdegree_all.fillna(0).astype(int)         
    return TF_outdegree_all


def Count_fun_PC(Network, Methods, len_Network, network_names):
    #Counts outdegree of all TFs in predicted network from method i.
    
    TF_outdegree_all = pd.DataFrame()
    for name, method in zip(network_names, Methods):
        
        Method_Network = pd.read_table(name, header=None)
        #Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,0], gold_genes)] 
        #Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,1], gold_genes)] 
        if len(Method_Network)>len_Network:
            Method_Network = Method_Network.iloc[:len_Network,:]
        TF_outdegree = pd.DataFrame(Method_Network.groupby(Method_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = [method])
        
        TF_outdegree_all = TF_outdegree_all.join(TF_outdegree, how='outer')
    
    #GS_outdegree = pd.DataFrame(GS.groupby(GS.loc[:,0].tolist()).size().sort_values(ascending=False), columns = ['GS'])
    #TF_outdegree_all = TF_outdegree_all.join(GS_outdegree, how='outer')
    
    TF_outdegree_all = TF_outdegree_all.fillna(0).astype(int) 
    
    return TF_outdegree_all

def pairwise_correlation(Network, Methods, network_names):
    
    pair_PCC = pd.DataFrame()
    for len_Network in [1000, 2000, 3000, 4000, 5000, 7000, 10000, 15000, 20000, 50000, 80000, 100000]:
        
        TF_outdegree = Count_fun_PC(Network, Methods, len_Network, network_names) 
    
        PCC_all, PCC_pval_all = Pair_corr(TF_outdegree, len_Network)
        
        pair_PCC = pair_PCC.join(PCC_all, how='outer')
        
    edge_cutoff = pair_PCC.mean().argmax()
        
    return pair_PCC, int(edge_cutoff)

def Method_performance(Network, Methods, gold_Network, gold_genes, network_names):
    GS_PCC = pd.DataFrame()
    for len_Network in [1000, 2000, 3000, 4000, 5000, 7000, 10000, 15000, 20000, 50000, 80000, 100000]:
        TF_outdegree = count_outdegree(Network, Methods, gold_Network, gold_genes, len_Network, network_names) 
        PCC = pd.DataFrame(Pearson_corr(x=TF_outdegree.loc[:,'GS'], Ymat=TF_outdegree).PCC)
        PCC.columns = [str(len_Network)]
        GS_PCC = GS_PCC.join(PCC, how='outer')
    return GS_PCC.iloc[:-1]

def plot_pairwise_correlation(pair_PCC, GS_PCC, edge_cutoff):
    
    pair_PCC.columns = [int(x) for x in pair_PCC.columns]
    pair_PCC = pair_PCC.melt()
    pair_PCC.columns = ['Interactions', 'PCC']
    pair_PCC['Interactions'] = np.log10(pair_PCC['Interactions'])

    GS_PCC.columns = [int(x) for x in GS_PCC.columns]
    GS_PCC = GS_PCC.melt()
    GS_PCC.columns = ['Interactions', 'PCC']
    GS_PCC['Interactions'] = np.log10(GS_PCC['Interactions'])
    
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('ytick', labelsize=24)
    plt.rc('xtick', labelsize=24)
    
    ax = sns.lineplot(x='Interactions', y='PCC', data=pair_PCC, marker='D', markersize=7, ci=95, c='b', label='Pairwise corr.')
    ax = sns.lineplot(x='Interactions', y='PCC', data=GS_PCC, marker='D', markersize=7, ci=95, c='C1', label='GS corr.')
    ax = plt.axvline(x=np.log10(edge_cutoff), c='k')
    
    plt.legend(bbox_to_anchor=(1.05, 1), fontsize=24)
    plt.ylabel('PCC',fontsize=28)
    plt.xlabel('Interactions',fontsize=28)
    #plt.xlim([0, 20000])
    plt.yticks([0, 0.2, 0.4])
    plt.xticks([3, 4, 5])
    plt.title('HPA', fontsize=36)
    
    sns.despine()
                            
    return fig

def random_commmunity_norepeat(Network, Methods, TF_outdegree):
    random_mat = pd.DataFrame()
    random_name = pd.DataFrame()
    mean_method=np.mean
    Used_orders = np.array([np.full(len(Methods),'t')])
    np.random.seed(seed=1)
    for i in range(1000):
        print(i)
        random_method = np.random.choice(Methods, size=len(Methods))#Random with replacement   
        #random_method = random_list()   

        #####################################################################################
        PCC_com=[]
        for j in range(0,len(random_method)):
            f=0
            for n in range(1,len(Used_orders)):
                if np.isin(random_method[:j+1], Used_orders[n,:j+1]).sum()==len(random_method[:j+1]):
                    f=f+1
                    break
            
            if f==0:    
                com = pd.Series(mean_method(TF_outdegree.loc[:,random_method[:j+1]].T), index=TF_outdegree.index)
                        
                ##Correlation
                PCC = sts.pearsonr(x=TF_outdegree.loc[:,'GS'], y=com)
                PCC_com.append(PCC)
            
            else:
                PCC_com.append((np.nan,np.nan))
        #######################################################################################
        
        PCC_com = pd.DataFrame(PCC_com, index=np.arange(1,len(random_method)+1), columns=['PCC','pval'])
            
        PCC_com = pd.DataFrame(PCC_com.PCC) #mean_method=sts.mstats.gmean, sts.mstats.hmean, np.mean
        PCC_com.columns = [i]
        
        random_mat = random_mat.join(PCC_com, how='outer')
        random_name = random_name.join(pd.DataFrame(random_method, columns=[i]), how='outer')        
        
        Used_orders=np.append(Used_orders,np.array([random_method]), axis=0)

    return random_mat

def hub_community(Network, Methods, gold_Network, gold_genes, len_Network, network_names):
    TF_outdegree = count_outdegree(Network, Methods, gold_Network, gold_genes, len_Network, network_names) 
    random_mat = random_commmunity_norepeat(Network, Methods, TF_outdegree)
    return random_mat

def read_and_rank_networks(Network, Methods, use_len_Network, len_Network, network_names):
    Rank_mat = pd.DataFrame()
    Name_mat = pd.DataFrame(columns=['TF', 'target'])
    
    for name, method in zip(network_names, Methods):  
        
        Met_Net = pd.read_table(name, header=None)
        if use_len_Network==True:
            Met_Net = Met_Net.iloc[:len_Network,:]
        Met_Net.loc[:,'rank'] = Met_Net.iloc[:,2].rank(ascending=False)        
        Met_Net.index = Met_Net.loc[:,0] + Met_Net.loc[:,1]        
        Met_Net.columns = ['TF', 'target', 'score', method]        
        
        Rank_mat = Rank_mat.join(Met_Net.loc[:,method], how='outer')        
        Name_mat = pd.merge(Name_mat.loc[:,['TF','target']], Met_Net.loc[:,['TF','target']], left_on=['TF','target'], right_on=['TF','target'], how='outer')
    
    Rank_mat = Rank_mat.fillna(100001)
    Name_mat.index = Name_mat.TF + Name_mat.target
    Rank_mat = Name_mat.join(Rank_mat, how='outer')
    return Rank_mat

def random_community_network_norepeat(rank_network_mat, Network, Methods, gold_Network, gold_genes, len_Network):
    GS_outdegree = pd.DataFrame(gold_Network.groupby(gold_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns=['GS'])
    random_mat = pd.DataFrame()
    Used_orders = np.array([np.full(len(Methods),'t')])
    np.random.seed(seed=1)
    for i in range(1000):
        print(i)
        random_method = np.random.choice(Methods, size=len(Methods))#Random with replacement   
        ######################################
        
        PCC_com_all=[]
        com_outdegree_all=pd.DataFrame()
        for j in range(0,len(random_method)):
            f=0
            for n in range(1,len(Used_orders)):
                if np.isin(random_method[:j+1], Used_orders[n,:j+1]).sum()==len(random_method[:j+1]):
                    f=f+1
                    break
            
            if f==0:    
                rank_network_mat.loc[:,'community'] = np.mean(rank_network_mat.loc[:,random_method[:j+1]], axis=1)             
        
                community = rank_network_mat.loc[:,['TF','target','community']]
                community = community[community.community!=100001]
                community = community.iloc[np.isin(community.iloc[:,0], gold_genes)] 
                community = community.iloc[np.isin(community.iloc[:,1], gold_genes)]
                community = community.sort_values(by='community')
                
                if len(community)>len_Network:
                    community = community.iloc[:len_Network,:]
                
                com_outdegree = pd.DataFrame(community.groupby(community.loc[:,'TF'].tolist()).size().sort_values(ascending=False), columns=[j])
                com_outdegree_all = com_outdegree_all.join(com_outdegree, how='outer')
            else:
                com_outdegree = pd.DataFrame(np.full(len(GS_outdegree),np.nan), columns=[j], index=GS_outdegree.index)
                com_outdegree_all = com_outdegree_all.join(com_outdegree, how='outer')
                

        com_outdegree_all = com_outdegree_all.join(GS_outdegree, how='outer').fillna(0)
        
        PCC_com_all = Pearson_corr(com_outdegree_all.GS, com_outdegree_all.iloc[:,:-1])

        ################################################   
        PCC_com = pd.DataFrame(PCC_com_all, columns=['PCC','pval'])
        PCC_com.index=np.arange(1,len(random_method)+1)
            
        PCC_com = pd.DataFrame(PCC_com.PCC) #mean_method=sts.mstats.gmean, sts.mstats.hmean, np.mean
        PCC_com.columns = [i]
        
        random_mat = random_mat.join(PCC_com, how='outer')
        
        Used_orders=np.append(Used_orders,np.array([random_method]), axis=0)

    return random_mat


def community_network(Network, Methods, gold_Network, gold_genes, edge_cutoff, network_names):
    rank_network_mat = read_and_rank_networks(Network, Methods, use_len_Network=False, len_Network=100000, network_names=network_names)
    random_mat=random_community_network_norepeat(rank_network_mat, Network, Methods, gold_Network, gold_genes, edge_cutoff)
    return random_mat

def plot_communities(comhub_mat, comnet_mat, ci):
    data_com1 = comnet_mat.T.melt()
    data_com1.columns = ['Methods', 'PCC']
    data_hub1 = comhub_mat.T.melt()
    data_hub1.columns = ['Methods', 'PCC']

    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('ytick', labelsize=24)
    plt.rc('xtick', labelsize=24)
    
    sns.lineplot(x='Methods', y='PCC', data=data_com1, linewidth=2, ci=ci, c='k', label='Community network')
    sns.lineplot(x='Methods', y='PCC', data=data_hub1, linewidth=2, ci=ci, c='r', label='Hub community')    

    plt.ylabel('PCC',fontsize=32)
    plt.xlabel('Methods',fontsize=32)
    plt.ylim([0.05, 0.3])
    plt.yticks([0.1, 0.2, 0.3])
    plt.xticks([1,2,3,4,5,6])
    plt.title('HPA', fontsize=36)

    ax.legend(bbox_to_anchor=(1, 0.25), fontsize=28)

    sns.despine()

    return fig

def random_PCC(Network, Methods, gold_Network, gold_genes, len_Network, network_names):
    
    TF_outdegree = count_outdegree(Network, Methods, gold_Network, gold_genes, len_Network, network_names)
    GS=TF_outdegree.loc[:,'GS']
    np.random.seed(seed=1)  
    Pmat=[]
    for i in range(4000):
        #X = np.arange(0, len(GS))
        #random.shuffle(X)
        X = random.sample(list(np.array(TF_outdegree.iloc[:,:-1]).flatten()), len(GS))
        Pcof = sts.pearsonr(X, GS)
        Pmat.append(Pcof[0])
    
    return np.mean(Pmat)

def plot_method_performance(GS_PCC, edge_cutoff, comhub_mat, comnet_mat, Network, Methods, gold_Network, gold_genes, network_names):
    rand = random_PCC(Network, Methods, gold_Network, gold_genes, edge_cutoff, network_names)
    
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('ytick', labelsize=24)
    plt.rc('xtick', labelsize=24)
    
    ax.bar(np.arange(0,6), height=np.array(GS_PCC.loc[Methods,edge_cutoff]), width=0.9, color=['C8','C7','C7','C5','C4','C4'], label='Bayesian')
    ax.bar(np.arange(8,9), height=np.array(comhub_mat.mean(axis=1))[-1], width=0.9, color=['r'], label='Hub community')
    ax.bar(np.arange(7,8), height=np.array(comnet_mat.mean(axis=1))[-1], width=0.9, color=['k'], label='Community network')
    ax.bar(np.arange(9,10), height=rand, width=0.9, color=['r'], label='random')
#    ax.text(10.3, 0.2, r'P: PCC', fontsize=22)
#    ax.text(10.3, 0.18, r'A: ARACNE', fontsize=22)
#    ax.text(10.3, 0.16, r'C: CLR', fontsize=22)
#    ax.text(10.3, 0.14, r'G: GENIE3', fontsize=22)
#    ax.text(10.3, 0.12, r'T: TIGRESS', fontsize=22)
#    ax.text(10.3, 0.10, r'E: ElasticNet ', fontsize=22)
#    ax.text(10.3, 0.08, r'H: Hub community', fontsize=22)
#    ax.text(10.3, 0.06, r'CN: Community network', fontsize=22)
#    ax.text(10.3, 0.04, r'R: Random', fontsize=22)
    
    ax.axhline(0, color='k', linewidth=1) #Plot line at 0
    ax.axhline(np.array(comhub_mat.mean(axis=1))[-1], color='k', linewidth=1.2, linestyle='--') #Plot line at 0
    ax.set_ylabel('PCC',fontsize=32)
    ax.set_yticks([0.1,0.2,0.3])
    ax.set_title('HPA', fontsize=36)

    plt.setp(ax, xticks=[0,1,2,3,4,5,8,7,9], xticklabels = Methods + ['H', 'C', 'R'])
    sns.despine(offset=0, trim=False, bottom=True)
    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=True)
    plt.xticks(rotation=90)
    
    return fig

def find_hubs(Network, Methods, gold_Network, gold_genes, len_Network, network_names):
    TF_outdegree = count_outdegree(Network, Methods, gold_Network, gold_genes, len_Network, network_names)
    community = np.mean(TF_outdegree.iloc[:,:-1], axis=1).sort_values(ascending=False)
    #hubs = community.index[:int(len(community)*0.1)]
    hubs = community.cumsum()[community.cumsum()<community.sum()*0.1].index
    
    return hubs

def get_network_names(Methods, Network):
    network_folder = '/home/bioinf/Documents/Hub_project/HPA_results/'
    network_names = ['PCC_Network_'+Network+'.csv','ARACNE_network_'+Network+'.csv','CLR_Net'+Network+'.csv','GENIE3_Network_'+Network+'.csv', 'TIGRESS_'+Network+'_L1.txt','EN_Net_500_'+Network+'.csv']
    network_names = [network_folder + name for name in network_names]
    return network_names

#More general, however need to change how files are saved when running methods.
def get_network_names_2(Methods, Network):
    network_names = ['/Networks/' + Network + '/' + name  + '_network.tsv' for name in Methods]
    return network_names

def main():

    Network = 'HPAznorm2' #Which Network to use. 1=In Silico, 2=S.aureus, 3=E.coli, 4=S.cerevisiae.
    Methods = ['PCC','ARACNE','CLR','GENIE3','TIGRESS','ElasticNet']
    gold_Network = pd.read_table('/home/bioinf/Documents/Hub_project/HPA/'+Network+'_gold_standard_string.tsv', header=None)
    gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()

    network_names = get_network_names(Methods,Network)

    pair_PCC, edge_cutoff = pairwise_correlation(Network, Methods, network_names)
    GS_PCC = Method_performance(Network, Methods, gold_Network, gold_genes, network_names)
    fig = plot_pairwise_correlation(pair_PCC, GS_PCC, edge_cutoff)
    fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/HPA_pairwise_GS_correlation_article.png', dpi=200, bbox_inches="tight")
    
    comhub_mat = hub_community(Network, Methods, gold_Network, gold_genes, edge_cutoff, network_names)
    comnet_mat = community_network(Network, Methods, gold_Network, gold_genes, edge_cutoff, network_names)
    fig = plot_communities(comhub_mat, comnet_mat, ci=95)
    fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/HPA_communities_article.png', dpi=200, bbox_inches="tight")
    
    fig = plot_method_performance(GS_PCC, edge_cutoff, comhub_mat, comnet_mat, Network, Methods, gold_Network, gold_genes, network_names)
    fig.savefig('/home/bioinf/Documents/Hub_project/Article_figures/HPA_method_performance_article.png', dpi=200, bbox_inches="tight")

    hubs = find_hubs(Network, Methods, gold_Network, gold_genes, edge_cutoff, network_names)
    pd.DataFrame(hubs).to_csv('/home/bioinf/Documents/Hub_project/HPA_hubs_string.txt', header=None, index=False)
    
    

if __name__ == "__main__":
    main()