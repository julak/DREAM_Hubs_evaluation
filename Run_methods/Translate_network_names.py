#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  4 13:33:21 2019

@author: bioinf
"""
import pandas as pd

net = pd.read_csv('/home/bioinf/Documents/Hub_project/Run_methods/Result/ARACNE_network_HPA.csv', sep='\t', header=None)
net = pd.read_csv('/home/bioinf/Documents/Hub_project/Run_methods/Result/CLR_network_HPA.tsv', sep='\t', header=None)
net = pd.read_csv('/home/bioinf/Documents/Hub_project/Run_methods/Result/EN_Net_500_HPA.csv', sep='\t', header=None)
net = pd.read_csv('/home/bioinf/Documents/Hub_project/Run_methods/Result/TIGRESS_HPA_L1.txt', sep='\t', header=None)

key = pd.read_csv('/home/bioinf/Documents/Hub_project/HPA/HPA_key.tsv', sep='\t', header=None)

net.columns = ['TF', 'target','c']
key.columns = ['TFname', 'TF']
net = net.merge(key, on='TF')
key.columns = ['target_name', 'target']
net = net.merge(key, on='target')

net = net.loc[:,['TFname','target_name','c']]
net = net.sort_values(by='c', ascending=False)
net = net[net.c != 0]

net.to_csv('/home/bioinf/Documents/Hub_project/Run_methods/Result/ARACNE_network_HPA_name.csv', sep='\t', header=None, index=False)
net.to_csv('/home/bioinf/Documents/Hub_project/Run_methods/Result/CLR_network_HPA_name.csv', sep='\t', header=None, index=False)
net.to_csv('/home/bioinf/Documents/Hub_project/Run_methods/Result/EN_Net_500_HPA_name.csv', sep='\t', header=None, index=False)
net.to_csv('/home/bioinf/Documents/Hub_project/Run_methods/Result/TIGRESS_HPA_L1_name.csv', sep='\t', header=None, index=False)