#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  3 16:16:03 2019

@author: bioinf
"""
import pandas as pd
import numpy as np
import sklearn.linear_model as lm
from sklearn.utils import resample
import time

#read data, HPA
Exp = pd.read_table('/home/bioinf/Documents/Hub_project/HPA/HPAznorm2_expression_data.tsv').T
TFs = pd.read_table('/home/bioinf/Documents/Hub_project/HPA/HPAznorm2_transcription_factors.tsv', header=None)
TFs_index = np.where(np.in1d(np.array(Exp.index), np.array(TFs)))[0]

#Exp=np.array([[1, 2, 4, 1],[3, 1, 1, 1],[2, 4, 3, 1], [5, 1, 2, 1], [5, 6, 3,1 ]])
#TFs_index=[1,3,4]
#Bootstrap LASSO, resample with replacement
coef_Mat_sum = np.zeros((len(Exp), len(TFs_index)))
for i in range(10):
    targets = np.transpose(resample(np.array(Exp.T)))
    TFs = np.transpose(targets[TFs_index])    
    
    model = lm.ElasticNetCV(n_jobs=-1)
    
    startTime = time.time()
    coef_Mat = []
    for i in range(100):#len(targets)):
        #print(str(i) + ' of ' + str(len(targets.T)))
        model.fit(TFs, targets[i])
        coef_Mat.append(model.coef_)
    
    print(time.time()-startTime)

    coef_Mat = (np.array(coef_Mat)>0)*1
    coef_Mat_sum = coef_Mat_sum + coef_Mat 

#How many times a coefficient is >0
coef_Mat_sum = coef_Mat_sum/10
pd.DataFrame(coef_Mat_sum).to_csv()

#coef_mat from cluster
coef_mat1 = pd.read_csv('/home/bioinf/Documents/Hub_project/HPA_results/HPAznorm2_EN_coef_1.csv', index_col='Unnamed: 0')
coef_mat2 = pd.read_csv('/home/bioinf/Documents/Hub_project/HPA_results/HPAznorm2_EN_coef_2.csv', index_col='Unnamed: 0')
coef_mat3 = pd.read_csv('/home/bioinf/Documents/Hub_project/HPA_results/HPAznorm2_EN_coef_3.csv', index_col='Unnamed: 0')
coef_mat4 = pd.read_csv('/home/bioinf/Documents/Hub_project/HPA_results/HPAznorm2_EN_coef_4.csv', index_col='Unnamed: 0')
coef_mat5 = pd.read_csv('/home/bioinf/Documents/Hub_project/HPA_results/HPAznorm2_EN_coef_5.csv', index_col='Unnamed: 0')

coef_mat = coef_mat1 + coef_mat2 + coef_mat3 + coef_mat4 + coef_mat5

coef_mat = coef_mat/500

coef_mat.columns = list(TFs.iloc[:,0])
coef_mat.index = Exp.index

TF = list(np.repeat(coef_mat.columns, len(coef_mat.index)))
targets = list(np.tile(coef_mat.index, len(coef_mat.columns)))
value = list(np.array(coef_mat.T).flatten())
Net = pd.DataFrame(np.transpose([TF,targets,value]))
Net.columns = ['TF','target','confidence']
Net_sort = Net.sort_values(by='confidence', ascending=False).iloc[:100000,:]

Net_sort.to_csv('/home/bioinf/Documents/Hub_project/HPA_results/EN_Net_500_HPAznorm2.csv', index=False, header=None, sep='\t')


