%expression_file = '/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Network1/input data/net1_expression_data.tsv'
%tflist_file = '/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Network1/input data/net1_transcription_factors.tsv'
%expression_file = string("net1_expression_data.tsv")
%tflist_file = string("net1_transcription_factors.tsv")

%tigress_full(expression_file,tflist_file,'L',3,'R',500,'verbose',false)
clear
cd '/home/bioinf/Documents/Hub_project/Run_methods/TIGRESS-PKG-2.1/src/run_tigress'

%datapath = '/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Network3/input data';
%networkname = 'net3';

%datapath = '/home/bioinf/Documents/Exjobb/LASSIM_data';
%networkname = 'TH2';

%datapath = '/home/bioinf/Documents/Exjobb/HPA';
datapath = '/home/bioinf/Documents/Hub_project/HPA';
networkname = 'HPAznorm2';

data = read_data(datapath,networkname);
TF_file = strcat([datapath,'/',networkname,'_transcription_factors.tsv']);
data.tflist = transpose(importdata(TF_file));
[tf,loc]=ismember(data.genenames,data.tflist);
idx=[1:length(data.genenames)];
idx=idx(tf);
idx=idx(loc(tf));
data.tf_index = idx;
%data.expdata = data.expdata(1:20,1:3000)


%freq=tigress(data)
freq = tigress(data,'R',1000,'alpha',0.2,'L',5,'verbose',true, 'LarsAlgo', 'lars');
scores=score_edges(freq,'method','area','L',1);
save_file = strcat(['./TIGRESS_', networkname, '_L1.txt']);
edges=predict_network(scores,data.tf_index,'cutoff',100000,'genenames',data.genenames,'name_net',save_file);