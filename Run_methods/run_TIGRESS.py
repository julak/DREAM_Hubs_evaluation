#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 16:10:05 2018

@author: bioinf
"""

import os
import pandas as pd
import numpy as np
from oct2py import octave

home_folder = '/home/bioinf/Documents/Hub_project/'

octave.addpath(home_folder+'DREAM_Hubs_evaluation/Run_methods/')

save_file=home_folder+'Run_methods/tigress_net_HPA.tsv'

#datapath = '/home/bioinf/Documents/Hub_project/Dream5/DREAM5_network_inference_challenge/Network1/input data/'
#networkname = 'net1'

datapath = '/home/bioinf/Documents/Hub_project/HPA/'
networkname = 'HPA'

edges=octave.runtigress(datapath, networkname, save_file, home_folder)
#freq = octave.tigress(data, genenames, tf_index);
#scores= octave.score_edges(freq,'method','area','L',5);
##save_file = strcat(['./TIGRESS_', networkname, '_DREAM5parameters.txt'])
#edges= octave.predict_network(scores,data.tf_index,'cutoff',100000,'genenames',data.genenames,'name_net',save_file)

pd.read_table('/home/bioinf/Documents/Hub_project/HPA/HPA_gold_standard.tsv').columns


