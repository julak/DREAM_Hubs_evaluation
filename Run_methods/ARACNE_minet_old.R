library('minet')

#https://www.bioconductor.org/packages/release/bioc/html/minet.html
#http://www.bioconductor.org/packages/release/bioc/manuals/minet/man/minet.pdf

#Example data
data(syn.data)
mim <- build.mim(syn.data,estimator="spearman")

#Read data
mydata <- read.table('/home/bioinf/Documents/Hub_project/Run_methods/ARACNE/HPA2_expression_data.tsv', row.names = 1)
colnames(mydata) <- mydata[1,]
mydata <- t(mydata[-c(1),])

#Mutual information matrix
mim <- build.mim(mydata,estimator="spearman")

#ARACNE
net <- aracne(mim)

#Format and save network
net_scores <- c(net)

n=length(colnames(net))
network = cbind(matrix(rep(colnames(net), each=n)), matrix(c(rep(rownames(net), times=n))), matrix(net_scores))

if (length(network)<100000) {
network_sort = network[order(network[,3], decreasing = TRUE),]
} else {
network_sort = network[order(network[,3], decreasing = TRUE),][1:10000,]
}

write.table(network, file = "/home/bioinf/Documents/Hub_project/Run_methods/ARACNE_network.csv",
          sep='\t',
          quote = FALSE,
          row.names = FALSE,
          col.names = FALSE)
