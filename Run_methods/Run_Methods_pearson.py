#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  1 11:02:37 2018

@author: bioinf
"""
import pandas as pd
import numpy as np
import scipy.stats as sts
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import sklearn.preprocessing as skp
import random

runfile('/home/bioinf/Documents/Exjobb/Script/Pearson_correlation/Functions_Pearson_correlation.py', wdir='/home/bioinf/Documents/Exjobb/Script/Pearson_correlation')

def comhubs(Mat_rank):
    rborda = np.divide(Mat_rank.iloc[:,:-2].T.sum(), 35) #rborda=1/K*sum(rankj)
    r = pd.DataFrame(rborda, columns=['comhub'])
    return r

def add_community(Mat_rank):
    r = comhubs(Mat_rank)
    Mat_rank = Mat_rank.join(r)
    return Mat_rank

##Function getting interaction count from network##
def topEdges(TFcount, len_Network, method, Method_Network, gold_genes): 
    
    if gold_genes != None:
        Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,0], gold_genes)] 
        Method_Network = Method_Network.iloc[np.isin(Method_Network.iloc[:,1], gold_genes)] 
    
    if len(Method_Network)>len_Network:
        Method_Network = Method_Network.iloc[:len_Network,:]

    TFcount2 = pd.DataFrame(Method_Network.groupby(Method_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = [method])
    TFcount = TFcount.join(TFcount2, how='outer')       
    TFcount = TFcount.fillna(0).astype(int) 
    
    return TFcount

def comHubs_TFrank(Mat_rank, Methods_PCC):
    P_com=[]
    for i in range(0,len(Methods_PCC)):

#        com = pd.Series(np.mean(Mat_rank.loc[:,Methods_PCC[:i+1]].T), index=Mat_rank.index)
        com = pd.Series(sts.mstats.gmean(Mat_rank.loc[:,Methods_PCC[:i+1]].T), index=Mat_rank.index)
#        com = pd.Series(sts.mstats.hmean(Mat_rank.loc[:,Methods_PCC[:i+1]].T), index=Mat_rank.index)
        
        ##Correlation
        P = sts.pearsonr(x=Mat_rank.loc[:,'GS'], y=com)
#        P = sts.pearsonr(x=GS, y=com) #Test om tar average av TF outdegree först och sen rankar blir bättre.
        P_com.append(P)
    
    P_com = pd.DataFrame(P_com, index=np.arange(1,len(Methods_PCC)+1), columns=['PCC','pval'])
    
    return P_com

def random_community(Network, Methods):
    #Ranked TFs.
    TFcount_rank = pd.read_csv(folder+'/Rank_mat/TFcount_Mat_' + Network + '.csv', sep=',', index_col='Unnamed: 0')
    IPRMat_rank = pd.read_csv(folder+'/Rank_mat/IPR_Mat_' + Network + '.csv', sep=',', index_col='Unnamed: 0')
    
    if Network=='1':
        Mat_rank=TFcount_rank
    else:
        Mat_rank=IPRMat_rank
        
    random_mat = pd.DataFrame()
    random_name = pd.DataFrame()
    
    for i in range(1000):
        print(i)
        #random_method = Methods.copy()
        #random.shuffle(random_method)
        
        random_method = np.random.choice(Methods, size=len(Methods))
        
        P_com = pd.DataFrame(comHubs_TFrank(Mat_rank, random_method).PCC)
        P_com.columns = [i]
        
        random_mat = random_mat.join(P_com, how='outer')
        random_name = random_name.join(pd.DataFrame(random_method, columns=[i]), how='outer')
     
    ##Plot
    color_pallet = "bright"
    plt.style.use('seaborn-ticks')
    sns.set_color_codes(color_pallet)
    
    fig, ax = plt.subplots(facecolor='w', edgecolor='k', figsize=(10,8), dpi=75)
    plt.rc('font', size=20) 
    #plt.rc('ytick', labelsize=14)
    #plt.rc('xtick', labelsize=14)
        
    plt.plot(random_mat, 'k.', markersize=11)
    plt.plot(random_mat.T.mean(), 'c.', markersize=13)
    
    ax.set(ylabel='PCC', xlabel='#Methods', title='HPA')
    ax.minorticks_on()
    ax.grid(which="major", color='lightgray', linestyle='--', linewidth=1)
    ax.grid(which="minor", color='lightgray', linestyle='--', linewidth=0.5)
       
    return fig, random_mat

def run_community(Network,folder,Methods):

    Goldstandard = 'Network' + Network  
      
    if Network == '1' or Network=='3':
        gold_Network = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Evaluation scripts/INPUT/gold_standard_edges_only/DREAM5_NetworkInference_Edges_' + Goldstandard + '.tsv', header=None)
    elif Network=='TH2' or Network == 'HPA2':
        gold_Network = pd.read_table('/home/bioinf/Documents/Exjobb/HPA/'+Network+'_gold_standard.tsv', header=None)
        #gold_Network = pd.read_table('/home/bioinf/Documents/Exjobb/LASSIM_data/TH2_gold_standard_from_TFBN_Th2.tsv', header=None)
       
    gold_TF = gold_Network.loc[:,0].unique()
    gold_genes = gold_Network.loc[:,0].append(gold_Network.loc[:,1]).unique()
    
    if Network == '1' or Network=='3':
        exp = pd.read_csv('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Network' + Network + '/input data/net' + Network + '_expression_data.tsv', sep='\t')
    elif Network=='TH2' or Network == 'HPA2':
        exp = pd.read_csv('/home/bioinf/Documents/Exjobb/HPA/'+ Network + '_expression_data.tsv', sep='\t')
    len_targets =len(exp.columns)
    
    if Network == '1':
        len_Network = 4012
    elif Network == '3' or Network=='TH2' or Network == 'HPA2':
        len_Network = 100000 
    
    ##IPR, ttest and topEdges
    #Create matrices
    IPRMat = pd.DataFrame()#index=gold_TF) #Store IPR score for each TF in each method 
    Tmat = pd.DataFrame()#index=gold_TF) #Store t-test score for each TF in each method
    TFcount = pd.DataFrame()
     
    for method in Methods:
        print(method)
        
        if method == 'ARACNE':
            if Network == '1':
                Met_Net = pd.read_table(folder+'/ARACNE_Net'+Network+'_MI_0_DPI_0.80.csv', sep=',', header=None)
            elif Network == '3':
                Met_Net = pd.read_table(folder+'/ARACNE_Net'+Network+'_Adj_MI_0_DPI_0.40.csv', sep=',', header=None)
            elif Network == 'TH2' or Network=='HPA2':
                Met_Net = pd.read_table(folder+'/ARACNE_'+Network+'_Adj_MI_0_DPI_0.10.csv', sep=',', header=None)
        elif method == 'CLR':
            Met_Net = pd.read_table(folder+'/CLR_Net'+Network+'.csv', sep=',', header=None)
        elif method == 'GENIE3':
            Met_Net = pd.read_table(folder+'/GENIE3_Network_'+Network+'.csv', sep='\t', header=None)
        elif method == 'TIGRESS':
            if Network == '1' or Network=='3':
                Met_Net = pd.read_table(folder+'/TIGRESS_Net'+Network+'_DREAM5parameters.txt', header=None)
            elif Network == 'TH2' or Network == 'HPA2':
                Met_Net = pd.read_table(folder+'/TIGRESS_'+Network+'_DREAM5parameters.txt', header=None)
        elif method == 'ElasticNet':
            Met_Net = pd.read_table(folder+'/ElasticNet_Net'+Network+'.csv', sep=',', header=None)
        elif method == 'PCC':
            Met_Net = pd.read_table(folder+'/PCC_Network_'+Network+'.csv', sep=',', header=None)
        
        Met_Net = Met_Net.iloc[np.isin(Met_Net.iloc[:,0], gold_genes)] 
        Met_Net = Met_Net.iloc[np.isin(Met_Net.iloc[:,1], gold_genes)]   
        
        TF_score = TF_target_score(Met_Net)
    
        if Network=='1' or Network=='3' or Network == 'HPA2':
            TF_score = TF_score.iloc[np.isin(TF_score.index,gold_genes),np.isin(TF_score.columns,gold_genes)]
    
        #Test edge cutoff
        Edges = pd.DataFrame(np.array(TF_score).flatten())
        Edge_cutoff = Edges[Edges>0].dropna().nlargest(len_Network, columns=0).iloc[-1,0]
        TF_score[TF_score < Edge_cutoff] = 0
                
        TF_score = TF_score.loc[~(TF_score==0).all(axis=1)]
    
        #Do L2 normalisation
        TF = pd.DataFrame(skp.normalize(TF_score, norm='l2', axis=1, copy=True, return_norm=False), index=TF_score.index, columns=TF_score.columns)
                 
        #Calculate IPR score
        Yinverse = inverse_participation_ratio(TF, method)
        IPRMat = IPRMat.join(Yinverse, how='outer')
    
        #Calculate t-test p-value
        Pval = ttest_method(TF, method)
        Tmat = Tmat.join(Pval, how='outer')
        
        #topEdges
        TFcount = topEdges(TFcount, len_Network, method, Met_Net, gold_genes=None)
             
    ##Gold standard
    TFcount_gold = pd.DataFrame(gold_Network.groupby(gold_Network.loc[:,0].tolist()).size().sort_values(ascending=False), columns = ['GS'])
    IPRMat = IPRMat.join(TFcount_gold, how='outer')
    IPRMat = IPRMat.fillna(0)
    TFcount = TFcount.join(TFcount_gold, how='outer')
    TFcount = TFcount.fillna(0)
    
    ##ranking
    IPRMat_rank = IPRMat.rank(axis=0, method='average', ascending=True)
    
    Tmat = Tmat.fillna(1)
    Tmat_rank = Tmat.rank(axis=0, method='average', ascending=False)
    Tmat_rank = Tmat_rank.join(IPRMat_rank.loc[:,'GS'], how='outer')
    
    TFcount = TFcount.rename(columns = {'Network'+Network:'GS'})
    TFcount_rank = TFcount.rank(axis=0, method='average', ascending=True)
    
    ##Add community
    TFcount_rank = add_community(TFcount_rank)
    IPRMat_rank = add_community(IPRMat_rank)
    Tmat_rank = add_community(Tmat_rank)
    
    TFcount_rank.to_csv(folder+'/Rank_mat/TFcount_Mat_' + Network + '.csv')
    IPRMat_rank.to_csv(folder+'/Rank_mat/IPR_Mat_' + Network + '.csv')
    Tmat_rank.to_csv(folder+'/Rank_mat/ttest_Mat_' + Network + '.csv')
    return TFcount_rank, IPRMat_rank, Tmat_rank


Network = 'HPA2' #Which Network to use. 1=In Silico, 2=S.aureus, 3=E.coli, 4=S.cerevisiae.
folder = '/home/bioinf/Documents/Exjobb/Methods/Networks/'+Network
Methods = ['ARACNE','GENIE3','ElasticNet','PCC','CLR','TIGRESS']

TFcount_rank, IPRMat_rank, Tmat_rank = run_community(Network,folder,Methods)

#Random community
fig, random_mat = random_community(Network, Methods)
random_mat.to_csv(folder+'/Random/Community_random_order_replacement.csv')

fig.savefig(folder+'/Random/Community_Random_order_gmean_string_' + Network + '.png', dpi=200, bbox_inches="tight")

##Kolmogorov-Smirnov test
KS_vec = []
for i,j in zip(np.arange(len(random_mat)-1), np.arange(1,len(random_mat))):
    KS = sts.ks_2samp(random_mat.iloc[i], random_mat.iloc[j])
    KS_vec.append([i+1,j+1,KS.statistic, KS.pvalue])
KS_vec = pd.DataFrame(KS_vec, columns=['method_i','method_j','statistic','pvalue'])
KS_vec.to_csv(folder + '/Random/KS_test_on_disribution_string_'+Network+'.csv', sep='\t',index=False)

MWU_vec = []
for i,j in zip(np.arange(len(random_mat)-1), np.arange(1,len(random_mat))):
    f_exp = random_mat.iloc[i]
    f_obs = random_mat.iloc[j]
    MWU = sts.mannwhitneyu(f_obs, f_exp)
    MWU_vec.append([i+1,j+1,MWU.statistic, MWU.pvalue])
MWU_vec = pd.DataFrame(MWU_vec, columns=['method_i','method_j','statistic','pvalue'])
MWU_vec.to_csv(folder + '/Random/MannWhitneyU_test_on_disribution_string_'+Network+'.csv', sep='\t',index=False)

##Pearson correlation coefficient##
P_TFcount = Pearson_corr(x=TFcount_rank.loc[:,'GS'], Ymat=TFcount_rank)
P_TFcount.columns = ['PCC_topEdges','pval_topEdges']
P_IPR = Pearson_corr(x=IPRMat_rank.loc[:,'GS'], Ymat=IPRMat_rank)
P_IPR.columns = ['PCC_IPR','pval_IPR']
P_ttest = Pearson_corr(x=Tmat_rank.loc[:,'GS'], Ymat=Tmat_rank)
P_ttest.columns = ['PCC_ttest','pval_ttest']

P_mat = P_TFcount.join(P_IPR)
P_mat = P_mat.join(P_ttest)

P_mat.to_csv(folder+'/Random/PCC_string_'+Network+'.csv', sep='\t')

##DREAM scores##
DREAM_scores = pd.read_table('/home/bioinf/Documents/Exjobb/Dream5/Scores.txt', index_col=0)
AUROC_col = 'AUROC_Net' + Network
AUPR_col = 'AUPR_Net' + Network
Net_DREAM = DREAM_scores.loc[:,[AUROC_col, AUPR_col]]#.iloc[:-1,:]
Net_DREAM = Net_DREAM.rename({'Community_RankSum' : 'community'})
P_mat = P_mat.join(Net_DREAM)
P_mat = P_mat.rename(columns={'AUROC_Net' + Network:'AUROC', 'AUPR_Net' + Network:'AUPR'})

P_mat = P_mat.sort_values(by='PCC_topEdges').iloc[:-1,:]

##Plot
Mask = (P_mat > 0.05)
Mask = Mask.iloc[:,[1,3,5,7]]
Mask.AUPR = False
Mask.columns = P_mat.iloc[:,[0,2,4,7]].columns

plt.rc('ytick', labelsize=5)
plt.rc('xtick', labelsize=10) 
Pearson_heatmap = sns.heatmap(P_mat.iloc[:,[0,2,4,7]], cmap='RdBu_r', center=0, mask=Mask)
fig = Pearson_heatmap.get_figure()

fig.savefig('/home/bioinf/Documents/Exjobb/Resultat/' + Network + '/Pearson_correlation/Pearson_topEdges_IPR_ttest_' + str(len_Network) + '_' + Network + '.png', dpi=200, bbox_inches="tight")


  