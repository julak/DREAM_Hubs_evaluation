#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 11:43:31 2018

@author: bioinf
"""

import pandas as pd
import numpy as np
import scipy.stats as sts

Network='HPAznorm2'
folder='/home/bioinf/Documents/Hub_project/HPA/'

Exp = pd.read_csv(folder + Network+'_expression_data.tsv', sep='\t')
TF_names = pd.read_csv(folder + Network+'_transcription_factors.tsv', sep='\t', header=None)

Net=[]
for i in np.array(TF_names).flatten():
    print(i)
    for j in Exp:
        p = sts.pearsonr(Exp.loc[:,i],Exp.loc[:,j])
        p_edge = [i,j,np.abs(p[0]),p[1]]
        Net.append(p_edge)
        
#Exp=np.array(Exp)
#Net=[]
#for i in range(len(np.array(TF_names).flatten())):
#    print(i)
#    for j in range(len(Exp.T)):
#        p = sts.pearsonr(Exp[:,i],Exp[:,j])
#        p_edge = [i,j,np.abs(p[0]),p[1]]
#        Net.append(p_edge)

Net = pd.DataFrame(Net, columns=['TF','target','confidence_score','p_val'])
Net = Net.sort_values(by='confidence_score', ascending=False)
Net = Net[Net.TF != Net.target]
Net2 = Net.iloc[:100000,:3]	
Net2.to_csv('/home/bioinf/Documents/Hub_project/HPA_results/PCC_Network_'+Network+'.csv', header=None, index=False, sep='\t')