library('minet')

#https://www.bioconductor.org/packages/release/bioc/html/minet.html
#http://www.bioconductor.org/packages/release/bioc/manuals/minet/man/minet.pdf

##Example data
#data(syn.data)
#mim <- build.mim(syn.data,estimator="spearman")

#ARACNE
mydata <- read.table('/home/bioinf/Documents/Hub_project/HPA/HPAznorm2_expression_data.tsv', header=TRUE, sep='\t')

mim <- build.mim(mydata,estimator="spearman")

net <- aracne(mim)

write.table(net, file = "/home/bioinf/Documents/Hub_project/HPA_results/ARACNE_matrix_HPAznorm2.csv",
          sep='\t',
          quote = FALSE,
          row.names = TRUE,
          col.names = TRUE)
