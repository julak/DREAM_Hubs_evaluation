function edges=runtigress(datapath, networkname)%expdata, genenames, tfnames, tf_index, save_file, home_folder)
%folder=strcat([home_folder,'Run_methods/TIGRESS-PKG-2.1/src/run_tigress/'])
cd '/home/bioinf/Documents/Hub_project/Run_methods/TIGRESS-PKG-2.1/src/run_tigress/'

%data.expdata=expdata;
%data.tflist=transpose(tfnames);
%data.tfindices=transpose(tf_index);
%data.genenames=genenames;

data = read_data(datapath,networkname);
data.genenames=strsplit(data.genenames{1}, '\t'); %only use line if running in octave
TF_file = strcat([datapath,'/',networkname,'_transcription_factors.tsv']);
data.tflist = transpose(importdata(TF_file));
[tf,loc]=ismember(data.genenames,data.tflist);
idx=[1:length(data.genenames)];
idx=idx(tf);
idx=idx(loc(tf));
data.tf_index = idx;
    
freq = tigress(data);
scores= score_edges(freq,'method','area','L',5);
edges= predict_network(scores,data.tf_index,'cutoff',100000,'genenames',data.genenames,'name_net',save_file);

end
