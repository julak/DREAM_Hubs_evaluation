#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  2 14:43:04 2019

@author: bioinf
"""

#Format network after running ARACNE in R.

import pandas as pd
import numpy as np

ARACNE = pd.read_csv('/home/bioinf/Documents/Hub_project/HPA_results/ARACNE_matrix_HPAznorm2.csv',sep='\t')
TFs = pd.read_csv('/home/bioinf/Documents/Hub_project/HPA/HPAznorm_transcription_factors.tsv')

ARACNE = ARACNE.loc[list(TFs.iloc[:,0])]
ARACNE=ARACNE.dropna()
TF = list(np.repeat(ARACNE.index, len(ARACNE.columns)))
targets = list(np.tile(ARACNE.columns, len(ARACNE.index)))
value = list(np.array(ARACNE).flatten())
Net = pd.DataFrame(np.transpose([TF,targets,value])).sample(frac=1)
Net.columns = ['TF','target','confidence']
Net_sort = Net.sort_values(by='confidence', ascending=False)
#Net_sort[Net_sort.iloc[:,2]=='0.0'] = Net_sort[Net_sort.iloc[:,2]=='0.0']
#Net_sort = Net_sort[pd.to_numeric(Net_sort.iloc[:,2])!=0]
Net_sort.iloc[:100000,:].to_csv('/home/bioinf/Documents/Hub_project/HPA_results/ARACNE_network_HPAznorm2.csv', sep='\t', index=False, header=None)

#Net = pd.read_csv('/home/bioinf/Documents/Hub_project/Run_methods/ARACNE_network_HPA.csv')
#Net = Net.iloc[:,1:]
#Net.to_csv('/home/bioinf/Documents/Hub_project/Run_methods/ARACNE_network_HPA.csv', sep='\t', index=False, header=None)
