#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  7 11:59:50 2018

@author: bioinf
"""
import pandas as pd
import numpy as np
from GENIE3 import loadtxt
from GENIE3 import GENIE3
from GENIE3 import get_link_list

runfile('/home/bioinf/Documents/Exjobb/Methods/GENIE3/GENIE3_python/GENIE3.py', wdir='/home/bioinf/Documents/Exjobb/Methods/GENIE3/GENIE3_python')

##Example data
#data = loadtxt('/home/bioinf/Documents/Exjobb/Methods/GENIE3_python/GENIE3_python/data.txt',skiprows=1)
#f = open('data.txt')
#gene_names = f.readline()
#f.close()
#gene_names = gene_names.rstrip('\n').split('\t')

##DREAM5 data network 1
#Network='3'
#data = loadtxt('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Network'+Network+'/input data/net'+Network+'_expression_data.tsv', skiprows=1)
#f = open('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Network'+Network+'/input data/net'+Network+'_expression_data.tsv')
#gene_names = f.readline()
#f.close()
#gene_names = gene_names.rstrip('\n').split('\t')
#TFs = pd.read_csv('/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Network'+Network+'/input data/net'+Network+'_transcription_factors.tsv', header=None)
#TFs = list(np.array(TFs).flatten())

##TH2-data
Network='HPA'
data = loadtxt('/home/bioinf/Documents/Exjobb/LASSIM_data/'+Network+'_expression_data.tsv', skiprows=1)
f = open('/home/bioinf/Documents/Exjobb/LASSIM_data/'+Network+'_expression_data.tsv')
gene_names = f.readline()
f.close()
gene_names = gene_names.rstrip('\n').split('\t')
TFs = pd.read_csv('/home/bioinf/Documents/Exjobb/LASSIM_data/'+Network+'_transcription_factors.tsv', header=None)
TFs = [str(x) for x in list(np.array(TFs).flatten())]

#Run GENIE3y
#VIM = GENIE3(data)
VIM = GENIE3(data, gene_names=gene_names, regulators=TFs)

Net = get_link_list(VIM, gene_names=gene_names, regulators=TFs, maxcount=100000, file_name='/home/bioinf/Documents/Exjobb/Methods/Networks/'+Network+'/GENIE3_Network_'+Network+'test.csv')

