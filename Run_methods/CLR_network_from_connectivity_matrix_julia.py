#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 14:54:02 2018

@author: bioinf
"""

import numpy as np
import pandas as pd

Network='HPAznorm2'

if Network=='3' or Network=='1':
    folder = '/home/bioinf/Documents/Exjobb/Dream5/DREAM5_network_inference_challenge/Network'+Network+'/input data/net'
elif Network=='TH2' or Network=='HPA2':
    folder = '/home/bioinf/Documents/Exjobb/LASSIM_data/'
elif Network == 'HPAznorm2':
    folder = '/home/bioinf/Documents/Hub_project/HPA/'
    
Exp = pd.read_csv(folder+Network+'_expression_data.tsv', sep='\t')
TF = pd.read_csv(folder+Network+'_transcription_factors.tsv', sep='\t', header=None)
if Network=='3' or Network=='1':
    CLR = pd.read_csv('/home/bioinf/Documents/Exjobb/Methods/CLRv1.2.2/Code/Connectivity_matrix_stouffer_Net'+Network+'.csv', header=None)
elif Network=='TH2' or Network=='HPA2':
    CLR = pd.read_csv('/home/bioinf/Documents/Exjobb/Methods/CLRv1.2.2/Code/Connectivity_matrix_stouffer_'+Network+'.csv', header=None)
elif Network=='HPAznorm2':
    CLR = pd.read_csv('/home/bioinf/Documents/Hub_project/HPA_results/Connectivity_matrix_stouffer_'+Network+'.csv', header=None)

Gene = Exp.columns
CLR.columns = Gene
CLR.index = Gene

#Interactions for all algorithms were only allowed from 328 known or predicted transcription factors to any of the 4,345 genes, enabling clear 
#biological interpretation, assignment of direction (from transcription factors to non–transcription factor genes), and validation of the predictions. 
#Interactions were also identified between transcription factors, but direction was not assigned. (Faith et al. 2007)
TFs = np.array(TF).flatten()
TFs = [str(x) for x in TFs]

CLR_dir = CLR.loc[TFs,:] #Only keep rows that are TFs to get directed network, see above.

TF = list(np.repeat(CLR_dir.index, len(CLR_dir.columns)))
targets = list(np.tile(CLR_dir.columns, len(CLR_dir.index)))
value = list(np.array(CLR_dir).flatten())
Net = pd.DataFrame(np.transpose([TF,targets,value]))
    
# almost get the same result as in DREAM5 (only checked top interactions) only difference is that TF-TF interactions have direction. 
# Seem almost as they only have taken lowest TF to highest TF. 
        
Net.columns = ['TF','target','confidence']

Net.confidence = [float(x) for x in list(Net.confidence)]

Net_sort = Net.sort_values(by='confidence', ascending=False).iloc[:100000,:]
        
Net_sort.to_csv('/home/bioinf/Documents/Hub_project/HPA_results/CLR_Net'+Network+'.csv', index=False, header=None, sep='\t')


